insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (1,'Enzo','scalone.enzo@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Scalone',0,NULL);
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (1, '1', '1');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (2, '1', '2');


INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (1, '27-36387877-0', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '1', 'Puerto Madryn', '9120', 'Chubut', '123');


-- Insert 10 usuarios (contraseña: 45454545)
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (50,'Jorge','dubrovnik@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Andor',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (51,'Alberto','hoteltolosa@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Pach',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (52,'Municipalidad','municipalidadpuertomadryn@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'De Puerto Madryn',1,2);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (53,'Lautaro','restaurant@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Sepul',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (54,'Marcos','nauticorestaurant@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Multi',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (55,'Mariano','rayentraiHotel@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Corbeto',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (56,'Julian','hotelterritorio@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Gena',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (57,'Nicolas','nicolasbueno@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Bueno',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (58,'Bruno','burnomil@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Kan',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (59,'Pablo','mrpablo@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Lupe',1,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (60,'Museo','museodebellasartes@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'De Bellas Artes',1,2);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (61,'Nahuel','nahuel@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Lupa',0,1);
insert into users (id,name,email,password,created_at,updated_at,lastname,active,payment_method_id) values (62,'Esteban','esteban@gmail.com','$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Pubg',0,1);


-- insert datos de los users
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (50, '20-36377877-1', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '50', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (51, '21-36377877-9', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '51', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (52, '22-36377877-8', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '52', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (53, '23-36377877-7', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '53', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (54, '24-36377877-6', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '54', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (55, '25-36377877-5', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '55', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (56, '26-36377877-4', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '56', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (57, '27-36377877-3', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '57', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (58, '28-36377877-2', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '58', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (59, '29-36377877-1', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '59', 'Puerto Madryn', '9120', 'Chubut', '123');
INSERT INTO `data_users` (`id`, `cuit`, `bussiness_name`, `phone`, `_address`, `user_id`, `city`, `postal_code`, `province`, `address_number`) VALUES (60, '30-36377877-0', 'Negocios de Prueba', '2804656565', 'Direccion de prueba', '60', 'Puerto Madryn', '9120', 'Chubut', '123');




-- insert roles clientes
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (50, '50', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (51, '51', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (52, '52', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (53, '53', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (54, '54', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (55, '55', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (56, '56', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (57, '57', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (58, '58', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (59, '59', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (60, '60', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (61, '61', '2');
INSERT INTO `user_has_role` (`id`, `user_id`, `role_id`) VALUES (62, '62', '2');

-- categories 
insert into categories(id, name, parentcategory_id) values (1, 'Alojamiento', null);
insert into categories(id, name, parentcategory_id) values (2, 'Comidas', null);
insert into categories(id, name, parentcategory_id) values (3, 'Entretenimiento', null);
insert into categories(id, name, parentcategory_id) values (4, 'Regionales', null);
insert into categories(id, name, parentcategory_id) values (12, 'Sin definir', null);

insert into categories(id, name, parentcategory_id) values (5, 'Hotel', 1);
insert into categories(id, name, parentcategory_id) values (6, 'Hostel', 1);
insert into categories(id, name, parentcategory_id) values (7, 'Campings', 1);

insert into categories(id, name, parentcategory_id) values (8, 'Restaurants', 2);
insert into categories(id, name, parentcategory_id) values (9, 'Pancherias', 2);

insert into categories(id, name, parentcategory_id) values (10, 'Boliches', 3);
insert into categories(id, name, parentcategory_id) values (11, 'Shoppings', 3);

update categories set updated_at = CURRENT_TIMESTAMP;
update categories set created_at = CURRENT_TIMESTAMP;

-- insert zone
INSERT INTO `zones` (`id`, `_name`, `_description`, `_deleted`) VALUES (50,'Puerto Madryn','Puerto Madryn es una ciudad del noreste de la provincia del Chubut, Argentina, siendo la cabecera del departamento Biedma. Se encuentra emplazada frente al mar Argentino en el océano Atlántico','0');

-- insert posts INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`, `updated_at`, `created_at`) VALUES (NULL, 'Prueba 2', '2805646464', 'Prueba 2', NULL, NULL, NULL, 'active', '51', '50', 'Prueba', 'free', NULL, '2018-10-08 21:00:00', '2018-10-08 21:00:00');
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (50, 'Arenas Blancas', '2804455888', 'Jenkins esq. Morgan', 'T50-021020181647.jpg', -42.784748, -65.017476, 'active', '50', '50', 'Complejo Arenas Blancas', 'gold', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (51, 'Hotel Tolosa', '2804455888', 'Roque Saenz Peña 253', 'T67-0210201816565.jpg', -42.765870, -65.037724, 'active', '51', '50', 'Hotel Tolosa', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (52, 'Argentina Vision', '2804455888', 'Av. Julio A. Roca 536', 'T51-021020181648.jpg', -42.768973, -65.031496, 'active', '52', '50', 'Argentina Vision agencia de turismo', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (53, 'Explore Patagonia', '2804450872', 'Belgrano 317', 'T52-021020181649.jpg', -42.768119, -65.037403, 'active', '52', '50', 'Explore Patagonia agencia de turismo', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (54, 'Cantina El Nautico', '2804471404', 'Av. Julio A. Roca 790', 'T53-021020181650.jpg', -42.771856, -65.029589, 'active', '53', '50', 'Restaurante El Nautico', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (55, 'Cerveceria Cara de Perro', '2804455888', 'Aaron Jenkins 339', 'T54-021020181651.jpg', -42.784438, -65.016718, 'active', '54', '50', 'Cara de Perro Casa de Birras', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (56, 'Restaurant El Coiron', '2804455838', 'Bv. Almte Brown 600', 'T55-021020181652.jpg', -42.769671, -65.030447, 'active', '55', '50', 'Restauran y cocina de autor El COiron', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (57, 'Cuyun Co', '2804451845', 'Av. Julio A. Roca 165', 'T56-021020181653.jpg', -42.765372, -65.033754, 'active', '56', '50', 'Cuyun-Co agencia de turismo', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (58, 'Yoaquina Beach', '2804607851', 'Bv. Brown 1050', 'T57-021020181654.jpg', -42.775047, -65.025306, 'active', '57', '50', 'Argentina Vision agencia de turismo', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (59, 'Dubrovnik Rent a Car', '2974526107', 'Av. Roca 19', 'T58-0210201816555.jpg', -42.763946, -65.034657, 'active', '58', '50', 'Dubrvnik Rent a Car', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (60, 'Ecocentro Puerto Madryn', '2804457470', 'Julio Verne 3784', 'T59-0210201816556.jpg', -42.781892, -64.996807, 'active', '59', '50', 'Espacio cultural de encuentro que promueve una actitud armónica con el océano', 'gold', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (61, 'El Retorno Hostel', '2804692188', 'Mitre 798', 'T60-0210201816558.jpg', -42.772534, -65.032536, 'active', '60', '50', 'Hostel ubicado cerca del centro', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (62, 'En mis fuegos', '2804603342', 'Av. Gales 32', 'T61-0210201816559.jpg', -42.769834, -65.031626, 'active', '51', '50', 'Restaurante En Mis Fuegos', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (63, 'Giuseppe', '02804456891', ' 25 de Mayo 386', 'T62-0210201816560.jpg', -42.767992, -65.033795, 'active', '52', '50', 'Giuseppe pizza y pastas', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (64, 'Hotel Fantilli', '2804455124', 'Mitre 619', 'T63-0210201816561.jpg', -42.770839, -65.033643, 'active', '53', '50', 'Hotel ubicado cerca del centro', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (65, 'Las Maras Apart Hotel', '2804453215', 'Marcos A. Zar 64', 'T64-0210201816562.jpg', -42.765526, -65.038486, 'active', '54', '50', 'Las Maras Apart Hotel', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (66, 'Playa Hotel', '2804451446', 'Av. Julio A. Roca 187', 'T65-0210201816563.jpg', -42.765443, -65.033749, 'active', '55', '50', 'Hotel ubicado en frente de la playa', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (67, 'Hotel Territorio', '2804470050', 'Bv. Brown 3251', 'T66-0210201816564.jpg', -42.785430, -65.003636, 'active', '56', '50', 'Hotel ubicado frente al mar', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (68, 'Kosten al Mar', '2804723564', 'Maria Humphreys 337', 'T68-0210201816566.jpg', -42.781070, -65.019000, 'active', '57', '50', 'Ubicado cerca de la playa', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (69, 'La Estela', '2804451573', 'Roque Saenz Peña 27', 'T69-0210201816567.jpg', -42.764864, -65.034806, 'active', '58', '50', 'Parrilla Restaurante', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (70, 'La Frontera Disco Bar', '2804723027', '9 de Julio 254', 'T70-0210201816568.jpg', -42.768767, -65.035706, 'active', '59', '50', 'La Frontera Disco', 'gold', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (71, 'Lizzard Café', '2804455306', 'Av. Roca 598', 'T71-0210201816569.jpg', -42.769492, -65.031264, 'active', '60', '50', 'Lizzard Cafe', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (72, 'Lobo Larsen', '2804516314', 'Av. Julio Roca 885', 'T72-0210201816570.jpg', -42.772965, -65.028984, 'active', '50', '50', 'Lobo Larsen Buceo', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (73, 'El Gualicho Hostel', '2804454163', 'Marcos A. Zar 480', 'T73-0210201816571.jpg', -42.769841, -65.035830, 'active', '51', '50', 'El Gualicho Hostel', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (74, 'Centro Comercial El Portal de Madryn', '2804474700', 'Avenida Julio Argentino Roca & 28 de Julio', 'T74-0210201816572.jpg', -42.765714, -65.033534, 'active', '52', '50', 'Centro Comercial', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (75, 'Aquatours Buceo', '2804451954', 'Av. Julio A. Roca 550', 'T75-0210201816573.jpg', -42.769408, -65.031164, 'active', '53', '50', 'Aquatours Buceo', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (76, 'Ocean Divers', '2804660865', 'Bv. Brown 1050 Balneario Yoaquina', 'T76-0210201816574.jpg', -42.775079, -65.025280, 'active', '54', '50', 'Ocean Divers Buceo', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (77, 'Lupita Cocina Mexicana', '2804722454', '9 de Julio 146', 'T77-0210201816575.jpg', -42.768367, -65.034485, 'active', '55', '50', 'Lupita', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (78, 'Madryn Buceo', '2804564422', 'Bv. Brown 1900 Bajada 5', 'T78-0210201816576.jpg', -42.780185, -65.019222, 'active', '56', '50', 'Madryn Buceo', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (79, 'Margarita', '2804612442', 'Roque Sáenz Peña 15', 'T79-0210201816577.jpg', -42.764881, -65.034863, 'active', '57', '50', 'Margarita', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (80, 'Mediterraneo', '2804475696', 'Bv. Brown 1070', 'T80-0210201816578.jpg', -42.775386, -65.024899, 'active', '59', '50', 'Mediterraneo Restaurante', 'gold', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (81, 'Monumento a los Colonos Galeses', NULL, 'Av. Julio Argentino Roca & Manuel Belgrano', 'T81-0210201816579.jpg', -42.766573, -65.032887, 'active', '58', '50', 'Monumento a los Colonos Galeses', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (82, 'Mostaza', '2804458373', 'Av. Julio A. Roca y 28 de Julio', 'T82-0210201816580.jpg', -42.765714, -65.033534, 'active', '60', '50', 'No te comas el verso. Veni a Mostaza', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (84, 'Mr. Jones', '2804475368', '9 de Julio 116', 'T83-0210201816581.jpg', -42.768188, -65.034067, 'active', '50', '50', 'Mr. Jones', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (85, 'Municipalidad de Puerto Madryn', '2804471599', 'Belgrano 206', 'T84-0210201816582.jpg', -42.767589, -65.035794, 'active', '51', '50', 'Municipalidad de Puerto Madryn', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (86, 'Museo Oceanogáfico y de Ciencias Naturales', '2804451139', 'Domecq García y Menéndez', 'T85-0210201816583.jpg', -42.762397, -65.039643, 'active', '52', '50', 'Museo Oceanografico y de Ciencias Naturales', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (87, 'Museo Municipal de Arte', '2804453204', 'Av. Julio A. Roca 444', 'T86-0210201816584.jpg', -42.767122, -65.032494, 'active', '53', '50', 'Museo Municipal de Arte', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (88, 'Museo Historico de Puerto Madryn', NULL, 'Dr. Avila', 'T87-0210201816585.jpg', -42.765712, -65.040959, 'active', '54', '50', 'Museo Historico de Puerto Madryn', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (89, 'Nueva Leon Apart Hotel', '2804193148', 'Av. Domec García 365', 'T88-0210201816586.jpg', -42.760253, -65.039017, 'active', '55', '50', 'Nueva Leon Apart Hotel', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (90, 'Pizza House', '2804454276', '25 de Mayo 546', 'T89-0210201816587.jpg', -42.769786, -65.032592, 'active', '56', '50', 'Pizza House', 'gold', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (91, 'Casino Punto y Banca', '2804453869', 'Belgrano 74', 'T90-0210201816588.jpg', -42.766896, -65.033804, 'active', '57', '50', 'Casino Punto y Banca', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (92, 'Hotel Rayentray', '2804459300', 'Boulevard Brown 2889', 'T91-0210201816589.jpg', -42.784504, -65.009818, 'active', '58', '50', 'Hotel Rayentray', 'gold', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (93, 'Sara Beach', '2804826139', 'Bv. Almte Brown 1855', 'T92-0210201816590.jpg', -42.780185, -65.019222, 'active', '59', '50', 'Parador Sara Beach', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (94, 'Scuba Duba Buceo', '2804452699', 'Bv. Brown 893', 'T93-0210201816591.jpg', -42.772452, -65.027822, 'active', '60', '50', 'Scuba Duba Buceo', 'silver', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (95, 'Valdés Resto & Bar', '2804456145', ' Bartolomé Mitre y Roque Saenz Peña', 'T94-0210201816592.jpg', -42.765553, -65.036952, 'active', '50', '50', 'Valdes', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (96, 'Vernardino Club de Mar', '2804474289', 'Bv. Brown 860', 'T95-0210201816593.jpg', -42.772391, -65.027839, 'active', '52', '50', 'Vernardino Club de Mar', 'free', NULL);
INSERT INTO `posts` (`id`, `_name`, `phone`, `address`, `logo`, `lat`, `lon`, `_status`, `user_id`, `zone_id`, `description`, `_type`, `expiration`) VALUES (97, 'El Almendro Restaurante', '2804470525', 'Alvear 409 esq. 9 de julio', 'T96-0210201816594.jpg', -42.770533, -65.040249, 'active', '53', '50', 'El Almendro Restaurante', 'silver', NULL);

-- Categoria aleatoria
INSERT INTO post_has_category(post_id, category_id) 
select id, (SELECT id from categories order by rand() limit 1) 
from posts;

-- insert promos
INSERT INTO `promos` (`id`, `time`, `price`, `post_type`, `name`) VALUES (50, '6000', '800', 'silver', '6000 segundos de promo silver');
INSERT INTO `promos` (`id`, `time`, `price`, `post_type`, `name`) VALUES (51, '6000', '800', 'gold', '6000 segundos de promo gold');


-- insert fotos de publicaciones gold
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (50, '92', 'T91-0210201816589-0.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (51, '92', 'T91-0210201816589-1.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (52, '92', 'T91-0210201816589-2.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (53, '92', 'T91-0210201816589-3.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (54, '92', 'T91-0210201816589-4.jpg');

INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (55, '50', 'T50-021020181647-0.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (56, '50', 'T50-021020181647-1.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (57, '50', 'T50-021020181647-2.jpg');

INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (60, '60', 'T59-0210201816556-0.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (61, '60', 'T59-0210201816556-1.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (62, '60', 'T59-0210201816556-2.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (63, '60', 'T59-0210201816556-3.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (64, '60', 'T59-0210201816556-4.jpg');

INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (65, '70', 'T70-0210201816568-0.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (66, '70', 'T70-0210201816568-1.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (67, '70', 'T70-0210201816568-2.jpg');

INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (70, '80', 'T80-0210201816578-0.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (71, '80', 'T80-0210201816578-1.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (72, '80', 'T80-0210201816578-2.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (73, '80', 'T80-0210201816578-3.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (74, '80', 'T80-0210201816578-4.jpg');

INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (75, '90', 'T89-0210201816587-0.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (76, '90', 'T89-0210201816587-1.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (77, '90', 'T89-0210201816587-2.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (78, '90', 'T89-0210201816587-3.jpg');
INSERT INTO `photos` (`id`, `post_id`, `image`) VALUES (79, '90', 'T89-0210201816587-4.jpg');

-- Created y updated, fechas necesarias para la actualizacion en android
update posts set updated_at = CURRENT_TIMESTAMP;
update posts set created_at = CURRENT_TIMESTAMP;

-- Nuevo rol para administrar eventos
insert into user_has_role(user_id, role_id)
select 1, (select id from roles where id_str = 'EVENT_MANAGER');

-- reportes
insert into reports (tourist_unique_identificator, message, post_id, created_at, updated_at, _status)
select FLOOR(RAND()*(9999999999999-1111111111111+1))+1111111111111, REPEAT(ELT(0.5 + RAND() * 17, '. ', ',', 'no ', 'funciona ', 'atencion ', 'mala ', 'horarios ', 'baños ', 'sucios ', 'feo ', 'olor ', 'piso ', 'cerrado ', 'lugar ', 'el ', 'la ', 'en '), FLOOR(RAND()*(55-12+1))+12), (SELECT id FROM posts ORDER BY RAND() LIMIT 1), CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ELT(0.5 + RAND() * 2, 'seen', 'not_seen');



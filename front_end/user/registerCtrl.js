app.controller("RegisterCtrl",["$scope","UserSrv", function($scope, UserSrv){

    $scope.newUser = {};



    $scope.validate = function () {
           if (!$scope.newUser.name) return false;
           if (!$scope.newUser.email) return false;
           if (!UserSrv.validateEmail($scope.newUser.email)) return false;
           return true;
    }

    $scope.register = function () {
        UserSrv.registerAdmin($scope.newUser, function(err,resp){
            if(err){
                return alert("Error!");
            }

            if (resp != 'ok'){
                alert(resp.message);
            } else {
                alert("Usuario registrado correctamente. ")
                $scope.newUser = {};
            }

        });
    }



}]);

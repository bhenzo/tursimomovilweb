app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  $routeProvider
    .when('/login', {
      templateUrl: 'app/html/user/login.html',
      controller: 'LoginCtrl',
    })
    .when('/client_manager', {
      templateUrl: 'app/html/user/client_manager.html',
      controller: 'ClientManagerCtrl',
    })
    .when('/signup', {
      templateUrl: 'app/html/user/signup.html',
      controller: 'SignUpCtrl',
    })
    .when('/change_password', {
      templateUrl: 'app/html/user/changePassword.html',
      controller: 'ChangePasswordCtrl',
    })
    .when('/register_admin', {
      templateUrl: 'app/html/user/register.html',
      controller: 'RegisterCtrl',
    })
    .when('/account_info',{
      templateUrl: 'app/html/user/add_accountData.html',
      controller: 'AccountDataCtrl',
    });
}]);

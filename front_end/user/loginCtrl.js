app.controller("LoginCtrl", ["$scope", "$location", "UserSrv", '$rootScope', 'dialogs', function($scope, $location, UserSrv, $rootScope, dialogs){

  $scope.data = {}

  $scope.login = function(){
    UserSrv.login($scope.data, function(err, resp){
        if (err){
          return alert("Ocurrio un error :(");
        }

        if (resp.user){
          $rootScope.user = resp.user;
          $location.path("/post_list");
        } else {
          dialogs.notify("Aviso", resp.message, {animation: true, size: 'md'});
        }


    });
  }

  $scope.isValid = function(){
      if (!$scope.data.user) return false;
      if (!$scope.data.password) return false;
      if (!($scope.data.user.length >= 6)) return false;
      if (!($scope.data.password.length >= 8)) return false;
      return true;
  }

}]);

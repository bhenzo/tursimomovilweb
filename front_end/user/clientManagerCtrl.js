app.controller("ClientManagerCtrl", ["$location", "$scope", "UserSrv", "PostsSrv", "dialogs", function ($location, $scope, UserSrv, PostsSrv, dialogs) {
  $scope.clients = [];
  $scope.filter = {
    'mode': 'register_date'
  };
  $scope.paymentMethods = [];

  $scope.posts = [];

  //Veo la ruta si tiene algun orden de los clientes  guardado
  if ($location.search().order){
    $scope.filter.mode = $location.search().order;
  }

  $scope.activatePost = (user, post) => {
    dialogs.create('app/html/posts/activate_post_modal.html','ActivatePostModalCtrl',{'post': post},{animation:true, size:'lg'},'Ctrl')
    .result.then(() => {
       $scope.getPostsUser(user);
        return true;
    }).then(() => {
        return true;
    });
  }


  //guardar cliente (por si se le asigno que puede ser administrador de eventos)
  $scope.save = (client) => {
      UserSrv.save(client, (err, resp) => {
        if (err){
          return;
        }
        client.edited = false;
      });
  }

  $scope.getClients = function () {
    //Guardo el filtro en el path 
    $location.search('order', $scope.filter.mode);


    UserSrv.getClients($scope.filter.mode, function (err, resp) {
      if (err) {
        return dialogs.error("Error", "Ocurrio un error :(");
      }

      $scope.clients = resp;
    });
  }
  $scope.getClients();

  $scope.getPostsUser = function (user) {
    PostsSrv.getPostsUser(user.id, function (err, resp) {
      if (err) {
        return alert("Error!" + err);
      }
      user.posts = resp;
     
    });
  }

  $scope.openReports = (user) => { 
    dialogs.create('app/html/reports/reports_modal.html','PostReportsModalCtrl',{'userId': user.id},{animation:true, size:'lg'},'Ctrl')
    .result.then(() => {
        return true;
    }).then(() => {
        return true;
    });

  }

  $scope.deletePostUser = function (user, postId) {
    dialogs.confirm('Confirme', 'Está seguro que desea bloquear la publicacion?', {
        animation: true,
        size: 'md'
      })
      .result.then(() => {
        PostsSrv.blockPost(postId, function (err, resp) {
          if (err) {
            return alert("Error al eliminar una publicacion" + err);
          }
          if (resp == 'ok') {
            $scope.getPostsUser(user);
            dialogs.notify("Exito", "La publicacion se ha bloqueado correctamente", {
              animation: true,
              size: 'md'
            });
          }

        });
      }).then((err) => {

      });
  }


  UserSrv.getPaymentMethods(function (err, resp) {
    if (err) {
      return dialogs.error("Error", "Ocurrio un error :(");
    }

    $scope.paymentMethods = resp;
  });

  //Activacion de cuenta cuando apreta boton
  $scope.enableAccount = function (account) {
    UserSrv.activateAccount(account, function (err, resp) {
      if (err) {
        return dialogs.error("Error", "Ocurrio un error :(");
      }
      $scope.getClients();
    });
  }

  //Desactivacion de la cuenta. Comprueba si hay publicaciones
  $scope.disableAccount = function (account) {
    dialogs.confirm('Confirme', 'Está seguro que desea desactivar esta cuenta?', {
        animation: true,
        size: 'md'
      })
      .result.then(() => {
        UserSrv.disableAccount(account.id, false, function (err, resp) {
          if (err) {
            return dialogs.error('Error', 'Ocurrio un error al desactivar cuenta');
          }
          if (resp == 'ok') {
            dialogs.notify("Exito", "La cuenta se ha desactivado correctamente", {
              animation: true,
              size: 'md'
            });
          } else {
            dialogs.confirm('Atención!', resp + '<br><br>Desea forzar la desactivacion?', {
                animation: true,
                size: 'md'
              })
              .result.then(() => {
                UserSrv.disableAccount(account.id, true, function (err, resp) {
                  if (err) {
                    return dialogs.error('Error', 'Ocurrio un error al desactivar cuenta');
                  }
                  dialogs.notify("Exito", "La cuenta se ha desactivado correctamente", {
                    animation: true,
                    size: 'md'
                  });

                  $scope.getClients();

                });
              }).then((err) => {

              });
          }

        })
      }).then((err) => {

      });

  }


}]);
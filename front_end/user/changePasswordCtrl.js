app.controller("ChangePasswordCtrl", ["$scope", "UserSrv", "dialogs", function ($scope, UserSrv, dialogs) {

    $scope.data = {};

    $scope.changePassword = function () {
        UserSrv.changePassword($scope.data, function (err, resp) {
            if (err) {
                return alert("Error!");
            }
            dialogs.notify("Hecho", "Cambios realizados", {
                animation: true,
                size: "sm"
            });
        });
    }

    $scope.validate = function () {
        if (!$scope.data.password) return false;
        if (!$scope.data.newPassword) return false;
        if ($scope.data.newPassword.length < 8) return false;
        if (!$scope.data.rptNewPassword) return false;
        if ($scope.data.rptNewPassword.length < 8) return false;
        if ($scope.data.newPassword != $scope.data.rptNewPassword) return false;
        return true;
    }
}]);
app.factory('UserSrv', ['$http', '$rootScope', 
function ($http, $rootScope) {
    return {
        refreshSession: function (callback) {
            $http.get('/session').then(function (resp) {
                $rootScope.user = resp.data;
            }, function (err) {
            });
        },
        login: function (data, callback) {
            $http.post('/login', {
                'user': data.user,
                'password': data.password
            }).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getLoggedUser: function (callback) {
            $http.get('/session').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        logout: function (callback) {
            $http.post('/logout').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        saveAccountData: function (data, callback) {
            $http.post('/account_info', {
                'data': data
            }).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getAccountData: function (callback) {
            $http.get('/account_info').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        refreshNotifications: () => {
            $http.get('/notifications/payments_not_billed').then(function (resp) {
                $rootScope.paymentsNotBilled = resp.data;
            }, function (err) {
            });
        },
        changePassword: function (data, callback) {
            $http.post('/change_password', {
                'password': data.password,
                'newPassword': data.newPassword,
                'rptNewPassword': data.rptNewPassword
            }).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getRoles: function (callback) {
            $http.get('/getRoles').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        checkEmailUssage: function (email, callback) {
            $http.get('/email_exists/' + email).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        save: (user, callback) => {
            $http.post('/users', user).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getClients: function (filterMode, callback) {
            $http.get('/clients?filter_mode=' + filterMode).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getPaymentMethods: function (callback) {
            $http.get('/payment_methods').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        registerAdmin: function (data, callback) {
            $http.post('/register_admin', {
                'data': data
            }).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        activateAccount: function (account, callback) {
            $http.post('/users/' + account.id + '/activate', account).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        disableAccount: function (accountId, force, callback) {
            $http.post('/users/' + accountId + '/disable', {'force' : force}).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        registerClient: function (data, callback) {
            $http.post('/register_client', data).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        validateEmail: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        },
      
    };
}]);

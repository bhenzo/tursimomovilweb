app.controller("AccountDataCtrl", ["$scope", "UserSrv", "$rootScope", function ($scope, UserSrv, $rootScope) {

    $scope.accountData = {};
    $scope.pattern = '\\d\\d-\\d{8}-\\d';

    $scope.saveAccountData = function () {
        UserSrv.saveAccountData($scope.accountData, function (err, resp) {
            if (err) {
                return alert("error!");
            }
            alert("Datos guardados");
            $rootScope.getSession();
        });
    }

    $scope.validate = function () {
        if (!$scope.accountData.cuit) return false;
        if (!$scope.accountData.bussiness_name) return false;
        if (!$scope.accountData.phone) return false;
        if (!$scope.accountData._address) return false;
        if (!$scope.accountData.address_number) return false;
        if (!$scope.accountData.postal_code) return false;
        if (!$scope.accountData.city) return false;
        if (!$scope.accountData.province) return false;
        return true;
    }

    $scope.getAccountData = function () {
        UserSrv.getAccountData(function (err, resp) {
            if (err) {
                return alert("Error!" + err);
            }
            if (resp == 0) {
                $scope.accountData = {};
            } else {
                $scope.accountData = resp;
            }
        })
    }
    $scope.getAccountData();

}]);

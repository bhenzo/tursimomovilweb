app.controller("SignUpCtrl", ["$scope", "$location", "UserSrv", '$rootScope', 'dialogs', function($scope, $location, UserSrv, $rootScope, dialogs){

    $scope.emailInUse = false;
    $scope.newUser = {};

    $scope.checkEmailUssage = function(){
        if (!$scope.newUser.email || !UserSrv.validateEmail($scope.newUser.email)){
          return false;
        }
        UserSrv.checkEmailUssage($scope.newUser.email, function(err, resp){
          if (err){
            return alert("Error");
          }
          $scope.emailInUse = resp == 'in_use';
        })
    }

    $scope.valid = function(){
      if (!$scope.newUser.email || !UserSrv.validateEmail($scope.newUser.email)){
        return false;
      }
      if ($scope.emailInUse) return false;
      if (!$scope.newUser.name) return false;
      if (!$scope.newUser.lastname) return false;
      if (!$scope.newUser.password || $scope.newUser.password.length < 8 ||
        $scope.newUser.password != $scope.newUser.passwordConfirmation) return false;
      return true;
    }

    $scope.signup = function(){
        UserSrv.registerClient($scope.newUser, function(err, resp){
            if (err){
              return dialogs.error("ERROR", "Error al registrarse, intente despues.", {size: 'sm', animation: true});
            }
            if (resp != 'ok'){
              return dialogs.error("Error", resp, {size: 'sm', animation: true});
            }

            dialogs.notify('Exito', 'Usted ha sido registrado en la plataforma. Por favor, espere a que algun administrador confirme su cuenta.', {size: 'md', animation: true});
            $location.path('/');
        });

    }

}]);

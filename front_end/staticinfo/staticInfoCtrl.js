app.controller("StaticInfoCtrl", ["LanguagesSrv", "$location", "$scope", "ZonesSrv", "StaticInfoSrv",
    "dialogs", "$routeParams", function (LanguagesSrv, $location, $scope, ZonesSrv, StaticInfoSrv, dialogs, $routeParams) {

        $scope.modules = [];
        $scope.selectedClass = {
            idstr: 'title'
        };
        $scope.langsettings = {
            selectedTab: 0
        }

        LanguagesSrv.getLanguages((err, resp) => {
            if (err) {
                return;
            }
            $scope.languages = resp;
        });

        $scope.checkTranslationExists = {
            'title': function (langId, module) {
                return module.mdata[langId] && module.mdata[langId].title;
            },
            'subtitle': function (langId, module) {
                return module.mdata[langId] && module.mdata[langId].subtitle;
            },
            'p': function (langId, module) {
                return module.mdata[langId] && module.mdata[langId].p;
            },
            'list': function (langId, module) {
                return module.mdata[langId] && module.mdata[langId].list;
            },
            'table': function (langId, module) {
                if (!module.mdata[langId]) {
                    return false;
                }
                let exists = false;
                let keys = Object.keys(module.mdata[langId]);
                keys.forEach(k => {
                    if (module.mdata[langId][k] && module.mdata[langId][k].length > 0) {
                        exists = true;
                    }
                })
                return exists;
            },
            'image': function(langId, module){
                //Siempre existe, porque es una imagen
                return true;
            }
        };

        $scope.classes = [
            {
                'name': 'Título',
                'template': {
                    'type': 'title'
                },
                'mdata': {}
            },
            {
                'name': 'Subtítulo',
                'template': {
                    'type': 'subtitle'
                },
                'mdata': {}
            },
            {
                'name': 'Párrafo',
                'template': {
                    'type': 'p'
                },
                'mdata': {}
            },
            {
                'name': 'Lista',
                'template': {
                    'type': 'list'
                },
                'mdata': {}
            },
            {
                'name': 'Tabla',
                'template': {
                    'type': 'table',
                    'rows': 3,
                    'cols': 3
                },
                'mdata': {}
            },
            {
                'name': 'Imagen',
                'template': {
                    'type': 'image'
                },
                'mdata': {}

            }
        ];

        $scope.saveEdit = () => {
            $scope.modules[$scope.selectedClass.index] = $scope.selectedClass;
            delete $scope.modules[$scope.selectedClass.index].index;
        }

        $scope.cancelEdit = () => {
            $scope.selectedClass = angular.copy($scope.classes[0]);

        }

        $scope.edit = (module, index) => {
            let modulecopy = angular.copy(module);
            modulecopy.index = index;
            $scope.selectedClass = modulecopy;
        }

        $scope.addRow = () => {
            $scope.selectedClass.template.rows++;
            //let newRow = [];
            //$scope.selectedClass.table[0].forEach(it => newRow.push({text:''}));
            //$scope.selectedClass.table.push(newRow);
        }
        $scope.removeRow = () => {
            $scope.selectedClass.template.rows--;

            //$scope.selectedClass.table.pop();
        }
        $scope.addCol = () => {
            $scope.selectedClass.template.cols++;

            // $scope.selectedClass.table.forEach(it => it.push({text:''}));
        }
        $scope.removeCol = () => {
            $scope.selectedClass.template.cols--;

            //$scope.selectedClass.table.forEach(it => it.pop());
        }

        $scope.removeModule = (index) => {
            dialogs.confirm("¿Esta seguro?", "¿Esta seguro que desea borrar este modulo?", { animation: true, size: 'md' })
                .result.then(() => {
                    $scope.modules.splice(index, 1);
                    return true;
                }, () => {
                    return true;
                });

        }

        $scope.parseList = (ls) => {
            if (!ls) {
                return [];
            }
            return ls.split("\n");
        }

        $scope.add = () => {
            $scope.modules.push($scope.selectedClass);
            $scope.selectedClass = angular.copy($scope.classes[0]);
        }

        $scope.selectClass = (cl) => {
            $scope.selectedClass = angular.copy(cl);
        }

        $scope.move = (index, where) => {
            if (index + where >= $scope.modules.length) return false;
            if (index + where < 0) return false;
            let aux = $scope.modules[index];
            $scope.modules[index] = $scope.modules[index + where];
            $scope.modules[index + where] = aux;
        }

        $scope.openGallery = () => {
            dialogs.create('app/html/staticinfo/photo_gallery_modal.html', 'PhotoGalleryModalCtrl', {}, { animation: true, size: 'lg' }, 'Ctrl')
                .result.then((data) => {
                    $scope.selectedClass.template.image = data.image_url;
                }, () => {
                    return true;
                });
        }

        $scope.staticinfo = {
            html: ''
        };
        $scope.zones = [];
        $scope.availableImages = [];

        if ($routeParams.id) {
            //Se esta editando alguno. Traerlo.
            StaticInfoSrv.getStaticInfo($routeParams.id, (err, resp) => {
                if (err) {
                    return;
                }
                $scope.staticinfo = resp;
                $scope.modules = [];
                resp.modules.forEach(mod => {
                    //let langIds = Object.keys(mod.mdata);
                    //langIds.forEach(k => $scope.modules[k] = JSON.parse(mod.mdata[k]));
                    let parsedMdata = {};
                    let langIds = Object.keys(mod.mdata);
                    langIds.forEach(k => parsedMdata[k] = mod.mdata[k] == "" ? {} : JSON.parse(mod.mdata[k]));
                    let parsedMod = angular.copy(mod);
                    parsedMod.mdata = parsedMdata;
                    parsedMod.template = JSON.parse(mod.template);
                    $scope.modules.push(parsedMod);
                });
                console.log($scope.modules);
            });
        }



        ZonesSrv.getZones((err, resp) => {
            if (err) {
                return;
            }
            $scope.zones = resp;
        });

        $scope.valid = () => {
            if (!$scope.staticinfo.zone_id) return false;
            if (!$scope.staticinfo.title) return false;
            if ($scope.modules.length == 0) return false;

            return true;
        }



        $scope.save = () => {
            //Stringifico cada elemento, que es lo que voy a guardar en la base de datos
            let modulestr = [];
            let morder = 0;
            $scope.modules.forEach(mod => {
                morder++;
                let mdataStr = {};
                let mdataKeys = Object.keys(mod.mdata);
                mdataKeys.forEach(k => mdataStr[k] = JSON.stringify(mod.mdata[k]));

                modulestr.push({
                    'template': JSON.stringify(mod.template),
                    'mdata': mdataStr,
                    'morder': morder
                });
            });
            $scope.staticinfo.modules = modulestr;

            StaticInfoSrv.save($scope.staticinfo, (err, resp) => {
                if (err) {
                    return;
                }
                dialogs.notify("Exito", "La información de zonas se ha guardado correctamente", { animation: true, size: 'sm' });
                $location.path('/static_info/' + resp);
            })
        }

    }]);

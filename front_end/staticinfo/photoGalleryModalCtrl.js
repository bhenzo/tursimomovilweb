app.controller("PhotoGalleryModalCtrl", ["dialogs", "$scope", "StaticInfoSrv", "$uibModalInstance", "data",
 function (dialogs, $scope, StaticInfoSrv, $modalInstance, data) {
  

    //TODO Considerar multiples imagenes (array) en un futuro
    $scope.$watch('image', () => {
        if (!$scope.image){
            return;
        }
        let valid = false;
        ['image/jpeg', 'image/png'].forEach(type => type == $scope.image.filetype ? valid = true:false);
        if (!valid){
            dialogs.error("Error", "ingrese una imagen con formato JPEG o PNG");
            return;
        }
        //Subo imagen y limpio
        StaticInfoSrv.uploadImage($scope.image, (err, resp) => {
            if (err){
                return dialogs.error("Error", "Ocurrio un error al cargar la imagen",{animation: true, 'size':'sm'});
            }
            $scope.image = null;
            $scope.getGallery();

        })
        //console.log($scope.image);
    });

    $scope.getGallery = () => {
        StaticInfoSrv.getGallery((err, resp) => {
            if (err){
                return;
            }
            $scope.availableImages = resp;
        });
    }

    $scope.getGallery();

    $scope.selectImage = (img) => {
        $modalInstance.close({'image_url':img});
    }

    $scope.close = function(){
        $modalInstance.dismiss('Canceled');
    }; 
    
    
}]);
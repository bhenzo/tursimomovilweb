app.factory('StaticInfoSrv', ['$http', function ($http) {
    return {
        save: function (data, callback) {
            $http.post('/static_info', data).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getStaticInfo: function (id, callback) {
            $http.get('/static_info/' + id).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getStaticInfoList: function (callback) {
            $http.get('/static_info/list').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        uploadImage: (img, callback)  => {
            $http.post('/static_info/gallery', img).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getGallery: function (callback) {
            $http.get('/static_info/gallery').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        }
    };
}]);
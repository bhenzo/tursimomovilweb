app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/static_info', {
    templateUrl: 'app/html/staticinfo/staticinfo.html',
    controller: 'StaticInfoCtrl',
  }),
  $routeProvider
   .when('/static_info/list', {
    templateUrl: 'app/html/staticinfo/list.html',
    controller: 'StaticInfoListCtrl',
  }),
  $routeProvider
   .when('/static_info/:id', {
    templateUrl: 'app/html/staticinfo/staticinfo.html',
    controller: 'StaticInfoCtrl',
  })
}]);

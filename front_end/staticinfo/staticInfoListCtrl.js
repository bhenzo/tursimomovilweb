app.controller("StaticInfoListCtrl", ["$location", "$scope", "ZonesSrv", "StaticInfoSrv", 
"dialogs", "$routeParams", function ($location, $scope, ZonesSrv, StaticInfoSrv, dialogs, $routeParams) {

    $scope.zones = {};
    $scope.statics_infos = [];

    ZonesSrv.getZones((err, resp) => {
        if (err){
            return;
        }
        //Lo meto como si fuera un hashmap id -> zone
        for(let i = 0; i < resp.length; i++){
            $scope.zones[resp[i].id] = resp[i];
        }
    });
    $scope.getInfoList = () => {
        StaticInfoSrv.getStaticInfoList((err, resp) => {
            if (err) {
                return;
            }
    
            $scope.statics_infos = resp;
        });
    
    }
    $scope.getInfoList();
    $scope.edit = (sinfo) => {
        $location.path('/static_info/' + sinfo.id);
    }

    $scope.delete = (sinfo) => {
        ZonesSrv.deleteStaticInfo(sinfo.id, (err, resp) => {
            if (err){
                return;
            }
            $scope.getInfoList();
        });
    }

    $scope.newInfo = () => {
        $location.path('/static_info');
    }


}]);
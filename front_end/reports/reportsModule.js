app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/reports', {
    templateUrl: 'app/html/reports/reports.html',
    controller: 'ReportsCtrl',
    reloadOnUrl: false
  })
 
}]);

app.controller("ReportsCtrl", ["$routeParams", "$location", "$scope", "PostsSrv", function ($routeParams, $location, $scope, PostsSrv) {
  
    $scope.filter = {
        text:'',
        order:'status',
        order_mode:'asc',
        page: 1
    };
    //Armo la ruta si esta vacia
    if (!$routeParams.page){
        $location.search($scope.filter);
    } else {
        $scope.filter = $routeParams;
    }


    $scope.data = {
        reports: []
        };

    $scope.search = () => {

        $location.search($scope.filter);
        $scope.getReports();

    }

    $scope.getReports = () => {
        //Este workaround es para que $routeparams se actualice si se cambio
        //En esta misma ejecucion de codigo

        setTimeout(() => {

            PostsSrv.getReports($routeParams.text, $routeParams.order,
                $routeParams.order_mode, $routeParams.page, (err, resp) => {
                if (err){
                    return;
                }
                $scope.data = resp;
            });
        }, 0);
      
    }
    $scope.getReports();

    $scope.showPost = (rep) => {
        $location.search({});
        $location.path('/posts/' + rep.post_id);
    }

    $scope.markAsReaded = (rep) => {
        PostsSrv.markReportAsReaded(rep.id, (err, resp) => {
            if (err){
                return;
            }
            $scope.getReports();
        });
    }
    
}]);
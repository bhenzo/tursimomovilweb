app.controller('MainCtrl', ['$rootScope', '$scope', 'PromosSrv', '$location', function($rootScope, $scope, PromosSrv, $location){
   
    $scope.promos = {
        'silver':[],
        'gold': []
    };

    PromosSrv.getPromos((err, resp) => {
        if (err){
            return;
        }
        $scope.promos = {
            'silver':[],
            'gold': []
        };
        resp.forEach(pr => $scope.promos[pr.post_type].push(pr));
        
    });

    

}])

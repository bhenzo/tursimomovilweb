app.factory('ZonesSrv', ['$http', function ($http) {
    return {

        saveZone: function (data, callback) {
            $http.post('/zones', {
                'data': data
            }).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },

        getZones: function (callback) {
            $http.get('/zones').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },

        deleteZone: function (id, callback) {
            $http.post('/zones/remove/' + id).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        deleteStaticInfo: function (id, callback) {
            $http.post('/static_info/remove/' + id).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
    };
}]);
app.controller("zoneListCtrl", ["$scope", "ZonesSrv", "dialogs", function ($scope, ZonesSrv, dialogs) {
    console.log("ZoneList");

    $scope.getZones = function () {
        ZonesSrv.getZones(function (err, resp) {
            if (err) {
                return alert("ocurrio un error" + err);
            }
            $scope.zones = resp;
        })
    }
    $scope.getZones();

    
    $scope.deleteZone = function (id) {
        ZonesSrv.deleteZone(id, function (err, resp) {
            if (err) {
                alert("Ocurrio un error al eliminar zona" + err);
            }
            if (resp == 'ok'){
                $scope.getZones();
            } else {
                console.log(resp);
                dialogs.error("Error", resp.message, {animation: true, size:'md'});
            }
            
        })
    }
}]);
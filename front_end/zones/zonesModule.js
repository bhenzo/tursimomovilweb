app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/add_zone', {
            templateUrl: 'app/html/zones/add_zone.html',
            controller: 'AddZoneCtrl',
        })
        .when('/zone_list', {
            templateUrl: 'app/html/zones/zone_list.html',
            controller: 'zoneListCtrl',
        });
}]);
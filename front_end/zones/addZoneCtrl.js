app.controller("AddZoneCtrl", ["$scope", "ZonesSrv", function ($scope, ZonesSrv) {
    $scope.zone = {};

    $scope.validate = function(){

            if(!$scope.zone._name)return false;
            if (!$scope.zone._description) return false;

            return true;
    }

    $scope.saveZone = function(){
        ZonesSrv.saveZone($scope.zone,function(err,resp){
            if(err){
                return alert("error!");
            }
            alert("Zona guardada");
            $scope.zone = {};
        });
    }


}]);
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/languages', {
    templateUrl: 'app/html/languages/languages.html',
    controller: 'LanguagesCtrl'
  })
 
}]);

app.factory('LanguagesSrv', ['$http', function ($http) {
    return {
        getTranslationItems: (callback) => {
            $http.get('/languages/translation_items').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getLanguages: (callback) => {
            $http.get('/languages').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getTranslation: (languageId, callback) => {
            $http.get('/languages/translation/' + languageId).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getNativeTranslation: (callback) => {
            $http.get('/languages/translation/native').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        saveTranslation: (lang, callback) => {
            $http.post('/languages/translation', lang).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        setNative: (langId, callback) => {
            $http.post('/languages/' + langId + '/native').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        saveLanguagePreference: (idsInOrder, callback) => {
            $http.post('/languages/language_preference', {'ids' : idsInOrder}).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        removeLanguage: (langId, callback) => {
            $http.post('/languages/remove', {'id': langId}).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        }
    };
}]);
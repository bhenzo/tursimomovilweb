app.controller("LanguagesCtrl", ["$rootScope", "LanguagesSrv", "$scope", "dialogs", "$routeParams", "$timeout",
function ($rootScope, LanguagesSrv, $scope, dialogs, $routeParams, $timeout) {
    $scope.languages = [];
    $scope.selected = null;
    $scope.nativeTranslation = {};
    //Keys almacena las claves que se deben traducir
    $scope.keys = [];

    $scope.getNative = () => {
        LanguagesSrv.getNativeTranslation((err, resp) => {
            if (err){
                return;
            }
            $scope.nativeTranslation = resp;
        })
    }
    $scope.getNative();

    $scope.getLanguages = () => {
        LanguagesSrv.getLanguages((err, resp) => {
            if (err) {
                return dialogs.err("Error", "Ocurrió un error, intentelo nuevamente mas tarde");
            }

            $scope.languages = resp;
            $scope.selected = null;

        })
    }
    $scope.getLanguages();
    $scope.getTranslationItems = () => {
        LanguagesSrv.getTranslationItems((err, resp) => {
            if (err) {
                return dialogs.error("Error", "Ocurrió un error, intentelo nuevamente mas tarde");
            }
            $scope.keys = resp;
        })
    }

    $scope.getTranslationItems();

    $scope.createLanguage = () => {
        $scope.selected = {
            name: 'Nuevo lenguaje',
            edit: true,
            translations: angular.copy($scope.keys)
        };
        $scope.languages.push($scope.selected);



        //Seleccionar el input para escribir el nuevo nombre
        $timeout(() => {
            let name = '#lang-item-'+($scope.languages.length - 1);
            $(name).focus()
            $(name).select()
        }, 0);

    }

    $scope.removeLanguage = (langId, name) => {
        dialogs.confirm("Confirme","Esta seguro que desea eliminar el lenguaje '" + name + "' y todas las traducciones asociadas?")
        .result.then(() => {
           LanguagesSrv.removeLanguage(langId, (err, resp) => {
               if (err){
                   return;
               }
               $scope.getLanguages();
               $rootScope.refreshNotifications();
           });
        });
    }

    $scope.swapLang = (index, dir) => {
        let tmp = $scope.languages[index];
        $scope.languages[index] = $scope.languages[index + dir];
        $scope.languages[index + dir] = tmp;
    }

    $scope.saveLanguagePreference = () => {
        let langOrder = [];
        $scope.languages.forEach(l => langOrder.push(l.id));
        LanguagesSrv.saveLanguagePreference(langOrder, (err, resp) => {
            if (err){
                return;
            }
            dialogs.notify("Exito","Se guardo la preferencia", {'size': 'md', 'animation':true});

        });
    }

    $scope.valid = (lang) => {
        //Es valido si existe almenos 1 elemento traducido
        let valid = false;
        if (!lang || !lang.translations){
            return false;
        }        
        lang.translations.forEach(i => {
            if (i.translation){
                valid = true;
            }
        })
        return valid;
        /*if (lang.missing_translations){
            return false;
        }
        let missing = lang.translations.find(e => !e.translation ? e : null);
        return missing ? false : true;*/
    }

    $scope.setNative = (lang) => {
        LanguagesSrv.setNative(lang.id, (err,resp) => {
            if (err){
                return;
            }
            $scope.getLanguages();
            $scope.getNative();
        });
    }

    $scope.save = (lang) => {
        LanguagesSrv.saveTranslation(lang, (err, resp) => {
            if (err){
                return;
            }        
            $scope.getLanguages();
            $scope.getNative();
            $rootScope.refreshNotifications();

        });
    }

    $scope.select = (lang) => {
        $scope.selected = lang;

        //Pongo los translations items
        //$scope.selected.translations = $scope.keys;
    }

}]);

app.controller('HeaderCtrl', ['$rootScope', '$scope', 'UserSrv', '$location', function($rootScope, $scope, UserSrv, $location){
    $scope.logout = function(){
        UserSrv.logout(function(err, resp){
            if (err){
              return alert("Ocurrio un error al cerrar sesión");
            }
            $rootScope.user = null;
            $location.path('/login');
        });
    }


    UserSrv.refreshNotifications();
    

}])

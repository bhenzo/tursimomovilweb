app.controller("PromosCtrl", ["PromosSrv", "$scope", "dialogs", "$location", function (PromosSrv, $scope, dialogs, $location) {
    
    $scope.allPromos = {
      'silver': [],
      'gold': []
    };

    let setPromos = (resp) => {
      $scope.allPromos = {
        'silver': [],
        'gold': []
      };    
      resp.forEach(promo => $scope.allPromos[promo.post_type].push(promo));
    }

    $scope.getPromos = () => {
      PromosSrv.getPromos((err, resp) => {
        if (err){
          return;
        }
        setPromos(resp);
        console.log($scope.allPromos);
      });
    }
    $scope.getPromos();
    //Editar promo
    $scope.edit = (promo) => {
      promo.editing = true;
      promo.edited = true;
      promo.days = promo.time / (60 * 60 * 24);
    }
    //agregar promo al array 
    $scope.newPromo = (postType, arr) => {
      arr.push({
        editing: true,
        post_type: postType
      });
    }
    
    //Borro la promo
    $scope.removePromo = (arr, index) => {
      arr.splice(index, 1);
    }
    
    //Guardar promos 
    $scope.save = () => {
      $scope.validationMessages = $scope.getValidationMessages();
      if ($scope.validationMessages.length > 0)
        return;
      PromosSrv.savePromos($scope.allPromos, (err, resp) => {
        if (err){
          return;
        }

        $scope.getPromos();
      });
    }

    //Genero mensajes de error.
    $scope.getValidationMessages = () => {
      let msgs = [];
      Object.keys($scope.allPromos).forEach(key => {
        //en key tengo cada tipo de promo (silver, gold)
        $scope.allPromos[key].forEach(pr => {
            if (!pr.name) {
              msgs.push("Hay una promo sin nombre de tipo " + key);
            } else {
              console.log(pr);
              if (!pr.time || pr.time <= 0){
                msgs.push("Ingrese correctamente la cantidad de dias en '"+pr.name+"'");
              }
              if (!pr.price || pr.price <= 0){
                msgs.push("Ingrese un precio válido para '"+pr.name+"'");
              }
            }
            
        });
        if ($scope.allPromos[key].length == 0){
          msgs.push("Debe haber por lo menos, una promo '"+key+"'");  
        }
      });
      return msgs;
    }


}]);

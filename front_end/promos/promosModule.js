app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
    .when('/promos', {
    templateUrl: 'app/html/promos/promos.html',
    controller: 'PromosCtrl',
  });
}]);

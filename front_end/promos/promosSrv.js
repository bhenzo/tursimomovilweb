app.factory('PromosSrv', ['$http', function ($http) {
    return {
        getPromos: function (callback) {
            $http.get('/promos').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        savePromos: function (promos, callback) {
            $http.post('/promos', promos).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        }
    };
}]);

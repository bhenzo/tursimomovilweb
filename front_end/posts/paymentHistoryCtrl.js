app.controller("PaymentHistoryCtrl",["$scope", "PostsSrv", function($scope,PostSrv){

    $scope.history = [];

    
    $scope.getPaymentHistory = function () {
        PostSrv.getPaymentHistory(function (err, resp) {
            if(err){
                return alert("error al recuperar historial de pagos");
            }
            $scope.history = resp;
        })
    }
    $scope.getPaymentHistory();


}]);
app.controller("SelectCategoryModalCtrl", ["$scope", "CategoriesSrv", "$uibModalInstance", "data", function ($scope, CategoriesSrv, $modalInstance, data) {
  
    $scope.userId = data.userId;
    $scope.list = [];
    $scope.post = data.post;
    $scope.selecteds = [];
    $scope.catsByPostType = {};

    $scope.selectCategory = (cat) => {

        if ($scope.selecteds.length >= $scope.catsByPostType[data.post.type].max_categories){
            return;
        }


        $scope.selecteds.push(cat);
        cat.selected = true;
    }

    $scope.removeCategory = (cat) => {
        $scope.selecteds = $scope.selecteds.filter(elem => elem.id != cat.id);
        cat.selected = false;
    }


    let catType = data.post.type == 'event' ? 'event' : 'normal';
    console.log(data);
    CategoriesSrv.getCategories(catType, (err, resp) => {
        if (err){
            return;
        }
        $scope.list = resp;

    });

    CategoriesSrv.getCatsByPostType((err, resp) => {
        if (err){
            return;
        }
        $scope.catsByPostType = resp;
    });

    $scope.done = () => {
        $modalInstance.close();
    }

    $scope.accept = function(){
        $modalInstance.close({'categories' : $scope.selecteds});
       // $modalInstance.dismiss({'category' : $scope.category.selected});
    }; 
    
    
}]);
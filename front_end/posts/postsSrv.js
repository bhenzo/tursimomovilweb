app.factory('PostsSrv', ['$http', function ($http) {
    return {
        savePost: function (data, callback) {
            $http.post('/posts', {
                'data': data
            }).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },

        getPost: function (id, callback) {
            $http.get('/posts/'+id).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },

        getPostList: function (callback) {
            $http.get('/posts').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },

        getPostsUser: function (user_id,callback) {
            $http.get('/posts/getPostsUser/' + user_id).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        checkLink: function (link,callback) {
            $http.post('/check_link', {'link' : link}) .then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },

        blockPost: function (post_id, callback) {
            $http.post('/posts/block/' + post_id).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        
        deletePost: function (id, callback) {
            $http.post('/posts/remove/' + id).then(function () {
                callback(null);
            }, function (err) {
                callback(err);
            });
        },
        markReportAsReaded: (reportId, callback) => {
            $http.post('/reports/' + reportId + "/read").then(function () {
                callback(null);
            }, function (err) {
                callback(err);
            });
        },
        activatePost: (postId, activation, callback) => {
            $http.post('/posts/' + postId + "/activate", {'activation': activation}).then(function () {
                callback(null);
            }, function (err) {
                callback(err);
            });
        },

        getPaymentHistory: function (callback) {
            $http.get('/payment_history').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getReports:  (filter, orderby, ordermode, page, callback) => {
            $http.get('/reports?page=' + page + "&filter="+filter+"&order_by=" + orderby + "&order_mode=" +
            ordermode).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        }
    };
}]);
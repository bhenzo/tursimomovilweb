app.controller("postListCtrl", ["dialogs", "$scope", "PostsSrv", "$interval", function (dialogs, $scope, PostsSrv, $interval) {
  console.log("PostList");


  $scope.getPostList = function () {
    PostsSrv.getPostList(function (err, resp) {
      if (err) {
        return alert("Error!" + err);
      }
      $scope.posts = resp;
    })
  }

  $scope.getPostList();

  $interval(function () {
    if (!$scope.posts)
      return;
    for (var i = 0; i < $scope.posts.length; i++) {
      var p = $scope.posts[i];
      p.secondsToExpire--;
      if (p.secondsToExpire < 0) {
        p.secondsToExpire = 0;
      }
    }
  }, 1000);


  $scope.deletePost = function (id) {

    dialogs.confirm("Desea eliminar?", "Esta seguro que desea eliminar esta publicación?",
     {animation: true,  size:'md'}).result.then(() => {
        PostsSrv.deletePost(id, function (err) {
          if (err) {
            return alert("Error!" + err);
          }
          $scope.getPostList();

        })
     });

  }
  $scope.getProgressValue = function (post) {
    if (post._type == 'free') {
      return 100;
    }
    return (post.secondsToExpire / post.payedLapseInSeconds) * 100;

    return 100;
  }

  $scope.getProgressText = function (post) {
    if (post._type == 'free')
      return 'Sin vencimiento';
    return post.secondsToExpire + " segs. Restantes";
  }
  $scope.getProgressBarType = function (post) {
    return 'success';
    /*  if (value < 25) {
        type = 'success';
      } else if (value < 50) {
        type = 'info';
      } else if (value < 75) {
        type = 'warning';
      } else {
        type = 'danger';
      }*/
  }

}]);
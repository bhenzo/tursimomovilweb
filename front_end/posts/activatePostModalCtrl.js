app.controller("ActivatePostModalCtrl", ["$scope", "PostsSrv", "$uibModalInstance", "data", function ($scope, PostsSrv, $modalInstance, data) {
    
    $scope.post = data.post;
    $scope.activation = {
        onlyActivate: false,
        days: 0
    };
    $scope.valid = () => {
        if ($scope.post._type == 'free'){
            return true;
        }
        if ($scope.activation.onlyActivate == false && $scope.activation.days <= 0) return false;
        return true;
    }

    $scope.accept = () => {
        PostsSrv.activatePost($scope.post.id, $scope.activation, (err, resp) => {
            if (err){
                return;
            }

            $modalInstance.close();
        });
    }

    $scope.cancel = function(){
        $modalInstance.dismiss('Canceled');
    }; 
    
    
}]);
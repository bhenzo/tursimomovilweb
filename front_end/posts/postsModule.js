app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/posts/:id', {
    templateUrl: 'app/html/posts/post.html',
    controller: 'PostCtrl',
  })
  .when('/payment_history', {
    templateUrl: 'app/html/posts/payment_history.html',
    controller: 'PaymentHistoryCtrl',
  })
    .when('/post_list', {
    templateUrl: 'app/html/posts/post_list.html',
    controller: 'postListCtrl',
  });
}]);

app.controller("PostCtrl", ["$rootScope", "LanguagesSrv","UserSrv", "$location", "$routeParams", "dialogs", "$scope", "PostsSrv", 
"ZonesSrv", "CategoriesSrv", "$interval", 
function ($rootScope, LanguagesSrv, UserSrv, $location, $routeParams, dialogs, $scope, PostsSrv, ZonesSrv, CategoriesSrv, $interval) {

  $scope.maxDescLen = 200;

  //Formato objeto para acceder tipo hashmap
  $scope.categories = {};
  //Esto es por si hago un evento
  $scope.meetings = [];

  //Manejo loco de fechas
  $scope.datesArray = [];
  $scope.meetingsMap = {};

  $scope.post = {
    'type': 'free',
    'album': []
  };
  $scope.arrivedType = 'free';

  $scope.allZones = [];

  LanguagesSrv.getLanguages((err, resp) => {
    if (err){
      return;
    }
    $scope.languages = resp;
  });

  $scope.getPost = () => {
    PostsSrv.getPost($routeParams.id, (err, resp) => {
      if (err){
        return;
      }
      
      $scope.post = resp;
      $scope.arrivedType = resp.type;
      //TODO esto es workaround para el moment.js
      if ($scope.post.type == 'event'){
        $scope.datesArray = [];
        $scope.meetingsMap = {};
        let added = {};
        for(let i = 0; i < $scope.post.meetings.length; i++){
          let dateKey = moment($scope.post.meetings[i].start_time).format('DD-MM-YYYY');

          if (!added[dateKey]){
            $scope.datesArray.push(moment($scope.post.meetings[i].start_time));
            added[dateKey] = true;
          }


          $scope.post.meetings[i].start_time = moment($scope.post.meetings[i].start_time);
          $scope.post.meetings[i].end_time = moment($scope.post.meetings[i].end_time);
          if (!$scope.meetingsMap[dateKey]){
            $scope.meetingsMap[dateKey] = [];
          }

          $scope.meetingsMap[dateKey].push($scope.post.meetings[i]);
          
          
        }
      }
      

    });
  };

  //Agrego un elemento al meetingList. Aca, cada elemento de esta lista
  //Esta sobre el mismo dia, por lo que clono la fecha
  $scope.addHour = (meetingList) => {
    let d = meetingList[0].start_time;
    meetingList.push({
      start_time: d.clone(),
      end_time: d.clone()
    })
  };

  $scope.checkLink = () => {
    PostsSrv.checkLink($scope.post.weblink, (err, resp) => {
      if (err){
        return;
      }
      console.log(resp);
    })
  }

  $scope.$watch('datesArray', (old, newv) => {
    //Acomodo los meetings en el map

    let newMap = {};
    $scope.datesArray.forEach(d => {
        let strdate = d.format('DD-MM-YYYY');
        if ($scope.meetingsMap[strdate]){
          newMap[strdate] = $scope.meetingsMap[strdate];
        } else {
          newMap[strdate] = [{
            start_time: d.clone(),
            end_time: d.clone()
          }];
          //newMap[strdate].start_time._d = null;
          //newMap[strdate].end_time._d = null;
        }
    });
    $scope.meetingsMap = newMap;

  }, true);

  //Veo si estoy queriendo editar uno o es nuevo
  if ($routeParams.id && $routeParams.id != 'new'){
    //No es nuevo, lo cargo.
    $scope.getPost();


  }

  $scope.typeChanged = () => {
      //me fijo si cambio de tipo de publicacion (publicaciones o eventos)
      if ($scope.arrivedType == 'event' && $scope.post.type != 'event' || 
          $scope.arrivedType != 'event' && $scope.post.type == 'event') {
            $scope.post.categories = [];
          }
      //Me fijo si cambio de tipo y le corresponden menos categorias
      let currMax = $scope.catsByPostType[$scope.post.type].max_categories;
      if ($scope.post.categories){
        while ($scope.post.categories.length > currMax) {
          $scope.post.categories.pop();
        }
      }
      

  }
  $scope.catsByPostType = {};

  CategoriesSrv.getCatsByPostType((err, resp) => {
    if (err){
        return;
    }
    $scope.catsByPostType = resp;
});

  CategoriesSrv.getCategoryList((err, resp)=>{
      if (err){
        return;
      }
      $scope.categories = {};
      for(let i = 0; i < resp.length; i++){
        $scope.categories[resp[i].id] = resp[i];
      }
  });

  $scope.openCategorySelector = () => {
      dialogs.create('app/html/posts/select_category_modal.html','SelectCategoryModalCtrl',{'post': $scope.post},{animation:true, size:'lg'},'Ctrl')
      .result.then((data) => {

          $scope.post.categories = data.categories;

          return true;
      }, () => {
          return true;
      });
  }

  
  $scope.gmapParams = "https://maps.google.com/maps/api/js?key=AIzaSyBdUW6PlIo3d-LUA4mmwPAIrBcsrN1Z_m0";
  $scope.validate = function () {


    if ($scope.getMeetingsErrors().length > 0) return false;

    if (!$scope.post.categories || $scope.post.categories.length == 0) return false;
    if (!atLeastOneTranslationExists($scope.post._name)) return false;
    if (!$scope.post.phone) return false;
  
    if (!$scope.post.id && !$scope.post.image) return false;

    if (!$scope.post.lat) return false;
    if (!$scope.post.lon) return false;
    if ($scope.post.type != 'free') {
        if (!atLeastOneTranslationExists($scope.post.description))
          return false;
       // if ($scope.post.description.length > 200)
        //  return false;
    }

    //if (!$scope.post.address) return false;
    return true;
  }


  $scope.$watch('post.album', function(newValue, oldValue) {
      while($scope.post.album.length > 5) {
        $scope.post.album.splice($scope.post.album.length - 1, 1);
      }
    }, true);


  $scope.deleteImg = function(index){
      $scope.post.album.splice(index, 1);
  }

  $scope.getZones = function () {
    ZonesSrv.getZones(function (err, resp) {
      if (err) {
        return alert("ERROR recuperando zonas");
      }
      $scope.allZones = resp;
      console.log(resp);
    });
  }
  $scope.getZones();


    $scope.confirmSave = () => {

      //Aca hago un workaround pa que llegen las fechas sin el "_d"
      let _post = angular.copy($scope.post);
      let _mettingsMap =  angular.copy($scope.meetingsMap);
      console.log(_post);
      if ($scope.meetingsMap){
        let keys = Object.keys(_mettingsMap);
        _post.meetings = [];
        for(var i = 0; i < keys.length; i++){
          for(var m = 0; m < _mettingsMap[keys[i]].length; m++){
            let met = _mettingsMap[keys[i]][m];
            //esto es porque al copiar no es mas un objeto momentjs
            let realMet = $scope.meetingsMap[keys[i]][m];
            met.start_time = realMet.start_time.format("YYYY-MM-DD HH:mm:ss");
            met.end_time =realMet.end_time.format("YYYY-MM-DD HH:mm:ss");

            _post.meetings.push(met);
          }
        }
      }

      PostsSrv.savePost(_post, function (err, resp) {
        if (err) {
          return alert("ERROR");
        }
        UserSrv.refreshSession();
        $rootScope.refreshNotifications();
        dialogs.notify("Aviso", "Su publicacion ha sido guardada correctamente", {animation:true, size:'md'});
        $location.path('/post_list');
      });
    }

    $scope.blockPost = () => {
      //Solo admins
      PostsSrv.blockPost($scope.post.id, (err, resp) => {
        if (err){
          return;
        }
         $scope.getPost();
      })
    }

    $scope.savePost = function () {
      let failed = false;
      let keys = $scope.post.weblink ? Object.keys($scope.post.weblink) : [];

      keys.forEach(langid => {
        console.log("KEY WEBLINK" + langid);
        console.log($scope.languages);
        let weblink = $scope.post.weblink[langid];
        if ($scope.getLinkFormatError(weblink) != null){
          let langname = $scope.languages.find(lang => lang.id == langid)._name;
          dialogs.error("Error", "La dirección web indicada en el lenguaje " + langname + " es inválida");
          failed = true;
        }
      });
      if (failed){
        return;
      }
      

      //Veo si esta cambiando de tipo, tengo que avisarle que se le va a cancelar lo que pago
      if ($scope.arrivedType != 'free' && $scope.post.type != $scope.arrivedType){
          dialogs.confirm('Esta seguro?', 'Cambiar el tipo de publicacion a una publicacion guardada cancelará el tiempo comprado', {size:'md', animation:true})
          .result.then(() => {
            $scope.confirmSave();
          });
      } else {
          $scope.confirmSave();
      }


   
  }

  $scope.getLinkFormatError = (link) => {
    let str = link;
    if (!str || str == ''){
      return null;
    } 

    var pattern = new RegExp(/^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/,'i'); // fragment locater
    if(!pattern.test(str)) {
      return "Ingrese un link válido";
    } else {
      return null;
    }
  }

  $scope.setLatLon = function (event) {
    $scope.post.lat = event.latLng.lat();
    $scope.post.lon = event.latLng.lng();
  }

  $scope.imageSelected = function (img) {
   // console.log(img);
   // console.log($scope.post.image);
  }

  //Eventos **************************************************************************
  $scope.addMeeting = () => {
    if (!$scope.post.meetings){
      $scope.post.meetings = [];
    }
    $scope.post.meetings.push({
      start_time: moment(),
      end_time: null,
      editing: true
    });
  }

  $scope.removeMeeting = (index) => {
    $scope.post.meetings.splice(index, 1);
  }

  $scope.dateChange = (which, meeting) => {
      console.log(meeting);
      switch (which) {
        case 'start_time':
          
          break;
        case 'end_time':

          break;
        default:
          break;
      }
  }

  $scope.removeHour = (list, index) => {
    list.splice(index, 1);
  }

  //Traigo errores si hay (solapamientos, etc)
  $scope.getMeetingsErrors = () => {
      let ps = $scope.post;
      let errs = [];
      if (ps.type == 'event'){
        if (Object.keys($scope.meetingsMap).length == 0) {
          errs.push("Debe definir almenos una fecha");
        } else {
          let va = 1;
          let keys = Object.keys($scope.meetingsMap);

          for(let i = 0; i < keys.length; i++){

            for(let mi = 0; mi < $scope.meetingsMap[keys[i]].length; mi++){
              let meeting = $scope.meetingsMap[keys[i]][mi];
              if (!meeting.start_time._d){
                errs.push("El encuentro numero " + va + " no se definió inicio");
              }
              if (!meeting.end_time._d){
                errs.push("El encuentro numero " + va + " no se definió finalización");
              }
              if (meeting.start_time._d && meeting.end_time._d && meeting.start_time._d > meeting.end_time._d){
                errs.push("El encuentro numero " + va + " tiene la fecha de inicio posterior a la de finalización");
              }
           
              va ++;
            }

          }
        }
      }
      return errs;
  }

  let atLeastOneTranslationExists = (item) => {
    if (item == null)
      return false;
    let keys = Object.keys(item);
    let setted = false;
    keys.forEach(key => {
      if (item[key] && item[key] != ''){
        setted = true;
      }    
    });
    return setted;
  }

  //TODO FIXME PArche para checkeo. ngChange no anda en el datepicker
  $interval(() => {
      $scope.meetingErrors = $scope.getMeetingsErrors();
  }, 1000);

}]);
app.factory('BillingSrv', ['$http', function ($http) {
    return {
        getPayments: (page, order, order_mode, callback) => {
            $http.get('/billing?page='+page+'&order='+order+'&order_mode='+order_mode).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        saveBill: (payment, callback) => {
            $http.post('/billing/' + payment.id + '/bill', payment).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        }
        
    };
}]);
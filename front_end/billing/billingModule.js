app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/billing', {
            templateUrl: 'app/html/billing/billing.html',
            controller: 'BillingCtrl',
        })
        
}]);
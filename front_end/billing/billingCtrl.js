app.controller("BillingCtrl", ["UserSrv", "$scope", "BillingSrv", "dialogs", function (UserSrv, $scope, BillingSrv, dialogs) {

    $scope.data = {
        'page': 1,
        'payments' : []
    };

    $scope.filter = {
        'order': 'status',
        'order_mode': 'asc'
    };

    $scope.getPayments = () => {
        BillingSrv.getPayments($scope.data.page, $scope.filter.order,
             $scope.filter.order_mode, (err, resp) => {
            if (err){
                return;
            }
            $scope.data = resp;
            UserSrv.refreshNotifications();
        });
    }
    $scope.getPayments();

    $scope.saveBill = (payment) => {
        BillingSrv.saveBill(payment, (err, resp) => {
            if (err){
                return;
            }
            $scope.getPayments();
        });
    }

}]);
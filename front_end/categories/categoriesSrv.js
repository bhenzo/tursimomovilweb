app.factory('CategoriesSrv', ['$http', function ($http) {
    return {
        getCategories: (type, callback) => {
            $http.get('/categories?type='+type).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getCategoriesWithTranslation: (type, callback) => {
            $http.get('/translated_categories?type='+type).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getCategoryList: (callback) => {
            $http.get('/categories/list').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        save: (cats, callback) => {
            $http.post('/categories', {'cats' : cats}).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        saveCatsByPostType: (config, callback) => {
            $http.post('/categories_by_post_type', config).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getCatsByPostType: (callback) => {
            $http.get('/categories_by_post_type').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        }
    };
}]);
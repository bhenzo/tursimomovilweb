app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/categories', {
    templateUrl: 'app/html/categories/categories.html',
    controller: 'CategoriesCtrl'
  })
 
}]);

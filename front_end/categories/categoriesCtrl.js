app.controller("CategoriesCtrl", ["$rootScope","LanguagesSrv", "$scope", "CategoriesSrv", 
"dialogs", "$routeParams", function ($rootScope, LanguagesSrv, $scope, CategoriesSrv, dialogs, $routeParams) {
    
    $scope.list = [];
    //local id es para poder identificar univocamente cada item dentro de la lista
    //para que la carga def imagenes funcione bien
    let lid = 1;

    //Configuracion de cantidad de categorias por publicacion
    $scope.catsConfig = {
        'free': {
            'max_categories': 1
        },
        'silver': {
            'max_categories': 3
        },
        'gold': {
            'max_categories': 5
        }
    }

    //Se traen los lenguajes disponibles para realizar las traducciones
    LanguagesSrv.getLanguages((err, resp) => {
        if (err){
        return;
        }
        $scope.languages = resp;
    });

    CategoriesSrv.getCatsByPostType((err, resp) => {
        if (err){
            return;
        }
        $scope.catsConfig = resp;
    });
    $scope.$watch('catsConfig', (oldv, newv) => {
        
        for(let [key, value] of Object.entries($scope.catsConfig)){
            value.max_categories = parseInt(value.max_categories);
            if (!value.max_categories || value.max_categories == '' || value.max_categories < 1){
                value.max_categories = 1;
            }
        }
    }, true);

    let identifyItemsList = (list) => {
        if (!list || list.length == 0){
            return;
        }
        list.forEach(elem => {
            elem.lid = lid++;
            identifyItemsList(elem.nodes);
        });
    }

    $scope.getCategories = () => {
        CategoriesSrv.getCategoriesWithTranslation('all', (err, resp) => {
            if (err){
                return;
            }
            $scope.list = resp;
            identifyItemsList($scope.list);
            console.log($scope.list);   
        });
    }
    $scope.getCategories();

    $scope.getValidationErrors = () => {
        //Primero checkeo si todos tienen logo
        let valid = true;
        let errs = [];
        for(let i = 0; i < $scope.list.length; i++){
            let elem = $scope.list[i];
            if (!elem.image && !elem.new_image){
                valid = false;
            }
        }
        if (!valid){
            errs.push("Todas las categorias base deben tener un logo.");
        }
        //Ahora veo que haya por lo menos una de eventos, y por lo menos una normal
        let commons = 0;
        let events = 0;
        $scope.list.forEach(cat => cat.events_category ? events++ : commons++);
        if (events == 0){
            errs.push("Debe haber almenos una categoria para eventos");
        }
        if (commons == 0){
            errs.push("Debe haber almenos una categoria para publicaciones normales");
        }
        return errs;
    }

    $scope.save = () => {
        CategoriesSrv.save($scope.list, (err, resp) => {
            if (err){
                return;
            }
            $scope.getCategories();
            $rootScope.refreshNotifications();
        });
        CategoriesSrv.saveCatsByPostType($scope.catsConfig, (err, resp) => {
            if (err){
                return;
            }
        });
    };


    $scope.newCategory = (cat) => {
        if (!cat){
            //Si es null, la agrego en la raiz
            $scope.list.unshift({
                name: [],
                editing: true,
                'lid': lid++,
                nodes: []
            });
        } else {
           if (!cat.nodes){
               cat.nodes = [];
           }
            //Si no es null es porque quiero agregar categoria hija
            cat.nodes.unshift({
                name: [],
                editing: true,
                'lid': lid++,
                nodes: []
            });
        }
    
    };

}]);

app.factory('PaymentsSrv', ['$http', function ($http) {
    return {
        getPromos: function (callback) {
            $http.get('/promos').then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        getPayment: function (paymentId, callback) {
            $http.get('/payments/' + paymentId).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        confirmOrder: function (posts, callback) {
            $http.post('/confirm_order', {'posts' : posts}).then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        },
        cancelTime: function (postId, callback) {
            $http.post('/posts/'+postId+"/cancel_time").then(function (resp) {
                callback(null, resp.data);
            }, function (err) {
                callback(err);
            });
        }
    };
}]);

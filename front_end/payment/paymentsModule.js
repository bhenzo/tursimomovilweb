app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
    .when('/payments', {
    templateUrl: 'app/html/payment/payments.html',
    controller: 'PaymentsCtrl',
  });
}]);

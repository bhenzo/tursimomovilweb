app.controller("PaymentsCtrl", ["UserSrv", "$interval",
 "$scope", "PostsSrv", "PaymentsSrv", "dialogs",
  "$location", "$window", function (UserSrv, $interval, $scope,
   PostsSrv, PaymentsSrv, dialogs, $location, $window) {
    $scope.posts = [];
    $scope.promosByType = {
      'silver' : [],
      'gold': []
    };
    $scope.total = 0;

    PaymentsSrv.getPromos(function(err, promos){
      if (err){
        return;
      }
      $scope.promosByType = {
        'silver' : [],
        'gold': []
      };
      for(var i = 0; i < promos.length; i++){
        $scope.promosByType[promos[i].post_type].push(promos[i]);
      }
    });
    var oncloseListener = null;
    //Payment
    $scope.startPayment = function(){
      $scope.onPayment = true;
      if (oncloseListener){
        $interval.cancel(oncloseListener);
      }
      PaymentsSrv.confirmOrder($scope.posts, function(err, resp){
        if (err){
          return;
        }
        if (resp.result == 'ok'){
          var paymentId = resp.payment_id;
          var tes = $window.open(resp.form_url, "popup", "width=800,height=700,left=300,top=200");
          oncloseListener = $interval(function(){
              if (tes.closed){
                $scope.onPayment = false;
                $interval.cancel(oncloseListener);
                PaymentsSrv.getPayment(paymentId, function(err, resp){
                    if (err){
                      return;
                    }
                    if (resp.status == 'success'){
                      UserSrv.refreshSession();

                      dialogs.notify('Exito!', 'La compra se realizó correctamente. ', {animation: true, size:'md'});
                      $location.path('/post_list');
                    } else {
                      dialogs.error('Ocurrio un error', 'La compra no se pudo completar, por favor, intentelo nuevamente o pruebe con una tarjeta de crédito diferente', {animation: true, size:'md'});
                    }
                });
              }
          }, 100);
        } else if (resp.result == 'from_balance'){
          UserSrv.refreshSession();

          dialogs.notify('Exito!', 'La compra se realizó correctamente. ', {animation: true, size:'md'});
          $location.path('/post_list');
        } else {
          dialogs.error("Error", "Ocurrio un error al solicitar el pago. Intentelo nuevamente mas tarde.", {animation: true, size: 'md'});
        }
      /*  if (resp == 'ok'){
          $location.path('/post_list');
        } else {
          dialogs.error("Error", resp, {animation: true, size: 'md'});
        }*/
      })
    }

    $scope.cancelTimePost = (post) => {
        dialogs.confirm("Confirme", "Esta seguro que desea cancelar el tiempo restante de esta publicación? El saldo restante quedará a favor en su cuenta para futuras compras.",
        {animation:true, size:'sm'}).result.then(()=>{
          PaymentsSrv.cancelTime(post.id, (err, resp) => {
            if (err){
              return;
            }
            //esto para que traiga el saldo a favor
            UserSrv.refreshSession();
            $scope.getPostList();
          });
        }, () => {});
    } 

    $scope.canBuyTime = function(post){
      if (post._type == 'free' || post._type == 'event'){
        return false;
      }
      if (post._status != 'active'){
        return false;
      }
      if (post.secondsToExpire > 0){
        return false;
      }
    
      return true;
    }

    //Traigo los posts
    $scope.getPostList = function () {
        PostsSrv.getPostList(function (err, resp) {
            if (err) {
                return alert("Error!" + err);
            }
            $scope.posts = resp;
        })
    }
    $scope.getPostList();

    $scope.calculateTotal = function(){
        $scope.total = 0;
        for(var i = 0; i < $scope.posts.length; i++){
            var post = $scope.posts[i];
            if (!post.payment_type_selected){
              continue;
            }
            $scope.total += post.payment_type_selected.price;
        }
    }

}]);

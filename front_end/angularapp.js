//Workaround para el textangular
angular.lowercase = text => text ? text.toLowerCase() : null;

moment.locale('es');

var app = angular.module("TurismoMovil", ['ngRoute', 'naif.base64', 'ngMap', 'ngAnimate', 
'ui.bootstrap', 'ngSanitize', 'dialogs.main', 'textAngular', 'ui.tree', 'moment-picker', 'multipleDatePicker']);

app.config(['$routeProvider', '$locationProvider', '$provide', function($routeProvider, $locationProvider, $provide) {
  $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions) {
    taOptions.forceTextAngularSanitize = false;
    return taOptions;
  }]);
    
  $routeProvider
   .when('/', {
    templateUrl: 'app/html/main/main.html',
    controller: 'MainCtrl'
  });
}]);

app.filter('newlineHtml', function () {
    return function(text) {
        if (!text)
          return null;
        return text.replace(/\n/g, '<br/>');
    }
})

app.config(["$provide", "$httpProvider", function ($provide, $httpProvider) {

  // Intercept http calls.
  $provide.factory('NotLoggedInterceptor', function ($q) {
    return {
      request: function (config) {
        return config || $q.when(config);
      },

      requestError: function (rejection) {
        console.log("request error");
        console.log(rejection);
        return $q.reject(rejection);
      },

      response: function (response) {
        return response || $q.when(response);
      },

      responseError: function (rejection) {
        if (rejection.status == 403){
          return window.location = "#!/login";
        }
        return $q.reject(rejection);
      }
    };
  });
  //Configuracion de textAngular 
  $provide.decorator('taOptions', ['taToolFunctions','taRegisterTool', '$delegate', 'dialogs', '$window', 
  function(taToolFunctions, taRegisterTool, taOptions, dialogs, $window) { // $delegate is the taOptions we are decorating
    taRegisterTool('gallery', {
        buttontext: 'Fotos',
        action: function() {
            //alert('Test Pressed');
            let _savedSelection = $window.rangy.saveSelection();
            let self = this;
           
           

            dialogs.create('app/html/staticinfo/photo_gallery_modal.html','PhotoGalleryModalCtrl',{},{animation:true, size:'lg'},'Ctrl')
            .result.then((data) => {
              var embed = '<img src="' + data.image_url + '" width="100%"><br>';

              self.$editor().displayElements.text[0].focus();
              $window.rangy.restoreSelection(_savedSelection);
              self.$editor().wrapSelection('insertHTML', embed, true);
            }, () => {
                return true;
            });

           // imageLink  = "http://localhost:82/uploads/staticinfo/11539211420223.png";

     

            // this.$editor().wrapSelection('forecolor', 'red');
        }, onElementSelect: {
            element: 'img',
            action: taToolFunctions.imgOnSelectAction
        }
    });

    taOptions.toolbar = [
        ['h1', 'h2', 'h3', 'h4', 'p', 'quote'],
        ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'undo'],
        ['justifyLeft', 'justifyCenter', 'justifyRight']
    ];

    taOptions.toolbar[1].push('gallery');
    
    // add the button to the default toolbar definition
    //taOptions.toolbar[1].push('colourRed');
    return taOptions;
  }]);

  $provide.factory('LoadingInterceptor', ['$q', '$rootScope', function($q, $rootScope) {
      return {
            response: function(response) {
                // do something on success
                $rootScope.ajaxLoading --;
                if ($rootScope.ajaxLoading == 0){
                  $rootScope.maxAjax = 0;
                }
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                // do something on error
                $rootScope.ajaxLoading --;
                if ($rootScope.ajaxLoading == 0){
                  $rootScope.maxAjax = 0;
                }
                return $q.reject(rejection);
            },
            request: function(config){
              $rootScope.ajaxLoading ++;
              if ($rootScope.ajaxLoading > $rootScope.maxAjax){
                $rootScope.maxAjax = $rootScope.ajaxLoading;
              }

              return config || $q.when(config);
            },
            requestError: function(rejection){
              $rootScope.ajaxLoading ++;
              if ($rootScope.ajaxLoading > $rootScope.maxAjax){
                $rootScope.maxAjax = $rootScope.ajaxLoading;
              }
              return $q.reject(rejection);
            }

        };
    }]);


  // Add the interceptor to the $httpProvider.
  $httpProvider.interceptors.push('NotLoggedInterceptor');
  $httpProvider.interceptors.push('LoadingInterceptor');

}]);

app.run(['UserSrv', '$rootScope', '$location', '$q', function(UserSrv, $rootScope, $location, $q) {
  $rootScope.ajaxLoading = 0;


  $rootScope.isPublicPath = function(){
    var path = $location.path();
    if (path == '/' || path == '/signup'){
      return true;
    }
    return false;
      //console.log($location.path());
  }

    $rootScope.showTapador = function(){
        if ($location.path() == '/signup') return false;
        if ($location.path() == '/') return false;
        return $rootScope.ajaxLoading > 0;
    }

    $rootScope.getSession = function(){
      UserSrv.getLoggedUser(function(err, resp){
        if (err){
          return;
        }

        $rootScope.user = resp;
        //$rootScope.$apply();
      });
    }
    
    $rootScope.getSession();

    $rootScope.refreshNotifications = () => {
      $rootScope.getSession();
    }


    $rootScope.checkRole = function(role){

        var usr = $rootScope.user;
        if (!usr || !usr.roles)
          return false;

        for(var i = 0; i < usr.roles.length; i++){
          if (role.toLowerCase() == usr.roles[i].id_str.toLowerCase()){
            return true;
          }
        }

        return false;
    }

    $rootScope.go = function(p){
      $location.path(p);
    }

    $rootScope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
    };

    $rootScope.secondsToStr = function(seconds){

        function numberEnding (number) {
            return (number > 1) ? 's' : '';
        }

        var temp = seconds;
        var years = Math.floor(temp / 31536000);
        if (years) {
            return years + ' año' + numberEnding(years);
        }
        //TODO: Months! Maybe weeks?
        var days = Math.floor((temp %= 31536000) / 86400);
        if (days) {
            return days + ' dia' + numberEnding(days);
        }
        var hours = Math.floor((temp %= 86400) / 3600);
        if (hours) {
            return hours + ' hora' + numberEnding(hours);
        }
        var minutes = Math.floor((temp %= 3600) / 60);
        if (minutes) {
            return minutes + ' minuto' + numberEnding(minutes);
        }
        var seconds = temp % 60;
        if (seconds) {
            return seconds + ' segundo' + numberEnding(seconds);
        }
        return 'justo ahora'; //'just now' //or other string you like;

    }

    /*//Parser para controlar imagenes
    $rootScope.checkImage = (file, base64) => {
        console.log(file);
        return base64;
    }*/

}]);




app.directive("imageControl", ['dialogs', (dialogs) => {
  return {
    require: 'ngModel',
    replace: false,
        link: (scope, element, attrs, ngModel) => {
            var config = {
              maxW: attrs.maxW | null,
              maxH: attrs.maxH | null,
              rel: attrs.rel | null,
              relMargin: attrs.relMargin | 0
            }

            let validate = (nval, callback) => {
              if (nval && nval.filetype){
                let valid = false;
                ['image/jpeg', 'image/png'].forEach(type => type == nval.filetype ? valid = true:false);
                if (!valid){
                  //tiro algun error o algo
                  dialogs.error('Error', 'Ingrese una imagen válida', {animation:true, size:'sm'});
                  callback(nval);
                  return;
                }
                //Sino, veo si alguna restriccion falla
                let b64img = scope[attrs.ngModel];
                let img = new Image();

                img.onerror = () => {
                  dialogs.error('Error', "La imagen ingresada no es válida", {animation:true, size:'sm'});
                  callback(nval);
                }

                img.onload = () => {
                  console.log("loaded");
                  console.log("Width: " + img.width + " - " + img.height);
                  let errs="";



                  if (config.maxW && config.maxW < img.width){
                    errs += "<li>El ancho maximo permitido de imagen es de " + config.maxW +"px</li>";
                  }
                  if (config.maxH && config.maxH < img.height){
                    errs += "<li>El alto maximo permitido de imagen es de " + config.maxH +"px</li>";
                  }

                  let relAsp = img.width / img.height;
                  if (config.rel){
                    let dif = Math.abs(config.rel - relAsp);
                    if (dif > config.relMargin){
                      let req = Math.round(config.rel * 100) / 100;
                      errs += "<li>La relación aspecto de la imagen seleccionada no es válida. Se requiere ";
                      errs += "Una relacion de " + req + ":1</li>";
                    }
                  }

                  if (errs != ""){
                    dialogs.error('Error', "<ul>"+errs+"</ul>", {animation:true, size:'sm'});
                    callback(nval);
                  }
                  img = null;
                }
                img.src = "data:"+nval.filetype+";base64,"+nval.base64;

              }
            }

            scope.$watch(attrs.ngModel, (nval) => {
                //console.log(scope[attrs.ngModel]);
                //console.log(nval);
                //ngModel.$setViewValue(null);
                if (nval){
                  if (Array.isArray(nval)){
                    //asigno un "ID" temporal a cada elemento
                    //Por si hay que borrar alguna cosa
                    for(let i = 0; i < nval.length; i++){
                      nval[i].tmpid = i;
                    }
                    for(let i = nval.length - 1; i >= 0 ; i--){
                      (function(j) {
                        validate(nval[j], (elem) => {
                          for(let td = nval.length - 1; td >= 0; td--){
                            if (nval[td].tmpid == elem.tmpid){
                              nval.splice(td);
                              break;
                            }
                          }
                        });
                      })(i);
                     
                    }
                  } else {
                    console.log("validando coso comun");
                    validate(nval, (elem) => {
                      ngModel.$setViewValue(null);
                    });
                  }
                }
                



                
            });
        }
  }
}]);

/*
app.factory("Utils", ['dialogs', function(dialogs){
    return {
      checkImage: (image, types) => {

      }
    }
}]);*/
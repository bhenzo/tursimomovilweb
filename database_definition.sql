 SET sql_mode = '';
drop database if exists turismo_movil;
create database turismo_movil;
use turismo_movil;

-- Table de publicaciones
create table posts (
    id bigint primary key AUTO_INCREMENT,
    _name varchar(128) not null,
    phone varchar(128),
    address varchar(256),
    logo varchar(256),
    lat float(10, 6),
    lon float(10, 6)
) engine = InnoDB;

ALTER TABLE posts ADD COLUMN _status varchar(128) not null;
ALTER TABLE posts ADD COLUMN _deleted boolean DEFAULT false;


-- Tabla de Zonas
create table zones (
    id bigint primary key AUTO_INCREMENT,
    _name varchar(128) not null,
    _description varchar(256)
) engine = InnoDB;

ALTER TABLE `zones` CHANGE `_description` `_description` VARCHAR(2048) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- User
create table users (
    	id integer AUTO_INCREMENT PRIMARY KEY,
    	name varchar(128),
    	email varchar(128),
    	password varchar(256)
    ) AUTO_INCREMENT=100 engine = InnoDB;

alter table users add column remember_token varchar(512);

alter table users add column created_at datetime;
alter table users add column updated_at datetime;

-- Data User
create table data_users (
    	id integer AUTO_INCREMENT PRIMARY KEY,
        cuit varchar(32),
        bussiness_name varchar(128),
        phone varchar(128),
        _address varchar(128),
        user_id integer, INDEX usr_id(user_id), FOREIGN key (user_id) REFERENCES users(id)
) engine = InnoDB;

-- Roles Table
create table roles (
    	id integer AUTO_INCREMENT PRIMARY KEY,
        id_str varchar(32),
        _name varchar(128)
) AUTO_INCREMENT = 100 engine = InnoDB;

-- user_has_role table
create table user_has_role (
    	id integer AUTO_INCREMENT PRIMARY KEY,
        user_id integer, INDEX usr_id(user_id), FOREIGN key (user_id) REFERENCES users(id),
        role_id integer, FOREIGN key (role_id) REFERENCES roles(id)
) engine = InnoDB;

-- Agrego un nuevo campo a zonas para aplicar la baja logica
ALTER TABLE zones ADD COLUMN _deleted boolean DEFAULT false;
ALTER TABLE posts ADD COLUMN (user_id integer, INDEX usr_id(user_id), FOREIGN key (user_id) REFERENCES users(id));
ALTER TABLE posts ADD COLUMN (zone_id bigint, INDEX zne_id(zone_id), FOREIGN key (zone_id) REFERENCES zones(id));

-- Insert de Roles
INSERT INTO roles (`id`, `id_str`, `_name`) VALUES (1, 'ADMIN', 'administrador');
INSERT INTO roles (`id`, `id_str`, `_name`) VALUES (2, 'CLIENT', 'cliente');



-- Sincronizacion de favoritos de turistas
create table tourist_accounts (
    id integer AUTO_INCREMENT primary key,
    account varchar(255)
) engine = InnoDB;

create table tourist_favorites (
    id bigint(20) PRIMARY key AUTO_INCREMENT,
    post_id bigint(20) not null,
    tourist_account_id int(11) not null,
    FOREIGN key (post_id) references posts(id),
    FOREIGN key (tourist_account_id) REFERENCES tourist_accounts(id)) engine = INNODB;

-- Descripcion de posts.
alter table posts add column description varchar(20000) default null;
alter table posts add column _type varchar(32) default 'free' not null;

-- Imagenes de publicaciones GOLD
create table photos (
    id bigint(20) primary key AUTO_INCREMENT,
    post_id bigint(20) not null,
    image varchar(256) not null,
FOREIGN KEY(post_id) references posts(id)) engine = INNODB;

-- Datos para registrar usuarios y coso.
alter table users add column lastname varchar(256) default null;
alter table users add column active boolean default false;

-- Metodos de pago
create table payment_methods (
    id bigint(20) primary key AUTO_INCREMENT,
    id_str varchar(64) not null,
    active boolean default true,
    name varchar(256) not null) ENGINE = INNODB;

alter table users add column payment_method_id bigint(20);
alter table users add FOREIGN key (payment_method_id) references payment_methods(id);

-- Tipos de pago
insert into payment_methods(id_str, name) values ('NORMAL', 'Pago con tarjeta en plataforma');
insert into payment_methods(id_str, name) values ('FREE_PREMIUM', 'Publicaciones premium gratuitas //TODO');

-- promos
create table promos (
    id bigint(20) AUTO_INCREMENT PRIMARY key,
    time bigint(20) not null,
    price float(10, 2) not null,
    post_type varchar(64) not null,
    name varchar(256) not null);

-- promos de testing
insert into promos(name, post_type, time, price) values ("30 segundos, $10", "silver", 30, 10.00);
insert into promos(name, post_type,  time, price) values ("60 segundos, $18 (10% desc.)", "silver", 60, 18.00);
insert into promos(name, post_type,  time, price) values ("90 segundos, $26 (15% desc.)", "silver", 90, 26.00);

insert into promos(name, post_type, time, price) values ("30 segundos, $40", "gold", 30, 40.00);
insert into promos(name, post_type,  time, price) values ("60 segundos, $66 (20% desc.)", "gold", 60, 66.00);


alter table posts add column expiration timestamp null default null;

-- pagos
create table payments (
    id bigint(20) AUTO_INCREMENT PRIMARY KEY,
    status varchar(32) not null,
    amount float(10, 2) not null
) engine = INNODB;

create table post_payment (
    id bigint(20) AUTO_INCREMENT PRIMARY KEY,
    post_id bigint(20) not null,
    payment_id bigint(20) not null,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp not null default current_timestamp,
    FOREIGN key fkpost(post_id) REFERENCES posts(id),
    FOREIGN key fkpayment(payment_id) references payments(id)) engine = INNODB;

alter table payments add column created_at timestamp not null default current_timestamp;
alter table payments add column updated_at timestamp not null default current_timestamp;

alter table post_payment add column purchased_seconds bigint(20) not null;
alter table payments add column payment_code varchar(256) not null;

-- mas datos de facturacion que pide todopago
alter table data_users add column city varchar(256);
alter table data_users add column postal_code varchar(32);
alter table data_users add column province varchar(64);
alter table data_users add column address_number varchar(64);

alter table payments add column paid_at timestamp null default null;

-- alter table de los indices de las tablas 
ALTER TABLE users AUTO_INCREMENT=150;
ALTER TABLE zones AUTO_INCREMENT=150;
ALTER TABLE photos AUTO_INCREMENT=150;
ALTER TABLE posts AUTO_INCREMENT=150;
ALTER TABLE roles AUTO_INCREMENT=150;
ALTER TABLE user_has_role AUTO_INCREMENT=150;
ALTER TABLE users AUTO_INCREMENT=150;
ALTER TABLE data_users AUTO_INCREMENT=150;


-- alter table posts se elimina la columna _deleted
ALTER TABLE posts  DROP COLUMN _deleted;
-- Puntuacion de los turistas 
create table ratings (
    id bigint(20) primary key AUTO_INCREMENT, 
    tourist_unique_identificator varchar(255) not null, 
    rating float(4,2) not null,
    post_id bigint(20) not null,
    FOREIGN KEY ratfk(post_id) references posts(id)) engine = InnoDB;;

-- Denuncias de los turistas
create table reports (
    id bigint(20) primary key AUTO_INCREMENT,
    tourist_unique_identificator varchar(255) not null,
    message varchar(2000) default null,
    post_id bigint(20) not null,
    created_at timestamp,
    updated_at timestamp,
    FOREIGN KEY pfk(post_id) REFERENCES posts(id)
    ) engine = InnoDB;;

alter table posts add column updated_at timestamp;
alter table posts add column created_at timestamp;

-- Informacion estatica 
create table static_info (
    id bigint(20) primary key AUTO_INCREMENT,
    title varchar(512) not null, 
    zone_id bigint(20) not null, 
    html TEXT not null, 
    FOREIGN KEY fksi1(zone_id) REFERENCES zones(id)) engine = InnoDB;;

-- Administracion de denuncias 
alter table reports add column _status varchar(64) default 'not_seen';

-- categories 
create table categories (
    id bigint(20) AUTO_INCREMENT primary key, 
    name varchar(255) not null,
    parentcategory_id bigint(20) default null, 
    FOREIGN KEY catfk1(parentcategory_id) references categories(id)) engine = InnoDB;;

ALTER TABLE categories AUTO_INCREMENT=150;
alter table categories add column morder bigint(20);

alter table posts add column category_id bigint(20) not null;
alter table posts add FOREIGN key postfkcat(category_id) references categories(id);

-- Multiples categorias por publicacion 
create table post_has_category (
    id bigint(20) AUTO_INCREMENT primary key,
    post_id bigint(20) not null,
    category_id bigint(20) not null,
    FOREIGN KEY phc1(post_id) references posts(id),
    FOREIGN KEY phc2(category_id) REFERENCES categories(id)) ENGINE = INNODB;

-- TODO Atencion, revisar aca el nombre de la FK
alter table posts drop FOREIGN KEY posts_ibfk_3;
alter table posts drop column category_id;

alter table categories add column created_at timestamp null;
alter table categories add column updated_at timestamp null;

-- Modificaciones 18/10/18
alter table categories add column image varchar(255) default null;

alter table promos modify post_type varchar(64) not null;

-- Modificaciones 23/10
insert into roles(id_str, _name) values ('EVENT_MANAGER', 'Organizador de eventos');

-- Eventos 
create table event_meetings (
    id bigint(20) PRIMARY KEY AUTO_INCREMENT, 
    start_time timestamp not null,
    end_time timestamp not null,
    post_id bigint(20) not null,
    description text default null,
    created_at timestamp, 
    updated_at timestamp,
    FOREIGN KEY em1(post_id) references posts(id));

-- 25/10
alter table categories add column events_category tinyint(1) default 0;
alter table event_meetings add column cancel_if_rains tinyint(1) default 0;

-- 05/11/2018

create table configurations (
    id bigint(20) AUTO_INCREMENT primary key,
    _group varchar(256) not null, 
    _key varchar(256) not null,
    int_value bigint(20),
    str_value varchar(1024));


-- 06-11-2018
alter table posts add column weblink text default null;

create table modules (
    id bigint(20) primary key AUTO_INCREMENT,
    morder bigint(20) not null,
    mdata TEXT not null,
    staticinfo_id bigint(20) not null,
    FOREIGN KEY sifk(staticinfo_id) references static_info(id)) ENGINE = INNODB;

-- 08 - 11- 2018
insert into configurations (int_value, _key, _group) 
values (99, 'event', 'cat_amount_by_post_type');

-- 09 - 11- 2018
alter table users add column balance float(10, 2) default 0;
alter table post_payment add column amount float(10, 2) not null;
alter table payments add column from_balance float(10, 2);

-- 13 - 11- 2018
CREATE TABLE `cache` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
create table sessions (
    id varchar(255),
    user_id bigint(20),
    ip_address varchar(64),
    user_agent text,
    payload text, 
    last_activity bigint(20));

-- 15 - 11 -2018
insert into configurations (int_value, _key, _group) 
values (1, 'free', 'cat_amount_by_post_type');
insert into configurations (int_value, _key, _group) 
values (2, 'silver', 'cat_amount_by_post_type');
insert into configurations (int_value, _key, _group) 
values (3, 'gold', 'cat_amount_by_post_type');

alter table payments add column billed_date timestamp null default null;
alter table payments add column bill_number varchar(255);

-- 14 - 05 - 2019
create table languages (
    id bigint(20) primary key AUTO_INCREMENT,
    _name varchar(128) not null,
    is_native tinyint(1) not null default 0
) ENGINE=INNODB;

create table translation_keys (
    id bigint(20) primary key AUTO_INCREMENT,
    description varchar(500),
    _key varchar(128) not null
) engine=InnoDB;

insert into translation_keys(_key, description) values ('app_name', 'Nombre de la aplicación');
insert into translation_keys(_key, description) values ('dialogs_loading_text', 'Texto de "carga" en ventana de CARGANDO');
insert into translation_keys(_key, description) values ('post_detail_activity_phone', 'Texto TELEFONO en detalle de publicación');
insert into translation_keys(_key, description) values ('post_detail_activity_address', 'Texto DIRECCION en detalle de publicación');
insert into translation_keys(_key, description) values ('title_activity_maps', 'Titulo de pantalla de MAPAS');
insert into translation_keys(_key, description) values ('title_activity_language_selection', 'Titulo de pantalla de SELECCION DE LENGUAJE');
insert into translation_keys(_key, description) values ('change_language_message', 'Mensaje para la selección de lenguaje en pantalla SELECCION DE LENGUAJE');
insert into translation_keys(_key, description) values ('accept_btn', 'Texto del boton de ACEPTAR');
insert into translation_keys(_key, description) values ('language_name', 'Texto del nombre del lenguaje a seleccionar');
insert into translation_keys(_key, description) values ('dialog_yes', 'Boton "SI" para confirmar en diálogos');
insert into translation_keys(_key, description) values ('dialog_no', 'Boton "NO" para rechazar en diálogos');
insert into translation_keys(_key, description) values ('text_insert_filter', 'Placeholder del campo para filtrar por texto');
insert into translation_keys(_key, description) values ('drawer_forecast', 'Texto "Pronostico" del menu lateral');
insert into translation_keys(_key, description) values ('drawer_calendar', 'Texto "Calendario" del menu lateral');
insert into translation_keys(_key, description) values ('drawer_change_language', 'Texto "Cambiar lenguaje" del menu lateral');
insert into translation_keys(_key, description) values ('drawer_select_zones', 'Texto "Seleccionar zonas" del menu lateral');
insert into translation_keys(_key, description) values ('drawer_static_info', 'Texto "Informacion estática" del menu lateral');
insert into translation_keys(_key, description) values ('drawer_favorites_list', 'Texto "Favoritos" del menu lateral');
insert into translation_keys(_key, description) values ('drawer_settings', 'Texto "Configuración" del menu lateral');

insert into translation_keys(_key, description) values ('title_activity_select_zones', 'Titulo de pantalla de SELECCIONAR ZONAS');
insert into translation_keys(_key, description) values ('select_zones_message', 'Mensaje de la pantalla SELECCIONAR ZONAS');
insert into translation_keys(_key, description) values ('select_zones_download_btn', 'Texto del boton de descarga en SELECCIONAR ZONAS');
insert into translation_keys(_key, description) values ('downloaded_zone', 'Texto de "Zona descargada" para indicar en la lista de zonas');
insert into translation_keys(_key, description) values ('error_no_internet', 'Error al no poder conectar con internet');
insert into translation_keys(_key, description) values ('deleting_zone_title', 'Titulo para borrar la zona');
insert into translation_keys(_key, description) values ('deleting_zone_message', 'Mensaje al borrar la zona');

insert into translation_keys(_key, description) values ('no_zone_downloaded', 'Mensaje al no haber ninguna zona descargada');
insert into translation_keys(_key, description) values ('new_favorite_text', 'Mensaje notificación al agregar un favorito');
insert into translation_keys(_key, description) values ('title_activity_favorite_list', 'Titulo de pantalla de LISTA DE FAVORITOS');

insert into translation_keys(_key, description) values ('text_phone', 'Texto "Telefono" en lista de favoritos');
insert into translation_keys(_key, description) values ('text_distance', 'Texto "Distancia" en lista de favoritos');
insert into translation_keys(_key, description) values ('delete_favorite_message', 'Mensaje de "Mantener presionado para borrar favorito"');
insert into translation_keys(_key, description) values ('delete_favorite_confirmation_title', 'Titulo de diálogo de Confirmación al borrar favorito');
insert into translation_keys(_key, description) values ('delete_favorite_confirmation_body', 'Contenido de diálogo de Confirmación al borrar favorito');
insert into translation_keys(_key, description) values ('gallery_text', 'Texto "Galeria" para la galeria de fotos de una publicación');
insert into translation_keys(_key, description) values ('no_internet_for_callery', 'Mensaje indicando falta de internet para bajar la galeria');
insert into translation_keys(_key, description) values ('error_loading_image', 'Mensaje de error al cargar imagen de galeria en pantalla de PUBLICACION');
insert into translation_keys(_key, description) values ('error_rating', 'Mensaje de error al indicar valoración de una publicación');
insert into translation_keys(_key, description) values ('report_post', 'Mensaje BOTON de reportar publicación');
insert into translation_keys(_key, description) values ('report_dialog_title', 'Titulo del diálogo al reportar publicacion');
insert into translation_keys(_key, description) values ('report_dialog_message', 'Contenido del diálogo al reportar publicación');
insert into translation_keys(_key, description) values ('report_dialog_send', 'Texto del boton ENVIAR al reportar publicación');
insert into translation_keys(_key, description) values ('report_dialog_cancel', 'Texto del boton CANCELAR al reportar publicación');
insert into translation_keys(_key, description) values ('error_reporting', 'Mensaje de error en caso de falla al reportar publicación');
insert into translation_keys(_key, description) values ('thanks_for_reporting_title', 'Titulo del dialogo de agradecimiento por reportar publicacion');
insert into translation_keys(_key, description) values ('thanks_for_reporting','Contenido del diálogo de agradecimiento por reportar publicacón');
insert into translation_keys(_key, description) values ('post_unavailable', 'Titulo de publicación no disponible');
insert into translation_keys(_key, description) values ('post_unavailable_message', 'Mensaje de explicación de publicacion no disponible');
insert into translation_keys(_key, description) values ('static_info_no_internet_error_title', 'Titulo del mensaje de error al intentar acceder a la informacion estática sin internet');
insert into translation_keys(_key, description) values ('static_info_no_internet_error_message', 'Contenido del mensaje de error al intentar acceder a la informacion estática sin internet');
insert into translation_keys(_key, description) values ('forecast_right_now', 'Texto "Justo ahora" del pronostico');
insert into translation_keys(_key, description) values ('enjoy_message', 'Texto de bienvenida al iniciar la aplicación');
insert into translation_keys(_key, description) values ('error', 'Texto "Error"');
insert into translation_keys(_key, description) values ('no_forecast_available', 'Mensaje de error al no poder descargar el pronostico');
insert into translation_keys(_key, description) values ('iwantgo', 'Texto "Quiero asistir" para eventos');
insert into translation_keys(_key, description) values ('event_added', 'Mensaje Popup de evento agregado al calendario');
insert into translation_keys(_key, description) values ('in_calendar', 'Mensaje en lista de publicaciones indicando evento agregado al calendario');

insert into translation_keys(_key, description) values ('long_press_to_delete_meeting', 'Mensaje de "Mantenga presionado" al borrar encuentro en calendario');
insert into translation_keys(_key, description) values ('delete_meeting_confirm_body', 'Mensaje de confirmacion para borrar el encuentro');

-- Filtros de busqueda 
insert into translation_keys(_key, description) values ('orderby_distance', 'Ordenacion de busqueda. "Cercanos primero"');
insert into translation_keys(_key, description) values ('orderby_rating', 'Ordenacion de busqueda "Mejor calificados"');

-- Pronostico
insert into translation_keys(_key, description) values ('forecast_2xx', 'Tormenta electrica');
insert into translation_keys(_key, description) values ('forecast_3xx', 'Llovizna');
insert into translation_keys(_key, description) values ('forecast_5xx', 'Lluevia');
insert into translation_keys(_key, description) values ('forecast_6xx', 'Nieve');
insert into translation_keys(_key, description) values ('forecast_7xx', 'Neblina');
insert into translation_keys(_key, description) values ('forecast_800', 'Despejado');
insert into translation_keys(_key, description) values ('forecast_80x', 'Nubloso');
insert into translation_keys(_key, description) values ('forecast_temperature', 'Temperatura en pantalla PRONOSTICO');
insert into translation_keys(_key, description) values ('forecast_humidity', 'Humedad en pantalla PRONOSTICO');

insert into translation_keys(_key, description) values ('dialog_close', 'Boton "Cerrar" en dialogos.');

-- Calendario
insert into translation_keys(_key, description) values ('duration', 'Texto "Duracion" de un evento');
insert into translation_keys(_key, description) values ('today_at', 'Texto "How a las" de un evento');
insert into translation_keys(_key, description) values ('tomorrow_at', 'Texto "Mañana a las" de un evento');
insert into translation_keys(_key, description) values ('in', 'Preposicion "En" para indicar cuanto falta');
insert into translation_keys(_key, description) values ('days', 'Traduccion de "Dias"');
insert into translation_keys(_key, description) values ('confirm', 'Traduccion de "por favor Confirme"');
insert into translation_keys(_key, description) values ('suggest_calendar', 'Boton de "sugerir calendario"');
insert into translation_keys(_key, description) values ('days_ago', 'traduccion de "Dias atras"');

insert into translation_keys(_key, description) values ('post_detail_weblink', 'Link a pagina web');
insert into translation_keys(_key, description) values ('promoted', 'Destacado');

-- Referencia de tiempos en eventos
insert into translation_keys(_key, description) values ('starts_in', 'Texto "Empieza en"');
insert into translation_keys(_key, description) values ('starts_today_at', 'Texto "Empieza hoy a las"');
insert into translation_keys(_key, description) values ('next_meeting_in', 'Texto "Siguiente encuentro en"');
insert into translation_keys(_key, description) values ('resume_today_at', 'Texto "Continua hoy a las"');
insert into translation_keys(_key, description) values ('in_progress', 'Texto "En progreso"');
insert into translation_keys(_key, description) values ('event_finished', 'Texto "evento finalizado"');
insert into translation_keys(_key, description) values ('select_meetings_msg', 'Texto "Seleccione encuentros a asistir"');



create table translations (
    id bigint(20) primary key AUTO_INCREMENT,
    key_id bigint(20) not null, 
    translation varchar(5000) not null,
    FOREIGN KEY keyfk(key_id) references translation_keys(id)
) engine=InnoDB;

-- 
alter table translations add column language_id bigint(20) not null;
alter table translations add FOREIGN KEY  (language_id) references languages(id);

insert into languages(_name, is_native) values('Ingles', false);
insert into languages(_name, is_native) values('Español', true);

-- Agrego los textos SOLO del idioma español. Futuros idiomas hacerlo desde backoffice
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'dialogs_loading_text'), 'Cargando, por favor espere.');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'post_detail_activity_phone'), 'Teléfono');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'post_detail_activity_address'), 'Dirección');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'title_activity_maps'), 'Map');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'title_activity_language_selection'), 'Selección de lenguaje');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'change_language_message'), 'Seleccione su lenguaje preferido');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'accept_btn'), 'Aceptar');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'language_name'), 'Español');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'dialog_yes'), 'Si');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'dialog_no'), 'No');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'text_insert_filter'), 'Ingrese texto para buscar');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'title_activity_select_zones'), 'Descarga de zonas');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'select_zones_message'), 'Seleccione las zonas turisticas para descargar en este dispositivo. Manten presionada una ya descargada para eliminarla del dispositivo');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'select_zones_download_btn'), 'Descargar');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'downloaded_zone'), 'Ya descargada');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'error_no_internet'), 'No se han podido traer las zonas disponibles para descargar del servidor. Revise su conexión a internte e intente nuevamente');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'deleting_zone_title'), '¡Advertencia!');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'deleting_zone_message'), 'Esta seguro que desea borrar esta zona y las publicaciones asociadas? Esto eliminara de su dispositivo los datos necesarios para poder acceder a las publicaciones de esta zona sin conexión a internet. No se eliminaran los lugares que guardó en favoritos.');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'no_zone_downloaded'), 'Parece que no ha descargado ninguna zona de visita todavia, por favor descargue alguna zona para poder ver las publicaciones');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'new_favorite_text'), 'Agergado a favoritos');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'title_activity_favorite_list'), 'Favoritos');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'text_phone'), 'Telefono');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'text_distance'), 'Distancia');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'delete_favorite_message'), 'Mantena presionado un favorito para eliminar');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'delete_favorite_confirmation_title'), '¿Borrar favorito?');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'delete_favorite_confirmation_body'), '¿Está seguro que quiere borrar el favorito?');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'gallery_text'), 'Galeria');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'no_internet_for_callery'), 'Para ver la galeria de imagenes se requiere una conexión a internet');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'error_loading_image'), 'Error al cargar imagen, revise su conexión a internet e intentelo nuevamente');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'error_rating'), 'Error al votar publicación. Revise su conexión a internet e intentelo nuevamente.');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'report_post'), 'Denunciar publicación');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'report_dialog_title'), 'Denunciando');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'report_dialog_message'), 'Por favor, diganos porqué está reportando esta publicación asi podemos tomar las medidas adecuadas.');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'report_dialog_send'), 'Enviar denuncia');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'report_dialog_cancel'), 'Cancelar');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'error_reporting'), 'Error denunciando publicación. Por favor, revise su conexión a internet e intentelo nuevamente.');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'thanks_for_reporting_title'), 'Listo!');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'thanks_for_reporting'), 'Gracias por su denuncia');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'post_unavailable'), 'No disponible');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'post_unavailable_message'), 'La publicación que está tratando de ver no está disponible en este momento. Disculpe las molestias.');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'static_info_no_internet_error_title'), 'Error');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'static_info_no_internet_error_message'), 'Error adquiriendo la información regional. Por favor, revise su conexión a internet e intentelo nuevamente.');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_right_now'), 'Ahora');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'enjoy_message'), '¡Disfrute su estadía!');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'error'), 'Error');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'no_forecast_available'), 'No hay pronostico disponible, intente nuevamente mas tarde.');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'iwantgo'), 'Quiero asistir!');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'event_added'), 'Evento guardado en la agenda');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'in_calendar'), 'En la agenda');

insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'app_name'), 'Turismo Móvil');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'drawer_forecast'), 'Pronóstico');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'drawer_calendar'), 'Calendario');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'drawer_change_language'), 'Cambiar idioma');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'drawer_select_zones'), 'Seleccionar zonas');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'drawer_static_info'), 'Informacion estática');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'drawer_favorites_list'), 'Favoritos');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'drawer_settings'), 'Ajustes');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'long_press_to_delete_meeting'), 'Mantenga presionado para borrar encuentro');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'delete_meeting_confirm_body'), 'Esta seguro que desea quitar esta visita?');

insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'orderby_distance'), 'Cercanos primero');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'orderby_rating'), 'Mejor calificados primero');


insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_2xx'), 'Tormenta electrica');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_3xx'), 'Llovizna');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_5xx'), 'Lluvia');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_6xx'), 'Nieve');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_7xx'), 'Neblina');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_800'), 'Despejado');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_80x'), 'Nubloso');

insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_temperature'), 'Temperatura');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'forecast_humidity'), 'Humedad');

insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'dialog_close'), 'Cerrar');

insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'duration'), 'Duracion');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'today_at'), 'Hoy a las');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'tomorrow_at'), 'Mañana a las ');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'in'), 'En');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'days'), 'Dias');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'confirm'), 'Confirme por favor');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'suggest_calendar'), 'Sugerir calendario');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'days_ago'), 'Dias atras');

insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'post_detail_weblink'), 'Pagina web');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'promoted'), 'Destacado');

insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'starts_in'), 'Empieza en');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'starts_today_at'), 'Empieza hoy a las ');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'next_meeting_in'), 'Siguiente encuentro en');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'resume_today_at'), 'Continua hoy a las');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'in_progress'), 'En Progreso');
insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'event_finished'), 'Evento finalizado');

insert into translations(language_id, key_id, translation) values ( (select id from languages where _name = 'Español'), 
(select id from translation_keys where _key = 'select_meetings_msg'), 'Seleccione encuentros a asistir');

alter table translation_keys add constraint unique_key unique (_key);

--  tablas para la traduccion

create table post_by_language (
    id bigint(20) primary key AUTO_INCREMENT,
    description varchar(20000),
    _name varchar(128) default null,
    weblink text default null,
    language_id bigint(20) not null,
    post_id bigint(20) not null,
    FOREIGN KEY fk_postid(post_id) REFERENCES posts(id),
    FOREIGN KEY fk_langid(language_id) references languages(id));

create table event_meeting_by_language (
    id bigint(20) primary key AUTO_INCREMENT,
    event_meeting_id bigint(20) not null,
    language_id bigint(20) not null,
    description text,
    FOREIGN key fk_lang(language_id) references languages(id),
    FOREIGN key fk_eventmeeting(event_meeting_id) references event_meetings(id));

-- Claves unicas para una traduccion por lenguaje 
ALTER TABLE post_by_language
ADD CONSTRAINT uniquetranslation UNIQUE KEY(post_id,language_id);

ALTER TABLE event_meeting_by_language
ADD CONSTRAINT uniquetranslation UNIQUE KEY(event_meeting_id,language_id);

-- migro la info de los posts a la traduccion español
insert into post_by_language(post_id, language_id, weblink, _name, description)
select id, (select id from languages where _name LIKE '%Español%'), weblink, _name, description
from posts;

-- Borro las columnas ahora innecesarias.
alter table posts drop column weblink;
alter table posts drop column _name;
alter table posts drop column description; 

-- Agrego el orden de preferencias de los idiomas
alter table languages add column preference integer default 0;
-- Temporalemente asigno las preferencias de acuerdo al id, es decir
-- El primer idioma agregado va a ser el mas preferiedo, el segundo menos, y asi.
update languages set preference = id;

-- Traducciones genericas de modelos
create table model_translations (
    id bigint(20) AUTO_INCREMENT primary key, 
    model varchar(128) not null,
    model_id bigint(20) not null,
    attribute varchar(128) not null, 
    content text not null,
    language_id bigint(20) not null, 
    FOREIGN key fk_lang(language_id) references languages(id)
) engine = INNODB;

-- migro las categorias a la traduccion  español
insert into model_translations(language_id, model, model_id, attribute, content)
select (select id from languages where _name LIKE '%Español%'), 'Category', id, 'name', name
from categories;

-- borro la columna de nombre para las categorias 
alter table categories drop column name;

-- Migro los modulos de informacion estatica 
insert into model_translations(language_id, model, model_id, attribute, content)
select (select id from languages where _name LIKE '%Español%'), 'Module', id, 'mdata', mdata
from modules;
-- mdata pasa a ser un campo traducible, por lo que se remueve la columna
alter table modules drop column mdata;
-- Se agrega la columna plantilla, que es igual en todos los lenguajes
alter table modules add column template varchar(2000);

-- Vuelvo a migrar al nuevo sistema de traduccion
insert into model_translations(model, model_id, attribute, content, language_id)
select 'Post', post_id, '_name', _name, language_id
from post_by_language
where _name is not null;

insert into model_translations(model, model_id, attribute, content, language_id)
select 'Post', post_id, 'description', description, language_id
from post_by_language
where description is not null;

insert into model_translations(model, model_id, attribute, content, language_id)
select 'Post', post_id, 'weblink', weblink, language_id
from post_by_language
where weblink is not null;

-- datos extras para la actualizacion de lenguajes
alter table translations add column created_at timestamp default current_timestamp;
alter table translations add column updated_at timestamp default current_timestamp;

-- datos para deteccion de actualizacion de idioma
alter table languages add column created_at timestamp default CURRENT_TIMESTAMP;
alter table languages add column updated_at timestamp default CURRENT_TIMESTAMP;

-- Borro columna titulo de informacion estatica, ya que ahora es traducible
alter table static_info drop column title;

-- Borro columna description porque es traducible ahora y no va en esta tabla
alter table event_meetings drop column description;

-- Usuario admin de prueba
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `lastname`, `active`, `payment_method_id`, `balance`) VALUES
(1, 'Test', 'test@gmail.com', '$2y$10$MZm..VmdRQNiTF8yP8mLFeNnkWUdqVcDAEsmNLjZK8qi3EjmkjXdG', '3H0DdgNfZbvAUDsEygfXsCAMtGDsNLKuQucr8zWdu2G6ZuiTEP4Px9eFNFHX', '2018-10-22 18:11:31', '2019-08-29 20:44:33', 'Test', 1, 1, 9000);

INSERT INTO `user_has_role` (`user_id`, `role_id`) VALUES
( 1, 1);
INSERT INTO `user_has_role` (`user_id`, `role_id`) VALUES
( 1, 2);

-- Limpieza de tablas obsoletas
drop table post_by_language;
drop table event_meeting_by_language;
alter table static_info drop column html;
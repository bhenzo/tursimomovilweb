module.exports = function (grunt) {
  // Project configuration.
  grunt.initConfig({
    //uglify
    concat: {
      options: {
        separator: ';\n;',
      },
      dist: {
        src: [
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/angular/angular.min.js',
          'bower_components/angular-animate/angular-animate.min.js',
          //'bower_components/angular-sanitize/angular-sanitize.min.js',

          'node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
          'bower_components/angular-route/angular-route.min.js',
          'bower_components/angular-base64-upload/dist/angular-base64-upload.min.js',
          'bower_components/ngmap/build/scripts/ng-map.min.js',
          'bower_components/bootstrap/dist/js/bootstrap.min.js',
          'bower_components/angular-dialog-service/dist/dialogs.min.js',

          'bower_components/textAngular/dist/textAngular-rangy.min.js',
          'bower_components/textAngular/dist/textAngular-sanitize.min.js',

          'bower_components/textAngular/dist/textAngular.min.js',
          'bower_components/angular-ui-tree/dist/angular-ui-tree.min.js',

          'bower_components/moment/min/moment-with-locales.min.js',

          'bower_components/angular-moment-picker/dist/angular-moment-picker.min.js',
          'bower_components/angular-multiple-date-picker/dist/multipleDatePicker.min.js',


          //Aplicacion angular
          'front_end/angularapp.js',
          'front_end/directives.js',

          //Modulos
          'front_end/**/*Module.js',
          'front_end/**/*Srv.js',
          'front_end/**/*Ctrl.js',

        ],
        dest: 'public/app/app.js',
      },
    },

    concat_css: {
      options: {
        // Task-specific options go here.
      },
      all: {
        src: [
          "bower_components/bootstrap/dist/css/bootstrap.css",
          'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
          'bower_components/angular-dialog-service/dist/dialogs.min.css',
          'bower_components/textAngular/dist/textAngular.css',
          'bower_components/angular-ui-tree/dist/angular-ui-tree.min.css',
          'bower_components/angular-moment-picker/dist/angular-moment-picker.min.css',
          'bower_components/angular-multiple-date-picker/dist/multipleDatePicker.css',

          "front_end/app.css",

          "front_end/**/*.css"

        ],
        dest: "public/app/app.css"
      },
    },

    watch: {
      scripts: {
        files: ['front_end/**/*', 'front_end/**'],
        tasks: ['default'],
        options: {
          spawn: false,
        },
      },
    },

    copy: {
      main: {
        files: [
          // includes files within path and its sub-directories
          {
            expand: true,
            cwd: 'front_end/',
            src: ['**/*.html'],
            dest: 'public/app/html/'
          },
          {
            expand: true,
            cwd: 'node_modules/angular-ui-bootstrap/template',
            src: ['**/*'],
            dest: 'public/uib/template'
          },
          {
            expand: true,
            cwd: 'bower_components/bootstrap/fonts',
            src: ['*'],
            dest: 'public/fonts/'
          },

        ],
      },
    },
  });

  // loadNpmTasks
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-concat-css');
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Run Default task(s).
  grunt.registerTask('default', ['concat', 'concat_css', 'copy']);
};

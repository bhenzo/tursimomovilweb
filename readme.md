# Turismo Movil. Parte WEB
---
## Como hacer andar todo esto en windows

1- Instalar NodeJS (para usar el npm)  
2- desde CMD: `npm install -g --production windows-build-tools`  
3- Ir a carpeta del proyecto  
4- Hacer `npm install`  
5- Si dio error, continuar, sino, ir al paso 8  
6- Instalar el Visual Studio Build Tools (buscar en google o usar el que bajo el windows-builds-tools)  
7- Ir al paso 4  
8- Instalar composer (buscar en google, para windows hay un instalador piola)  
9- hacer un `composer install` en carpeta del proyecto (quedarse en la carpeta del proyecto)  
10- Hacer un `npm install -g bower`  
11- Aplicar un `npm install -g grunt-cli` para mas placer  
12- Ejecutar `bower install`  
13- Meterle un buen `grunt` en la pera  
14- CONFIGURAR apache para que levante el proyecto.  
15- Copiar ".env.example" a ".env"  
16- Configurar el ".env" con los datos correspondientes  
17- Hacer un buen `php artisan key:generate` para hacer la KEY del ".env"  
18- Efectuar un `php artisan config:cache`    
19- Ejecutar todos los SQL de "database_definition.sql"  

20- Esto es mucho muy importante: Aumentar el maximo de memoria de php de 128M a 512M
en el archivo "php.ini" sino cuando cargas imagenes muy grandes no anda


Y creo que no me olvidé nada. Y si me olvidé algo, entonces actualizar este archivo.  
---
## Cosas para tener en cuenta  
1- Cada cambio que se hace en algun archivo dentro de "front_end" se ve reflejado
solo al hacer un `grunt`. Es mucho muy importante hacer grunt todo el tiempo.  
2- Cada cambio que se hace en el archivo ".env", solo se va a ver reflejado
cuando se hace un `php artisan config:cache`. Lo que hace es "cachear" la configuracion
que esta en el .env y convertirlo a PHP. Esto es para que no de errores y stuff.  
3- Cada cambio a la base de datos, se agrega el correspondiente `ALTER TABLE` o
`CREATE TABLE` en el archivo "database_definition.sql" abajo de todo. Esto es para
cuando se crea un nuevo entorno de desarrollo, ejecutar ese script sql y altoque
anda toda la base de datos.  
---
### Ingredientes
-Enzo Scalone (Escalenzo)  
-Sergio Veizaga (Servio V-Model)  

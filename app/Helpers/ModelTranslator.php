<?php
namespace App\Helpers;
use \App\Language;
use \App\ModelTranslation;
use Illuminate\Support\Facades\DB;

class ModelTranslator {

    public function loadTranslations(&$model, $columns){
        
        $baseClass = class_basename($model);
        $langs = Language::orderBy('preference', 'ASC')->get();
        foreach ($langs as $lang) {
            foreach($columns as $col) {
                $tr = ModelTranslation::where('model', $baseClass)
                ->where('attribute', $col)
                ->where('model_id', $model->id)
                ->where('language_id', $lang->id)
                ->first();
                if (!isset($model[$col])){
                    $model[$col] = [];
                }
               // $model[$col][$lang->id] = $tr ? $tr->content : null; 
               $nm = $model[$col];
               $nm[$lang->id] = $tr ? $tr->content : ""; 
               $model[$col] = $nm;
            }
            
        }
    }
    /**
     * Carga el modelo con la traduccion mas preferencial como parte del modelo.
     * Si se agrega el ID del lenguaje en $prefLangId, se cargara el modelo con el lenguaje indicado
     * si existen traducciones, sino, se utilizara la mas preferencial existente. 
     */

    public function loadPreferredTranslations(&$model, $columns, $prefLangId = -1) {
        
        //Inicializo los datos que voy a usar
        $baseClass = class_basename($model);
        $langs = Language::orderBy('preference', 'ASC')->get();

        //Se busca si existe la traduccion para el lenguaje preferencial, sino busco 
        //por orden de preferencia
        foreach($columns as $col) {

            //Busca la traduccion para el lenguaje indicado
            if ($prefLangId != -1) {
                $tr = ModelTranslation::where('model', $baseClass)
                ->where('attribute', $col)
                ->where('model_id', $model->id)
                ->where(function($sq){
                    $sq->whereNotNull('content')
                    ->whereRaw("TRIM(content) <> ''");
                })
                ->where('language_id', $prefLangId)
                ->first();
                if ($tr) {
                    $model[$col] = $tr->content;
                    continue;
                }
            }
            //Si no se encuentra la traduccion preferencia, busca la 
            //mas preferible entre todas
            $colTranslation = DB::table('model_translations')
            ->where('model', $baseClass)
            ->where('attribute', $col)
            ->where('model_id', $model->id)
            ->where(function($sq){
                $sq->whereNotNull('content')
                ->whereRaw("TRIM(content) <> ''");
            })
            ->join('languages', 'languages.id', '=', 'model_translations.language_id')
            ->select('content')
            ->orderBy('languages.preference', 'ASC')
            ->first();
            if ($colTranslation){
                $model[$col] = $colTranslation->content;

            } else {
                $model[$col] = "";
            }

 
        }

    }
    
    public function saveTranslations($model, $data, $columns){
        
        $baseClass = class_basename($model);
        $langs = Language::orderBy('id', 'ASC')->get();
        foreach ($langs as $lang) {
            foreach($columns as $col){
                if (!isset($data[$col])){
                    continue;
                }
                $tr = ModelTranslation::where('model', $baseClass)
                ->where('attribute', $col)
                ->where('model_id', $model->id)
                ->where('language_id', $lang->id)
                ->first();
                if (!$tr){
                    $tr = ModelTranslation::create(
                        [
                            'model' => $baseClass,
                            'attribute' => $col, 
                            'model_id' => $model->id, 
                            'language_id' => $lang->id,
                            'content' => 'temp'
                        ]
                    );
                }
               // $model[$col][$lang->id] = $tr ? $tr->content : null; 
               if (isset($data[$col]) && isset($data[$col][$lang->id])){
                $tr->content = $data[$col][$lang->id];
               } else {
                $tr->content = null;
               }
               
               $tr->save();
            }
            
        }
    }
    public function removeTranslations($model){
        ModelTranslation::where('model', class_basename($model))
        ->where('model_id', $model->id)
        ->delete();
    }
}
<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Facade;

class ModelTranslatorFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'modeltranslator';
    }
}




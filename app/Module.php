<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{

    public $table = "modules";
    public $timestamps = false;

    protected $fillable = [
       'morder','staticinfo_id', 'template'
    ];
    
}

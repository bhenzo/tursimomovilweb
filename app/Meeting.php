<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{

    public $table = "event_meetings";
    public $timestamps = true;

    protected $fillable = [
        'start_time', 'end_time', 'post_id', 'cancel_if_rains'
    ];

    protected $dates = ['created_at', 'updated_at', 'start_time', 'end_time'];


}

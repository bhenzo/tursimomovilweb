<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model{
    
    public $table = "zones";
    public $timestamps = false;

    protected $fillable = [
        '_name','_description','_deleted'
    ];

}

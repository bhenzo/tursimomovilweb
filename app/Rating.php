<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    public $table = "ratings";
    public $timestamps = false;

    protected $fillable = [
        'rating', 'tourist_unique_identificator', 'post_id'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostPayment extends Model
{

    public $table = "post_payment";
    public $timestamps = true;

    protected $fillable = [
        'post_id', 'payment_id', 'purchased_seconds', 'amount'
    ];

}

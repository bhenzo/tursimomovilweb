<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    public $table = "payments";
    public $timestamps = true;

    protected $fillable = [
        'amount', 'status', 'payment_code', 'paid_at', 'take', 'from_balance', 'billed_date', 'bill_number'
    ];

    protected $dates = ['paid_at', 'created_at', 'updated_at'];

    protected $hidden = [
        'payment_code'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    public $table = "translations";
    public $timestamps = true;

    protected $fillable = [
        'key_id', 'translation','language_id'
    ];


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslationItem extends Model
{
    public $table = "translation_keys";
    public $timestamps = false;

    protected $fillable = [
        '_key'
    ];


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticInfo extends Model
{

    public $table = "static_info";
    public $timestamps = false;

    protected $fillable = [
        'zone_id'
    ];

}

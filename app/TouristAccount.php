<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TouristAccount extends Model {

    public $table = "tourist_accounts";
    public $timestamps = false;

    protected $fillable = [
        'account'
    ];

    public function favorites(){
      return $this->belongsToMany('App\Post', 'tourist_favorites',  'tourist_account_id', 'post_id');
    }

}

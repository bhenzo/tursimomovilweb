<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public $table = "posts";
    public $timestamps = true;

    protected $fillable = [
       'phone', 'logo', 'address', 'lat', 'lon','_status','user_id','zone_id', '_type'
    ];
    
    protected $dates = ['expiration'];

    public function categories(){
        return $this->belongsToMany('App\Category','post_has_category','post_id','category_id');
    }

    public function meetings(){
        return $this->hasMany('App\Meeting', 'post_id', 'id')->orderBy('start_time');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{

    public $table = "configurations";
    public $timestamps = false;

    protected $fillable = [
        '_key', '_group', 'str_value', 'int_value'
    ];
   

}

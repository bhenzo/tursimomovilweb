<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model{
    
    public $table = "categories";
    public $timestamps = true;

    protected $fillable = [
        'parentcategory_id', 'morder', 'image', 'events_category'
    ];

}

<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;


class CustomerAuth
{

    public function handle($request, Closure $next)
    {

        $user = Auth::user();

        if (!$user){
          abort(403);
        }

        return $next($request);
    }

}

<?php
namespace App\Http\Controllers\Posts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Services\PostsService;
use Illuminate\Support\Facades\Log;
use \App\Post;

use \App\PostPayment;
use \App\Payment;
use \App\Meeting;

use Illuminate\Support\Facades\DB;
use Mail;

use \App\User;
use \App\Photo;
use \App\Rating;
use \App\Report;
use \App\Category;
use \App\Language;

use \App\TouristAccount;

use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class PostsController extends Controller {
    private $srv;

    public function __construct(PostsService $srv){
        $this->srv = $srv;
    }

    public function getPostsUser(Request $r, $user_id){
        
        $posts = Post::where("user_id", $user_id)

                        ->leftJoin('reports as rp', 'rp.post_id', '=', 'posts.id')
                        ->leftJoin('reports as ns_rp', function($join){
                            $join->on('ns_rp.post_id', '=', 'posts.id')
                            ->where('ns_rp._status', '=', 'not_seen');
                        })


                      ->where("posts._status","!=" ,"deleted")
                        ->groupBy('posts.id')
                      ->get(
                        [
                            'posts.*',
                            DB::raw(' count(distinct(rp.id)) as reports_count '),
                            DB::raw(' count(distinct(ns_rp.id)) as not_seen_reports_count ')
                            
                            ]
                      );


    //Busco el lenguaje nativo
    $nativeLang = \App\Language::where('is_native', true)->first();

    foreach ($posts as $post) {
        \ModelTranslator::loadPreferredTranslations($post, ['_name'], $nativeLang ? $nativeLang->id : -1);

    }
                      
       return json_encode($posts); 
    }

    public function blockPostsUser(Request $r, $post_id){
    
        $post = Post::where("id", $post_id)
                    ->first(); 
                    
            //TODO FIXME Poner email aca correspondiente
            /*Mail::send('emails.deleteposts',['publicacion' => $post->_name], function ($m) {
                    $m->from('turismomovilprueba@gmail.com', 'Turismo Movil');
                    $m->to("scalone.enzo@gmail.com", "Enzo")->subject('Aviso Importante!');
            });*/

            $post->_status = "blocked";
            $post->save();
        
        return 'ok';
    }

    public function readReport(Request $r, $id){
        Report::where('id', $id)->update(['_status' => 'seen']);
        return 'ok';
    }

    public function activatePost(Request $r, $id){
        $act = $r->input('activation');
        $post = Post::find($id);
        $post->_status = 'active';

        if ($post->_type != 'free'){
            if (!$act['onlyActivate']){
                $days = $act['days'];
                $post->expiration = Carbon::now()->addDays($days);
            } else {
                $post->expiration = Carbon::now();
            }
        } 
        

        $post->save();
        return 'ok';
        
    }

    public function getReports(Request $r){
        $page_size = 10;

        $reports = Report::join('posts', 'posts.id', '=', 'reports.post_id')
        ->join('users', 'posts.user_id', '=', 'users.id');

        //Si vino un texto filtro por texto
        if ($r->input('filter')){
            $reports = $reports->where(function($q) use($r){
                $q->where('users.email', 'LIKE', '%'.$r->input('filter').'%')
                ->orWhere('users.name', 'LIKE', '%'.$r->input('filter').'%')
                ->orWhere(function($q) use($r){
                    $q->whereRaw("EXISTS (SELECT id FROM model_translations WHERE model = 'Post' and attribute = '_name' and model_id = posts.id AND content LIKE ? )", 
                    ['%'.$r->input('filter').'%']);
                });
            });
        }

        //Si vino un order by
        $mode = $r->input('order_mode');
        if (!$mode) {
            $mode = 'ASC';
        }
        if ($r->input('order_by')){
            switch ($r->input('order_by')) {
                case 'status':
                $reports = $reports->orderBy('reports._status', $mode)//Para que aparezcan primero los no leidos
                ->orderBy('reports.created_at', 'DESC');
                break;
                case 'post_name':
                $reports = $reports->orderBy('posts._name', $mode);
                    break;
                case 'client_email':
                $reports = $reports->orderBy('users.name', $mode);
                    break;
                case 'date':
                $reports = $reports->orderBy('reports.created_at', $mode);
                    break;
                default:
                   
                    break;
            }
        }

        //Primero traigo todos los items para la paginacion
        $total_items = $reports->count();

       //Paginacion
       if ($r->input('page')){
            $reports = $reports->skip(($r->input('page') - 1) * $page_size)->take($page_size);
       }


          $reports = $reports->get(['reports.*'/*, 'posts._name as post_name'*/, 'posts.id as post_id',
          'users.email as user'
        ]);

       foreach ($reports as $rep) {
           $mdl = Post::find($rep->post_id);
           \ModelTranslator::loadPreferredTranslations($mdl, ['_name']);
           $rep->post_name = $mdl->_name;
       }

        return json_encode([
            'reports' => $reports,
            'page' => $r->input('page'),
            'page_size' => $page_size,
            'total_items' => $total_items
        ]);
    }
    
    public function getPaymentHistory(Request $r){
        $user = Auth::user();
        $id = $user->id;

        $posts = Post::where("user_id", $id)
                                ->join('post_payment','post_payment.post_id','=','posts.id')
                                ->join('payments','payments.id','=','post_payment.payment_id') 
                                ->select('posts._name','posts._type','post_payment.purchased_seconds','payments.status',
                                'payments.amount','payments.paid_at','payments.payment_code')
                                ->get();
             
        return json_encode($posts);
        
    }

    public function getPosts(Request $r){
       
        $user = Auth::user();
        $id = $user->id;

        $posts = Post::where("user_id", $id)
                                ->where("posts._status","!=" ,"deleted")
                                ->join('zones','posts.zone_id','=','zones.id')
                                ->select('posts.*','zones._name as zone_name')->get();


        $nativeLang = \App\Language::where('is_native', true)->first();
        $nativeLangId = $nativeLang?$nativeLang->id:-1;
        //Calculo los segundos para que expire cada post
        foreach ($posts as $post) {

            \ModelTranslator::loadPreferredTranslations($post, ['_name', 'description', 'weblink'], $nativeLangId);

            $post->load('categories');
            $post->load('meetings');

            foreach ($post->categories as $cat) {
                \ModelTranslator::loadPreferredTranslations($cat, ['name'], $nativeLangId);

            }

            foreach ($post->meetings as $meeting) {
                \ModelTranslator::loadPreferredTranslations($meeting, ['description'], $nativeLangId);
            }

            //TODO FIXME Optimizacion pendiente (hacer todo en una query)
            $rating = DB::table('ratings')
            ->where('post_id', $post->id)
            ->avg('rating');
            //si es null lo pongo en -1 
            if (is_null($rating)){
                $rating = -1;
            }

            $post->rating = $rating;
            $post->ratingStars = round($rating);

            //si es gratis, no expira
          if ($post->_type == 'free'){
            continue;
          }

          //TODO temp (para poner expiracion a los que no tienen)
          if (!$post->expiration){

            $post->expiration =  Carbon::now();

            $tosave = Post::find($post->id);
            $tosave->expiration = Carbon::now();
            
            $tosave->save();
          }
          $now = Carbon::now();

          //Sino, si que expira eh
          $post->secondsToExpire = $post->expiration->diffInSeconds($now);
          if ($post->expiration->lessThan($now)){
            $post->secondsToExpire = 0;
          }


          //Calculo cuantos segundos hay entre el ultimo pago exitoso y el vencimiento
          $lastPaymentSuccessfull =
          Payment::join('post_payment as pp', 'pp.payment_id', '=', 'payments.id')
          ->where('pp.post_id', $post->id)
          ->where('payments.status', 'success')
          ->orderBy('pp.created_at', 'DESC')
          ->select(['payments.*'])
          ->first();
          $lapse = 300;
          if ($lastPaymentSuccessfull){
            $lapse = $post->expiration->diffInSeconds($lastPaymentSuccessfull->paid_at);
          }
          $post->payedLapseInSeconds = $lapse;

        }




        return json_encode($posts);

    }

    public function getCategoriesFromDate($date){
        $categories = Category::where('updated_at', '>', $date)->get();
        return $categories;
    }


    public function getApiUpdate(Request $r, $zonesId, $lastPostDate, $lastCategoryDate, $langId){
        //$tempLangId = Language::orderBy('preference', 'ASC')->first()->id;

        //SI no se obtuvo el id del lenguaje desde el cliente android, se elige el preferencial.
        if ($langId == -1){
            $langId = Language::orderBy('preference', 'ASC')->first()->id;
        }

        $zonesIdArr = explode(',', $zonesId);

        $posts = Post::/*where("posts._status", "active")
        ->*/join('users as us', 'us.id', '=', 'posts.user_id')
        ->where('us.active', true)
        ->whereIn('zone_id',$zonesIdArr)
        ->where('posts.updated_at', '>', $lastPostDate)
        /*->where(function($q){
           $q->where('posts.expiration', '>', Carbon::now()->format('Y-m-d H:i:s'));
           $q->orWhereIn('posts._type', ['free', 'event']);
        })*/
        ->get(['posts.*']);

        //Traduccion de posts 
        foreach ($posts as $post) {
            \ModelTranslator::loadPreferredTranslations($post, ['_name', 'description', 'weblink'], $langId);
        }

        //traigo las nuevas categorias desde lastcategorydate 
        $categories = $this->getCategoriesFromDate($lastCategoryDate);
        $allCats = Category::get();
        $activeCategories = [];
        foreach ($allCats as $cat) {
            $activeCategories[] = $cat->id;
        }
        foreach ($categories as $cat) {
            \ModelTranslator::loadPreferredTranslations($cat, ['name'], $langId);
        }


        //TODO FIXME Optimizacion pendiente (hacer todo en una query)
        foreach ($posts as $post) {
            $post->load('categories');
            $post->load('meetings');
            foreach ($post->meetings as $meet) {
                \ModelTranslator::loadPreferredTranslations($meet, ['description'], $langId);
            }

            $rating =  DB::table('ratings')
            ->where('post_id', $post->id)
            ->avg('rating');
            //si es null lo pongo en -1 porque en android no se puede poner null a un double nativo. 
            if (is_null($rating)){
                $rating = -1;
            }

            $post->rating = $rating;
        }

        //Armo el update pack :V
        $pack = [
            'posts' => $posts,
            'categories' => $categories,
            'active_categories' => $activeCategories
        ];

        //\Log::info(json_encode($pack));

        return json_encode($pack);
    }

    public function reportPostApi(Request $r, $id){
        $uid = $r->input("android_id");
        $msg = $r->input('message');

        if (!$uid){
            abort(422, "Se requiere AID para denunciar");
        }

  
        Report::create([
            'tourist_unique_identificator' => $uid, 
            'message' => $msg, 
            'post_id' => $id
        ]);

        return 'ok';
    }

    public function ratePostApi(Request $r, $id){
        $uid = $r->input("android_id");
        $rating = $r->input('rating');
        if (!$uid || !$rating){
            abort(422, "No se puede puntear. Faltan datos");
        }

       Rating::updateOrCreate(
           ['tourist_unique_identificator' => $uid, 'post_id' => $id],
           ['rating' => $rating]);

        Post::find($id)->touch();

        return 'ok';


    }

    public function _cancelPostTime($id){
        $post = Post::find($id);
        $postPayment = PostPayment::where('post_id', $id)
        ->join('payments', 'payments.id', '=', 'post_payment.payment_id')
        ->where('payments.status', 'success')
        ->orderBy('payments.paid_at', 'DESC')
        ->select(['post_payment.*'])
        ->first();
        if (!$postPayment){
            return;
        }

        $payment = Payment::find($postPayment->payment_id);
        $timePaid = $payment->paid_at;

        //Veo si ya vencio, salgo
        $secondsToExpire = $post->expiration->diffInSeconds(Carbon::now());
        if ($post->expiration->lessThan(Carbon::now())){
            $secondsToExpire = 0;
        }
        if ($secondsToExpire == 0){
            return;
        }

        //Calculo el porcentaje usado
        $usedSeconds = Carbon::now()->diffInSeconds($timePaid);
        $purchasedSeconds = $postPayment->purchased_seconds;
        $factor = 1 - $usedSeconds / $purchasedSeconds;
        $balance = $factor * $postPayment->amount;
        Log::info(json_encode($postPayment));
        Log::info("Devolviendo $balance de $postPayment->amount ID $postPayment->id" );
        //ahora le devuelvo la platita
        $user = Auth::user();
        $user->balance += $balance;
        $user->save();

        $post = Post::find($id);
        $post->expiration = Carbon::now();
        $post->save();
    }

    public function cancelPostTime(Request $r, $id){
        DB::transaction(function() use ($r, $id){
            $this->_cancelPostTime($id);
        });
    }
    //Traigo solo las activas DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED 
    //TODO FIXME Creo que no se usa, revisar
    public function getPostsApi(Request $r){

        $posts = Post::where("posts._status", "active")
        ->join('users as us', 'us.id', '=', 'posts.user_id')
        ->where('us.active', true)
        ->where(function($q){
           $q->where('posts.expiration', '>', Carbon::now()->format('Y-m-d H:i:s'));
           $q->orWhere('posts._type', 'free');
        })
        ->get(['posts.*']);

        //TODO FIXME Optimizacion pendiente (hacer todo en una query)
        foreach ($posts as $post) {
            $post->rating =  rand(10, 50) / 10.0;
        }


       return json_encode(
         $posts
       );
    }

    public function checkLink(Request $r){
        $link = $r->input('link');

        $client = new \GuzzleHttp\Client();
        return json_encode($client->head($link, ['connect_timeout' => 3.14]));
    }

    public function deletePost(Request $r, $id){
        //Implementacion de una baja logica de un post
        $post = Post::find($id);
        $post->_status = "deleted";
        $post->save();
    }

    public function getPost(Request $r, $id){
        $post = Post::find($id);
        $user = Auth::user();

        //Le pongo las categorias 
        $post->load('categories');
        foreach ($post->categories as $cat) {
            \ModelTranslator::loadPreferredTranslations($cat, ['name']);
        }
        //Tambien los meetings por si es un evento
        $post->load('meetings');
        //le cargo las traducciones a los meetings
        foreach ($post->meetings as $meet) {
          
            \ModelTranslator::loadTranslations($meet, ['description']);
        }


        //Le cargo las imagenes 
        $post->album = Photo::where('post_id', $id)->get();

        //Cambio algunos nombrecillos
        $post->type = $post->_type;

        //Esto es para cuando un admin lo ve, que solo pueda verlo
        $post->disableEditing = $post->user_id != $user->id;

       
    \ModelTranslator::loadTranslations($post, ['description', '_name', 'weblink']);

        return json_encode($post);
    }

    public function getPostApi(Request $r, $id){
      return json_encode(Post::find($id));
    }


    public function savePost(Request $r){
       
        //Genero el nombre de la imagen bassado en el tiempo que subio y extension
        $user = Auth::user();
        $data = $r->input('data');

        //Veo si es un evento, y el usuario puede cargar eventos
        if ($data['type'] == 'event'){
            $eventEnable = false;
            $user->load('roles');
            foreach($user->roles as $role){
                if ($role->id_str == 'EVENT_MANAGER'){
                    $eventEnable = true;
                }
            }
            if (!$eventEnable){
                abort(422, 'El usuario no puede cargar eventos');
            }
        }

        DB::beginTransaction();

        //Veo si es para guardar un post existente
        $id = @$data['id'];
        $savedPost = null;
        $fileName = null;
        $milliseconds = round(microtime(true) * 1000);

        //Ahora creo el logo chiquito
        //TODO FIXME Aca puede llegar a fallar en LINUX por el tema de las rutas y eso.
        $logosPath = Storage::disk('logos_uploads')->getDriver()->getAdapter()->getPathPrefix();
        $photosPath = Storage::disk('photos_uploads')->getDriver()->getAdapter()->getPathPrefix();
        
        if ($id){
            $savedPost = Post::find($id);
        }
        

        //Si vino una imagen nueva (en base 64)
        if (isset($data['image'])){

            //Viene imagen. Pero tengo que ver si habia una ya, pa borrarla
            if ($id){
                Storage::disk('logos_uploads')->delete($savedPost->logo);
                Storage::disk('logos_uploads')->delete("thumbnail-" . $savedPost->logo);
            }

            $image = $data['image'];
            $ext = pathinfo( $image['filename'], PATHINFO_EXTENSION);
            $fileName = $user->id . '-' . $milliseconds . '.' . $ext;
            $imgBytes = base64_decode($image['base64']);
    
            if(!Storage::disk('logos_uploads')->put($fileName, $imgBytes)) {
                DB::rollback();
                return false;
            }
    
            $imgBytes = null;
            $image['base64'] = null;


            $img = Image::make($logosPath . $fileName);
            $img->resize(200, 170);
            $img->save($logosPath . "thumbnail-" . $fileName);
            $img = null;
        }
        

        $description = @$data["description"];
        //Un poco de control por si quieren hacerse los hackers
        if ($data["type"] == "free"){
          $description = null;
        }

        //Si es un post existente y cambio el tipo, lo venzo. 
        if ($savedPost && $savedPost->_type != $data['type']){
           // $savedPost->expiration = Carbon::now();
            //$savedPost->save();
            $this->_cancelPostTime($savedPost->id);
        }

        //Lo creo aca para tener el id despues para las fotitos
        if (!$savedPost){
            $post = Post::create([
                //"_name" => $data["_name"],
                "phone" => $data["phone"],
                "logo" => $fileName,
                "lat" => $data["lat"],
               // "weblink" => @$data["weblink"],
                "lon" => $data["lon"],
                "address" => $data["address"],
                "_status" => "active",
                "user_id" => $user->id,
                "zone_id" => $data["zone_id"],
                //"description" => $description,
                "_type" => $data["type"]
            ]);
        } else {
            $updateData = [
               // "_name" => $data["_name"],
                "phone" => $data["phone"],
                "lat" => $data["lat"],
                "lon" => $data["lon"],
                //"weblink" => @$data["weblink"],
                "address" => $data["address"],
                "_status" => "active",
                "zone_id" => $data["zone_id"],
                //"description" => $description,
                "_type" => $data["type"]
            ];
            if ($fileName){
                //Lo actualizo solo si se cambio
                $updateData["logo"] = $fileName;
            }

            $savedPost->update($updateData);

            //Asigno asi pa las modificaciones siguientes, ya lo habia hecho con el nombre $post 
            $post = $savedPost;
        }

        
        //Guardo los datos traducidos
        /*if (isset($data['_name'])){
            foreach ($data['_name'] as $langId => $translation) {
                $trans = $this->retrieveTranslation($langId, $post->id);
                $trans->_name = $translation;
                $trans->save();
            }
        }
        if (isset($data['description'])){
            foreach ($data['description'] as $langId => $translation) {
                $trans = $this->retrieveTranslation($langId, $post->id);
                $trans->description = $translation;
                $trans->save();
            }
        }
        if (isset($data['weblink'])){
            foreach ($data['weblink'] as $langId => $translation) {
                $trans = $this->retrieveTranslation($langId, $post->id);
                $trans->weblink = $translation;
                $trans->save();
            }
        }*/
        \ModelTranslator::saveTranslations($post, $data, ['weblink', 'description', '_name']);
        
    
        $post->touch();
        $catSelecteds = $data["categories"];
        $catsIds = [];
        foreach ($catSelecteds as $cat) {
            $catsIds[] = $cat['id'];
        }
        //Les guardo estas.
        $post->categories()->sync($catsIds);

        //Guardo las imagenes del album si vinieron algunas, y si el gold
        if ($data['type'] == 'gold'){
            $photos = $data["album"];
            $phNum = 0;

            //Veo si hay fotos cambiadas. Borro las que estaban pero ahora no
            if ($id){
                //Agarros los IDS que vinieron da las fotos que no son nuevas
                $arrivedImageIds = [];
                foreach($photos as $ph){
                    if (isset($ph['id'])){
                        $arrivedImageIds[] = $ph['id'];
                    }
                }


                $imgsToRemove = Photo::where('post_id', $id)
                ->whereNotIn('id', $arrivedImageIds)
                ->get();
                //Borro del disco estas
                foreach($imgsToRemove as $todel){
                    Storage::disk('photos_uploads')->delete($todel->image);
                    $todel->delete();
                }

            }

            foreach ($photos as $ph) {
                if (!isset($ph['base64'])){
                    continue;
                }

            $phBytes = base64_decode($ph['base64']);
            $phExt = pathinfo($ph['filename'], PATHINFO_EXTENSION);
            $phFileName = $user->id . '-' . $milliseconds . '-' . $phNum . '.' . $phExt;

            //Guardo real size.
            if(!Storage::disk('photos_uploads')->put($phFileName, $phBytes)) {
                DB::rollback();
                abort(500, "ERROR MORTAL AL SUBIR LAS IMAGENES");
            }
            //Libero un poquito de memoria pa que no explote el php
            $phBytes = null;
            $ph['base64'] = null;
            //Guardo miniatura para android
            $img = Image::make($photosPath . $phFileName);
            $img->resize(100, 80);

            // use callback to define details
            // draw a red line with 5 pixel width
            /*$img->line(0, 0, 100, 80, function ($draw) {
                $draw->color('#00f');
            });*/
            $img->save($photosPath . "thumbnail-" . $phFileName);
            $img = null;
            //Si todo salio bien, guardo en la base de datos la nueva foto
            Photo::create([
                'post_id' => $post->id,
                'image' => $phFileName
            ]);
            //Incremento el numero
            $phNum++;
            }
        } else {
            //SINO, borro las imagenes que tenia, si es que tenia
            $imgsToRemove = Photo::where('post_id', $post->id)
            ->get();
            //Borro del disco
            foreach($imgsToRemove as $todel){
                Storage::disk('photos_uploads')->delete($todel->image);
                $todel->delete();
            }
        }

        //Guardo si es un evento, los meetings
        //TODO FIXME falta controlar que no mande type"evento" si no es user permitido a eventos
        
        //acomodo los meetings
        $mids = [];
        foreach ($data['meetings'] as $meeting) {
            if (isset($meeting['id'])){
                $mids[] = $meeting['id'];
            }
        }

        $eventMeetings = Meeting::where('post_id', $post->id)->whereNotIn('id', $mids)->get();
        foreach ($eventMeetings as $meet) {
           \ModelTranslator::removeTranslations($meet);
            $meet->delete();
        }

        if ($post->_type == 'event'){
            foreach ($data['meetings'] as $meeting) {
                $meeting['post_id'] = $post->id;

                if (isset($meeting['id'])){
                    $createdMeeting = Meeting::find($meeting['id']);
                    $createdMeeting->update($meeting);
                } else {
                    $createdMeeting = Meeting::create($meeting);
                }

              
                \ModelTranslator::saveTranslations($createdMeeting, $meeting, ['description']);
            
            }
        }

        DB::commit();
        //Log::info(json_encode($r->all()));



    }

   

    public function getPhotosApi(Request $r, $postId){
        return json_encode(Photo::where('post_id', $postId)->get());
    }

    public function getPostsByZoneApi(Request $r, $zoneId){

        $posts = Post::/*where("posts._status", "active")
        ->*/join('users as us', 'us.id', '=', 'posts.user_id')
        ->where('us.active', true)
        ->where('zone_id',$zoneId)
      //  ->where(function($q){
           //$q->where('posts.expiration', '>', Carbon::now()->format('Y-m-d H:i:s'));
         //  $q->orWhere('posts._type', 'free');
      //  })
        ->get(['posts.*']);



        //TODO FIXME Optimizacion pendiente (hacer todo en una query)
        foreach ($posts as $post) {
            $post->load('categories');
            //$post->load('meetings');
            $rating =  DB::table('ratings')
            ->where('post_id', $post->id)
            ->avg('rating');
            //si es null lo pongo en -1 porque en android no se puede poner null a un double nativo. 
            if (is_null($rating)){
                $rating = -1;
            }

            $post->rating = $rating;
        }

       return json_encode(
         $posts
       );

        
    }

    public function getPostsByTouristEmailApi(Request $r, $email){
        //Traigo los posts relacionados con ese correo
        //TODO mas adelante se podria hacer algo de login o algo, pero
        //Como no son datos privados ni criticos, que se maneje la seguridad
        return json_encode(
          Post::join('tourist_favorites', 'posts.id', '=', 'tourist_favorites.post_id')
              ->join('tourist_accounts', 'tourist_favorites.tourist_account_id', '=', 'tourist_accounts.id')
              ->where('tourist_accounts.account', $email)->get(['posts.*'])
        );

    }

    public function saveFavoritesApi(Request $r, $email){
        if (!$email || strlen($email) == 0){
            abort(403, "No hay email definido");
        }
        //Convierto los IDS de los post a guardar en favoritos, que vienen
        //En un string separado por coma, en un array.
        $postsIds = explode(',', $r->all()[0]);

        //Busco al turista por email, si no esta, lo creo on the fly.
        $tourist = TouristAccount::where('account', $email)->first();
        if (!$tourist){
          $tourist = TouristAccount::create([
            'account' => $email
          ]);
        }

        //Asocio los posts de los ids que vinieron al ameo turista
        foreach ($postsIds as $pid) {
          $tourist->favorites()->detach($pid);
          $tourist->favorites()->attach($pid);
        }

    }


    function resize_image($file, $w, $h, $crop=FALSE) {
        ini_set('memory_limit', 1024 * 1024 * 40);


        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }
        Log::info("lleva$file");

        $src = imagecreatefromjpeg($file);

        $dst = imagecreatetruecolor($newwidth, $newheight);

        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }

}

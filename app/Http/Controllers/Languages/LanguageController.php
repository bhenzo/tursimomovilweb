<?php
namespace App\Http\Controllers\Languages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use \App\TranslationItem;
use \App\Translation;
use \App\Language;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use \App\ModelTranslation;


class LanguageController extends Controller {

    public function getTranslationItems(Request $r){
        $items = TranslationItem::get();

        //Traigo el lenguaje para las traducciones
        $nativeLang = Language::where('is_native', true)->first();

        //Si no hay nativo, agarro cualquiera
        if (!$nativeLang){
            $nativeLang = Language::orderBy('id')->first();
        }
        foreach ($items as $item) {
            $trans = Translation::where('key_id', $item->id)
            ->where('language_id', $nativeLang->id)->first();
            if ($trans) {
                $item->native = $trans->translation;
            }
        }

        return json_encode($items);
    }

    public function getNativeTranslation(Request $r){
        $lang = Language::where('is_native', true)->first();
        if (!$lang){
            $lang = Language::orderBy('id')->first();
        }
        $nativeTrans = 
        Translation::where('language_id', $lang->id)
        ->join('translation_keys as tk', 'tk.id', '=', 'translations.key_id')
        ->get(['tk._key', 'translations.translation']);

        $keys = [];
        foreach ($nativeTrans as $item) {
           $keys[$item['_key']] = $item['translation'];
           
        };
        return json_encode($keys);
    }

    public function getApiLanguageUpdatePack(Request $r, $last_lang_update, $last_translation_update){
        //Se comprueba si hay algun cambio, Si lo hay, se envian TODOS
        //los lenguajes y traducciones, y no solo las actualizaciones
        $updLang = Language::where('updated_at', '>', $last_lang_update)->first();
        $updTrans = Translation::where('updated_at', '>', $last_translation_update)->first();
        if ($updLang || $updTrans){
            //Hay un cambio, se envia todo el paquete de traduccion
            $pack = [
                'languages' => Language::get(),
                'keys' => TranslationItem::get(),
                'translations' => Translation::get()
            ];

            return json_encode($pack);
        }
        

    }

    public function removeLanguageAndReferences(Request $r){
        $langId = $r->input('id');
        DB::transaction(function() use($langId) {
            ModelTranslation::where('language_id', $langId)->delete();
            Translation::where('language_id', $langId)->delete();

            Language::find($langId)->delete();
        });
    }

    public function getLanguages(Request $r){
        $languages = Language::orderBy('preference', 'asc')->get();

        //Se ve cuantas traducciones hay que hacer y si se hicieron en cada lenguaje
        //Para ver si hay lenguajes incompletos
        $itemQty = TranslationItem::count();

        foreach ($languages as $lang) {
            $lang->missing_translations = 
                Translation::where('language_id', $lang->id)->count() != $itemQty;

                //Se busca la traduccion de cada item, si tiene, sino, se agrega  el item pero sin
                //La traduccion
            $lang->translations = 
            TranslationItem::leftJoin('translations as trs', function($join) use($lang) {
                $join->on('trs.key_id', '=', 'translation_keys.id')
                ->where('trs.language_id', '=', $lang->id);
            })
            /*->where('trs.language_id', $lang->id)*/->get(['translation_keys.*', 'trs.translation']);
        }
        
        return json_encode($languages);
    }

    public function setNative(Request $r, $lang_id){
        Language::where('id', '<>', $lang_id)->update(['is_native' => false]);
        Language::where('id', $lang_id)->update(['is_native' => true]);

    }
    public function saveLanguagePreference(Request $r){
        $idsInOrder = $r->input('ids');
        $currOrder = 0;
        foreach ($idsInOrder as $id) {
            $lang = Language::find($id);
            $lang->preference = $currOrder;
            $lang->save();
            $currOrder++;
        }
        //Actualiza fecha de update de traducciones
        //Para forzar descarga en dispositivos
        Translation::query()->update(['updated_at' => DB::raw('current_timestamp')]);
        //TODO FIXME lo siguite no es necesario, es para reflejar los cambios en los POSTS 
        //desde la app android
        //\App\Post::query()->update(['updated_at' => DB::raw('current_timestamp')]);
    }
    public function saveTranslation(Request $r){
        $data = $r->all();

        //Se crea la entrada para el lenguaje si es nuevo.
        if (!isset($data['id'])){
            $maxPreference = Language::orderBy('preference','DESC')->first()->preference;
            $data['id'] = Language::create([
                '_name' => $data['name'],
                'preference' => $maxPreference + 1
            ])->id;
        }


        foreach ($data['translations'] as $trans) {
            if (!isset($trans['translation'] )){
                //El lenguaje se esta guardando pero no se tradujeron todos los elementos
                continue;
            }
            $existingTrans = Translation::where('language_id', $data['id'])
            ->where('key_id', $trans['id'])->first();
            if ($existingTrans){
                $existingTrans->translation = $trans['translation'];
                $existingTrans->save();
            } else {
                 Translation::create([
                    'key_id' => $trans['id'],
                    'language_id' => $data['id'],
                    'translation' => $trans['translation'] 
                ]);
            }

        }
    }

    public function getTranslation(Request $r, $language_id){
        return json_encode(Translation::where('language_id', $language_id)->orderBy('id')->get());
    }


}
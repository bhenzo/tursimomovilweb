<?php
namespace App\Http\Controllers\Payments;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use \App\Post;
use \App\User;
use \App\Promo;
use Illuminate\Support\Facades\DB;
use \App\PostPayment;
use \App\DataUser;
use \App\Payment;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use \App\Services\TodoPagoService;

class PaymentsController extends Controller {

    protected $tpSrv = null;

    public function __construct(TodoPagoService $tpSrv){
        $this->tpSrv = $tpSrv;
    }

    public function savePromos(Request $r){
      DB::transaction(function() use ($r) {
        $allPromos = $r->all();
        $validsIds = [];
        $availablePromos = ['silver', 'gold'];
        //Voy guardando una por una, si es nueva o no. Y guardo el ID de las que creo
        //Entonces despues borro todas las que no esten dentro de esos ids
        foreach ($availablePromos as $prName) {
          $promos = $allPromos[$prName];
          foreach($promos as $promo){
            if (isset($promo['id'])){
              //Vino con ID, actualizo y agrego el ID
              $validsIds[] = $promo['id'];
              Promo::find($promo['id'])->update($promo);
            } else {
              //Vino sin ID (es nueva) la guardo y traigo el ID que vino y lo meto al array
              $newPromo = Promo::create($promo);
              $validsIds[] = $newPromo->id;
            }
          }
        }
        //ahora, en validsIds tengo TODOS los ids que solos quiero que esten, borro los que no.
        Promo::whereNotIn('id', $validsIds)->delete();

        //Listo :v
      });
    }

    public function getPromos(Request $r){
      return json_encode(Promo::get());
    }

    public function getPayment(Request $r, $id){
        return json_encode(Payment::find($id));
    }

    public function testTp(Request $r){
        $pk = $this->tpSrv->getPublicKey();
        return $pk;
    }

    private function confirmPayment($postsPromo, $success){

        $total = 0;
        //Recorro para ver el total
        foreach ($postsPromo as $pp) {
            if (!isset($pp['payment_type_selected'])){
              continue;
            }
            $total += $pp['payment_type_selected']['price'];
        }
        //Creo el pago
        //TODO el estado despues puede no ser success
        DB::beginTransaction();
        $payment = Payment::create([
          'amount' => $total,
          'result' => $success ? 'success' : 'failed'
        ]);
        //Recorro para asignar el pago, haya salido como sea, para registrar
        foreach ($postsPromo as $pp) {
          if (!isset($pp['payment_type_selected'])){
            continue;
          }
          PostPayment::create([
            'post_id' => $pp['id'],
            'payment_id' => $payment->id,
            'amount' => $pp['price']
          ]);
        }
        //Si la transaccion fue exitosa, le agrego los tiempos y todo lo que
        //Corresponda a las publicaciones
        if ($success){
          foreach ($postsPromo as $pp) {
            if (!isset($pp['payment_type_selected'])){
              continue;
            }
            $post = Post::find($pp['id']);

            //Si ya expiro, el tiempo es desde NOW
            $post->expiration = Carbon::now()->addSeconds($pp['payment_type_selected']['time']);
            $post->save();
            //TODO Si no expiro, se le suma el tiempo

          }
        }
        DB::commit();

    }



    private function _paymentOk($order) {
      $orderCode = $order;
      $payment = Payment::where('payment_code', $orderCode)->first();
      $payment->status = 'success';
      $payment->paid_at = Carbon::now();
      $payment->save();

      if ($payment->from_balance > 0){
          //Traigo el usr
          $somePostPayment = PostPayment::where('payment_id', $payment->id)
          ->first();
          $somePost = Post::find($somePostPayment->post_id);
          $user = User::find($somePost->user_id);
          $user->balance -= $payment->from_balance;
          $user->save();
      }

      $promos = PostPayment::where('payment_id', $payment->id)->get();
      foreach ($promos as $promo) {
          $post = Post::find($promo->post_id);

          //Si expiro o no tiene, le agrego desde NOW, sino, se los sumo
          if (!$post->expiration || $post->expiration->lessThan(Carbon::now())){
              $post->expiration = Carbon::now()->addSeconds($promo->purchased_seconds);
          } else {
            $post->expiration = $post->expiration->addSeconds($promo->purchased_seconds);
          }
          
          $post->save();
      }

    }

    public function paymentOk(Request $r){
        $this->_paymentOk($r->query('order'));

        return view('closewindow');
    }
    public function paymentErr(Request $r){
        $orderCode = $r->query('order');
        $payment = Payment::where('payment_code', $orderCode)->first();
        $payment->status = 'failed';
        $payment->save();
        return view('paymenterror');
    }

    public function confirmOrder(Request $r){
        //Datos para la compra
        $user = Auth::user();
        $info = DataUser::where('user_id', $user->id)->first();
        if (!$info){
          return json_encode([
            'result' => 'err',
            'message' => 'Cargue primero los datos de usuario.'
          ]);
        }

        $info = $info->toArray();
        $info['address'] = $info['_address'];
        $info['name'] = $user->name;
        $info['lastname'] = $user->lastname;
        $info['email'] = $user->email;

        //Agarro la orden
        $orderUnfiltered = $r->input('posts');
        $userInformation = $r->input('userInformation');
        //Solo dejo los posts que tienen alguna cantidad de segundos para comprar
        $order = [];
        foreach ($orderUnfiltered as $pp) {
            if (isset($pp['payment_type_selected'])){
              $order[] = $pp;
            }
        }
        $total = 0;
        //Ahora, calculo el total. Tengo que buscar las promos en la DB de nuevo
        //Para que no halla un vivillo que mande los precios cambiados
        foreach ($order as &$itm) {
            $promo = Promo::find($itm['payment_type_selected']['id']);
            $itm['purchased_seconds'] = $promo->time;
            $total += $promo->price;
        }

        //Si tiene saldoa favor, lo uso
        $balance = $user->balance;
        $takenFromBalance = 0;
        if ($balance <= $total){
          $total -= $balance;
          $takenFromBalance = $balance;
        } else {
          $takenFromBalance = $total;
          $total = 0;
          $user->balance -= $total;
        }

        //Ahora registro la solicitud de pago.

        //Genero el codigo de pago (algo medio random)
        $orderCode = $user->id . md5($user->email . "-" . microtime());
        //Los posibles  status son "unpaid", "success" o "failed"
        DB::beginTransaction();
        $payment = Payment::create([
          'amount' => $total,
          'status' => 'unpaid',
          'from_balance' => $takenFromBalance,
          'payment_code' => $orderCode
        ]);

        //Asigno el tiempo a pagar en cada publicacion
        foreach ($order as $pp) {
          PostPayment::create([
            'post_id' => $pp['id'],
            'payment_id' => $payment->id,
            'purchased_seconds' => $pp['purchased_seconds'],
            //TODO OJO, lo que sigue lo pueden hackear y explota todo
            'amount' => $pp['payment_type_selected']['price']
          ]);
        }

        DB::commit();

        //Veo si tiene plata, directamente la descuento y listo
        if ($total == 0){
          $this->_paymentOk($orderCode);
          return json_encode([
            'result' => 'from_balance'
          ]);
        }

        //Solicito el pago a todopago
        $res = $this->tpSrv->getPayForm(
            $orderCode,
            $r->ip(),
            $user->id,
            $total,
            $info
          );

        if ($res['status'] == 'ok'){
          return json_encode([
            'result' => 'ok',
            'form_url' => $res['form_url'],
            'payment_id' => $payment->id
          ]);
        } else {
          return json_encode([
            'result' => 'err',
            'message' => $res['message']
          ]);
        }

    }

    public function getPaymentHistory(Request $r){
      $user = Auth::user();
      $payments = Payment::distinct()->join('post_payment as pp', 'pp.payment_id', '=', 'payments.id')
      ->join('posts', 'posts.id','=', 'pp.post_id')
      ->join('users', 'users.id','=', 'posts.user_id')
      ->where('users.id', $user->id)
      ->whereIn('payments.status', ['success', 'failed'])
      ->orderBy('payments.created_at', 'DESC')
      ->get(['payments.id as id', 'payments.amount', 'payments.paid_at', 'payments.created_at', 'payments.status']);

      foreach($payments as $paym){
        $paym->posts = PostPayment::join('posts', 'posts.id', '=', 'post_payment.post_id')
        ->where('post_payment.payment_id', $paym->id)
        ->get([/*'posts._name', */'post_payment.purchased_seconds', 'posts.id as post_id']);
          //Traduzco cada post 
          foreach ($paym->posts as $post) {
              $tradPost = Post::find($post->post_id);
              \ModelTranslator::loadPreferredTranslations($tradPost, ['_name']);
              $post->_name = $tradPost->_name;
          }
      }

      return json_encode($payments);


    }


    public function getClientPaymentInfo(Request $r){
      $per_page = 10; 
      $page = $r->input('page') - 1;

      $paymentsInfo = Payment::join('post_payment as pp', 'pp.payment_id', '=', 'payments.id')
      ->where('payments.status', 'success')
      ->where('payments.amount', '>', 0);

      $order_mode = 'asc';
      if ($r->input('order_mode')){
        $order_mode = $r->input('order_mode');
      }

      if ($r->input('order')){
        switch ($r->input('order')) {
          case 'status':
            $paymentsInfo = $paymentsInfo->orderBy('payments.billed_date', $order_mode);
            break;
          case 'payment_date':
            $paymentsInfo = $paymentsInfo->orderBy('payments.created_at', $order_mode);
          break;
          default:
            # code...
            break;
        }
      }
      //TODO Agregar esta linea para que solo traiga los que realmente pago algo
      //


      $totalItems = $paymentsInfo->count();
      $paymentsInfo = $paymentsInfo->skip($per_page * $page)->take($per_page)->get(['payments.*']);

      foreach($paymentsInfo as $pif){
        //Traigo cada pago individual
        $pif->post_payments = PostPayment::where('payment_id', $pif->id)
        ->join('posts', 'posts.id', '=', 'post_payment.post_id')
        ->get(['post_payment.*', 'posts.user_id']);

        //Traigo el usuario 
        $pif->user = User::find($pif->post_payments[0]->user_id);
        $pif->account = DataUser::where('user_id', $pif->user->id)->first();
      }

      return json_encode([
        'payments' => $paymentsInfo,
        'page' => $r->input('page'),
        'page_size' => $per_page,
        'total_items' => $totalItems
      ]);
    }

    public function getPaymentsNotBilled(Request $r){
      return Payment::join('post_payment as pp', 'pp.payment_id', '=', 'payments.id')
      ->where('payments.status', 'success')
      ->whereNull('payments.billed_date')
      ->where('payments.amount', '>', 0)->count();
      
    }

    public function saveBillInfo(Request $r, $paymentId){
        $payment = Payment::find($paymentId);

        if (!$payment){
          abort(404);
        }
        if (!$payment->billed_date){
          $payment->billed_date = Carbon::now();
        }
        
        $payment->bill_number = $r->input('bill_number');
        $payment->save();
        Log::info(json_encode($payment));

    }



}

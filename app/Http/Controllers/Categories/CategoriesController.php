<?php
namespace App\Http\Controllers\Categories;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use \App\Category;
use \App\Post;
use \App\Configuration;

use \App\Payment;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CategoriesController extends Controller {


    public function getCategoryList(Request $r){ 
        return json_encode(Category::get());
    }

    public function saveAllCategories(Request $r){
        $cats = $r->input('cats');

        $errorOnDelete = false;
  
        //Primero, junto todos los IDS para borrar los que NO esten.
        $ids = [];
        foreach ($cats as $cat) {
            $this->getIdsRecursive($ids, $cat);
        }

        //Log::info($ids);

  
        //Borro los que no vinieron en $cats. Si alguno da error
        //ES porque hay algun producto colgado
        try {
            DB::beginTransaction();
            $catsrv = Category::whereNotIn('id', $ids)->get();
            foreach ($catsrv as $catrv) {
                \ModelTranslator::removeTranslations($catrv);
            }
            Category::whereNotIn('id', $ids)->update(['parentcategory_id' => null]);
            Category::whereNotIn('id', $ids)->delete();
            
            
            
            DB::commit();

        } catch (\Illuminate\Database\QueryException $e){
            $errorOnDelete = true;
            Log::info("ERROR ON DELETE  " . $e->getMessage());
            //Igual continuo, hay que guardar todo el arbol todavia.
        }
  
        //Ahora, recursivamente empiezo a guardar todo
        //Meto todo en una transaccion para que sea atomico y mas rapido
        //Nuevo order de acuerdo al orden puesto, se tiene que pasar por referencia
        $morder = 1;
        DB::beginTransaction();
            foreach ($cats as $cat) {
                //categoria base no tiene padre
                $cat['parentcategory_id'] = null;
                $this->insertOrUpdateCategoryRecursive($cat, $morder);
            }
        DB::commit();
  
        if ($errorOnDelete){
          return 'errOnDel';
        }
        return 'ok';
  
    }

    public function saveCategoriesByPostType(Request $r){
        $config = $r->all();
        foreach ($config as $key => $value) {
            Configuration::updateOrCreate(
                [
                    '_group' => 'cat_amount_by_post_type',
                    '_key' => $key
                ],
                    ['int_value' => $value['max_categories']]
            );
        };
    }

    public function getCategoryAmountByPostType(Request $r){
        $configs = Configuration::where('_group', 'cat_amount_by_post_type')->get();
        $formConfig = [];
        foreach ($configs as $cfg) {
            $formConfig[$cfg->_key] =  [
                'max_categories' => $cfg->int_value
            ];
        };


        return json_encode($formConfig);
    }

    private function insertOrUpdateCategoryRecursive($baseCat, &$morder){
        $selfId = null;
        //Veo si es nueva o vieja.
        if (isset($baseCat['id']) && $baseCat['id'] > 0){
          //Tiene id, tengo que actualizar
          $dbcat = Category::find($baseCat['id']);
          //$dbcat->name = $baseCat['name'];
          $dbcat->morder = $morder;
          $dbcat->parentcategory_id = @$baseCat['parentcategory_id'];
          $dbcat->events_category = @$baseCat['events_category'] ? 1 : 0;
          $morder++;
          $dbcat->save();
          $dbcat->touch();
          \ModelTranslator::saveTranslations($dbcat, $baseCat, ['name']);
          $selfId = $dbcat->id;
        } else {
          //ES nueva, la inserto, pero pero pero tambien tengo que ver
          //que id le toco en la DATABASE
          $baseCat['morder'] = $morder;
          $morder++;
          $cat = Category::create($baseCat);
          \ModelTranslator::saveTranslations($cat, $baseCat, ['name']);

          $selfId = $cat->id;
        }
  
        //Guardo la imagen miniatura redondita
        $image = @$baseCat['new_image'];
        if ($image){
            $ext = pathinfo( $image['filename'], PATHINFO_EXTENSION);
            $fileName = $selfId . '.' . $ext;
            $imgBytes = base64_decode($image['base64']);
    
            Storage::disk('categories_images')->put($fileName, $imgBytes);
            Category::find($selfId)->update(['image' => $fileName]);
            //Limpio memoria
            $imgBytes = null;
            $image = null;
        }
      

        //Recorro todo todito, pero ojo, tengo que ponerle el ID del padre
        //Porque por ahi no lo tiene bien puesto por la libreria de ui-tree
        //Solo te mueve los nodos, pero no cambia nada de adentro
        if (isset($baseCat['nodes'])){
            foreach ($baseCat['nodes'] as $cat) {
                $cat['parentcategory_id'] = $selfId;
                $this->insertOrUpdateCategoryRecursive($cat, $morder);
            }
         }
  
    }
  
    private function getIdsRecursive(&$arr, $baseCat){
        //Veo si tiene id (es decir, no es nuevo)
        if (isset($baseCat['id']) && $baseCat['id'] > 0){
          $arr[] = $baseCat['id'];
        }
  
        //Aca, sea nuevo o no, sigo la recursividad, porque puede ser
        //Que hayan puesto una categoria viejo dentro de una nueva
        if (isset($baseCat['nodes'])){
            foreach ($baseCat['nodes'] as $cat) {
                $this->getIdsRecursive($arr, $cat);
            }
        }
      
    }

    
  //Recursivamente llena todo el arbol de categorias
  private function fetchChildsRecursive($baseCat, &$parentPostCount, $allTranslations){
        $parentId = $baseCat->id;
        $childs = Category::where('parentcategory_id', $parentId)->orderBy('morder','ASC')->get();



        foreach ($childs as $ch) {
            $ch->collapsed = true;

            $pCount = Post::join('post_has_category as phc', 'phc.post_id', '=', 'posts.id')
            ->where('phc.category_id', $ch->id)
            ->count();
            
            $ch->post_count_self = $pCount;
            $this->fetchChildsRecursive($ch, $pCount, $allTranslations);
            $parentPostCount += $pCount;

            $ch->post_count_childs = $pCount;
            if ($allTranslations){
                \ModelTranslator::loadTranslations($ch, ['name']);
            } else {
                \ModelTranslator::loadPreferredTranslations($ch, ['name']);
            }
            


        }

        $baseCat->nodes = $childs;
    }
  
    public function getCategoryTreeWithTranslations(Request $r){
        return $this->_getCategoryTree($r, true);
    }
    public function getCategoryTree(Request $r){
        return $this->_getCategoryTree($r, false);
    }

    private function _getCategoryTree(Request $r, $allTranslations = true){
        $type = $r->input('type');

        DB::beginTransaction();
        $cats = Category::orderBy('morder', 'ASC')
        ->where('parentcategory_id', null);

        

        //Solo traigo las aptas para eventos
        if (!is_null($type)){
            switch ($type) {
                case 'event':
                    $cats = $cats->where('events_category', true);
                break;
                case 'normal':
                    $cats = $cats->where('events_category', false);
                break;
                default:
                    
                    break;
            }
        }
    

        $cats = $cats->get();
        
        
        foreach ($cats as $cat) {
            if ($cat->image){
                $cat->image = $cat->image . '?' . rand();
            }
                
            $cat->collapsed = true;

           
            $pCount = Post::join('post_has_category as phc', 'phc.post_id', '=', 'posts.id')
            ->where('phc.category_id', $cat->id)
            ->count();

            if ($allTranslations){
                \ModelTranslator::loadTranslations($cat, ['name']);
            } else {
                \ModelTranslator::loadPreferredTranslations($cat, ['name']);
            }
            

            $cat->post_count_self = $pCount;
            $this->fetchChildsRecursive($cat, $pCount, $allTranslations);
            $cat->post_count_childs = $pCount;
        }
        DB::commit();
  
        return $cats;
    }


}
<?php
namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use \App\User;
use \App\Post;
use \Carbon\Carbon;
use \App\PaymentMethod;
use \App\DataUser;
use \App\Role;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Mail;


class UserController extends Controller {

    public function login(Request $r){
        $username = $r->input('user');
        $password = $r->input('password');

        $user = User::where('email', $username)->orWhere('name', $username)->first();
        if(!$user){
          return json_encode([
            'message' => "No existe el usuario o la contraseña es inválida"
          ]);
        }

        if (!Hash::check($password, $user->password)){
          return json_encode([
            'message' => "No existe el usuario o la contraseña es inválida"
          ]);
        }

        $isAdmin = false;
        foreach ($user->roles as $role) {
            if ($role->id_str == 'ADMIN'){
                $isAdmin = true;
            }
        }
        if (!$isAdmin && !$user->active){
          return json_encode([
            'message' => "La cuenta no fue activada todavia, por favor espere a que un administrador active su cuenta"
          ]);
        }


        Auth::login($user);
        if ($user){
          $user->load('roles');
        }

        $info = DataUser::where('user_id', $user->id)->first();

        if ($info){
          $user->info = $info;
        } else {
          $user->info = null;
        }
        return json_encode([
          'user' => $user
        ]);
    }

    public function activateAccount(Request $r, $id){
      $user = User::find($id);
      if (!$user){
        abort(404, "user not found");
      }
      $user->active = true;
      $user->payment_method_id = $r->input('payment_method_id');
      $user->save();
      return 'ok';
    }

    public function saveUser(Request $r){
      DB::transaction(function() use ($r){
        $user = User::find($r->input('id'));
        if (!$user){
          abort(404, "User not found");
        }
        $role = Role::where('id_str', 'EVENT_MANAGER')->first();
        //Si vino con asignacion de que puede hacer eventos, le pongo el  rol
        $user->roles()->detach($role->id);
        if ($r->input('eventManager')){
          $user->roles()->attach($role->id);  
        }  
      });
     
    }

    public function disableAccount(Request $r, $id){
      $user = User::find($id);
      $force = $r->input('force');
      $errMsg = null;
      if (!$user){
        abort(404, "user not found");
      }
      //Primero veo si tiene publicaciones gratis.
      $postCount = Post::where('user_id', $id)
      ->where('_type', 'free')
      ->count();
      if ($postCount > 0){
        $errMsg = "El cliente tiene $postCount publicacion/es gratis activas ";
      }
      //Ahora veo si tiene publicaciones premium no vencidas
      $postCount = Post::where('user_id', $id)
      ->where('_type', '<>', 'free')
      ->where('expiration', '>', Carbon::now()->format('Y-m-d H:i:s'))
      ->count();
      if ($postCount > 0){
        if ($errMsg)
          $errMsg .= "y $postCount publicacion/es pagas activas ";
        else 
          $errMsg = "El cliente tiene $postCount publicacion/es pagas activas ";
      }

      //Si no fuerza la desactivacion, retorno el error si es que hay
      if (!$force && $errMsg){
        return $errMsg;
      }
      //Sino, desactivo la cuenta. 
      $user->active = false;
      $user->save();
    }

    //TODO password "pseudoseguro", enrealidad no es tan seguro. pero no importa
    public function randomPassword() {
        $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    public function registerClient(Request $r){
        $data = $r->all();
        $user = User::where('email', $data['email'])->first();

        if($user){
          return 'Ya existe un usuario con ese correo electronico';
        }

        //Quito esto por si algun vivillo quiso cambiarlo en el frontend
        $data['active'] = false;
        $data['payment_method_id'] = null;

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);
        $user->roles()->attach(Role::where('id_str', 'CLIENT')->first()->id);

        return 'ok';
    }

    public function getPaymentMethods(Request $r){
      return json_encode(PaymentMethod::get());
    }

    public function checkRole($client, $roleIdStr){
        foreach($client->roles as $role){
          if ($role->id_str == $roleIdStr){
            return true;
          }
        }
        return false;
    }

    public function getClients(Request $r){

      $clients = User::join('user_has_role as uhr', 'users.id', '=', 'uhr.user_id')
      ->join('roles as rs', 'rs.id', '=', 'uhr.role_id')
      ->leftJoin('posts as ps', 'ps.user_id', '=', 'users.id')
      ->leftJoin('reports as rp', 'rp.post_id', '=', 'ps.id')
      ->leftJoin('reports as ns_rp', function($join){
        $join->on('ns_rp.post_id', '=', 'ps.id')
        ->where('ns_rp._status', '=', 'not_seen');
      })
      ->where('rs.id_str', 'CLIENT')
      ->groupBy('users.id')
      ;

      //Veo los filtros que vinieron
      $filterMode = $r->input('filter_mode');
      if ($filterMode){
        switch ($filterMode) {
          case 'register_date':
            $clients->orderBy('created_at', 'ASC');
            break;
          case 'reports_first':
          $clients->orderBy('not_seen_reports_count', 'DESC');

            break;
          case 'posts_amount':
            $clients->orderBy('posts_count', 'DESC');
              break;
              
          default:
            break;
        }
      }

      $clients = $clients->get([
        'users.*',
        DB::raw(' count(distinct(rp.id)) as reports_count'),
        DB::raw(' count(distinct(ps.id)) as posts_count'),
        DB::raw(' count(distinct(ns_rp.id)) as not_seen_reports_count')
        
        ]);

      foreach($clients as $cl){
        $cl->load('roles');
        $cl->eventManager = $this->checkRole($cl, 'EVENT_MANAGER');
      }

      return json_encode(
        $clients
        );
    }

    public function registerAdmin(Request $r){
      $username = $r->input('data.name');
      $email = $r->input('data.email');

      $user = User::where('email', $email)->first();

      if($user){
        return json_encode([
            'message' => "Error usuario ya registrado"
          ]);
      }
      $pass = $this->randomPassword();

      //Primero mando el mail, despues agrego el user.
      Mail::send('emails.newadmin', ['password' => $pass], function ($m) use ($email){
          $m->from('turismomovilprueba@gmail.com', 'Turismo Movil');
          $m->to($email, "TurismoMovil")->subject('Sos administrador de Turismo Movil!');
      });

      $user = User::create([
          "name" => $username,
          "email" => $email,
          "password" => Hash::make($pass)
        ]);

      $user->roles()->attach(Role::where('id_str', 'ADMIN')->first()->id);
      $user->roles()->attach(Role::where('id_str', 'CLIENT')->first()->id);
      $user->roles()->attach(Role::where('id_str', 'EVENT_MANAGER')->first()->id);

      return 'ok';
    }

    public function getRoles(Request $r){
      return json_encode(Role::get());
    }

    public function getLoggedUser(Request $r){
      $user = Auth::user();

      if ($user){
        $user->load('roles');
        $info = DataUser::where('user_id', $user->id)->first();

        if ($info){
          $user->info = $info;
        } else {
          $user->info = null;
        }

        //Le carga las notificaciones 
        $user = $user->toArray();
        $user['notifications'] = [];


        $langCount = \App\Language::count();
        $cats = \App\Category::get();
        $missingCategoriesTranslations = 0;
        //Todo optimizar esto algun dia
        foreach ($cats as $cat) {
          $missingCategoriesTranslations += $langCount - \App\ModelTranslation::where('model', 'Category')
          ->whereNotNull('content')
          ->whereRaw("TRIM(content) <> ''")
          ->where('model_id', $cat->id)->count();
        }
        $user['notifications']['missing_post_translations'] = [];
        //Traduccion de posts 
        $posts = \App\Post::where('user_id', $user['id'])->where('_status', '<>', 'deleted')->get();
        foreach ($posts as $post) {
          $colTranslated = 
          DB::select(DB::raw("  select attribute, count(content) as amount
          from model_translations
          where model = 'Post'
          and model_id = :modelId
          and content is not null
          and trim(content) <> ''
          group by attribute"), [$post->id]);
          $missingTranslation = false;
          foreach ($colTranslated as $col) {
            if ($col->amount < $langCount && $col->amount > 0){
              if ($col->attribute == 'description' && $post->_type == 'free'){
                //En este caso, la publicacion fue premium pero se cambio a free
                //, quedo un campo sin traducir pero como es free no esta utilizable
                //asi que ignoro
                continue;
              }
              $missingTranslation = true;
            }

          }
         // \App\ModelTranslation::where('model', 'Post')
          //->where('model_id', $post->id)
          //->groupBy('attribute');
          if ($missingTranslation){
            $user['notifications']['missing_post_translations'][] = $post->id;

          }
        }


        $user['notifications']['categories'] = [
            'missing_translations' => $missingCategoriesTranslations
        ];
      


        return json_encode($user);
      } else {
        return null;
      }
    }

    public function logout(Request $r){
        Auth::logout();
    }

    public function checkEmail(Request $r, $email){
      $usr = User::where('email', $email)->first();
      if ($usr){
        return 'in_use';
      }
      return 'free';
    }

    public function saveAccountData(Request $r){
      $user = Auth::user();
      $id = $user->id;

      DataUser::updateOrCreate(
        ["user_id" => $id],[
        "cuit" => $r->input("data.cuit"),
        "bussiness_name" => $r->input("data.bussiness_name"),
        "phone" => $r->input("data.phone"),
        "_address" => $r->input("data._address"),
        "address_number" => $r->input("data.address_number"),
        "city" => $r->input("data.city"),
        "postal_code" => $r->input("data.postal_code"),
        "province" => $r->input("data.province")
        ]);
    }

    public function getAccountData(Request $r){
      $user = Auth::user();
      $id = $user->id;

      $dataUser = DataUser::where("user_id", $id)->first();

      if($dataUser == null){
        return null;
      }else{
        return json_encode($dataUser);
      }
    }

    public function changePassword(Request $r){
      $user = Auth::user();

      $password = $r->input('password');
      $newPassword = $r->input('newPassword');
      $rptNewPassword = $r->input('rptNewPassword');

      if (!Hash::check($password, $user->password)){
          return json_encode([
            'message' => "La contraseña es inválida"
          ]);
      }
      if($newPassword == $rptNewPassword){
        $user->password =  Hash::make($newPassword);
        $user->save();
      }
    }

}

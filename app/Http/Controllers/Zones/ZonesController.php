<?php
namespace App\Http\Controllers\Zones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use \App\Zone;
use \App\Post;
use Illuminate\Support\Facades\Storage;

class ZonesController extends Controller{

    //crear una zona
    public function saveZone(Request $r){
        Zone::create([
            "_name" => $r->input("data._name"),
            "_description" => $r->input("data._description"),
            "_deleted"=>false
        ]);

    }
    public function getZone(Request $r, $id){
        return json_encode(Zone::find($id));
    }

    public function getZones(Request $r){
       return json_encode(Zone::where("_deleted",false)->get());
    }

    public function getZonesApi(Request $r){
      return json_encode(Zone::where("_deleted",false)->get());
    }

    public function deleteZone(Request $r, $id){
        //Implementacion de una baja logica de una zona
        $zone = Zone::find($id);
        if((Post::where("zone_id", $id)
                        ->join('zones','posts.zone_id','=','zones.id')
                        ->count()) != 0){
            return json_encode([
            'message' => "No se puede eliminar la zona porque contiene publicaciones asociadas"
          ]);
        } else {
            $zone->_deleted = true;
            $zone->save();
            return 'ok';
        }   
    }
}

<?php
namespace App\Http\Controllers\StaticInfo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use \App\StaticInfo;
use \App\Module;

use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class StaticInfoController extends Controller {

    public function findById(Request $r, $id){
        $sinfo = StaticInfo::find($id);
        \ModelTranslator::loadTranslations($sinfo, ['title']);

        $sinfo->modules = Module::where('staticinfo_id', $id)->orderBy('morder', 'ASC')->get();
        foreach ($sinfo->modules as $mod) {
            \ModelTranslator::loadTranslations($mod, ['mdata']);    
        }
        
        return json_encode($sinfo);
    }

    public function getGallery(Request $r){
        $files = Storage::disk('static_info_gallery')->files();
        $images = [];
        foreach ($files as $file) {
            $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
            if (in_array($fileExtension, ['jpg', 'png'])){
                $images[] = $file;
            }
        }
        return json_encode($images);
    }

    //Obtiene STATIC INFO de acuerdo al lenguaje indicado. Se adapta el formato para 
    //El parser anterior 
    public function getStaticInfo(Request $r, $id, $langId){
        $sinfo = StaticInfo::find($id);
        \ModelTranslator::loadPreferredTranslations($sinfo, ['title'], $langId);
        $sinfo->modules = Module::where('staticinfo_id', $id)->orderBy('morder', 'ASC')->get();
        foreach ($sinfo->modules as $mod) {
            $template = json_decode($mod->template, true);
            \ModelTranslator::loadPreferredTranslations($mod, ['mdata'], $langId);

            $mdata = json_decode($mod->mdata, true);
            unset($mod->template);
            unset($mod->mdata);

            switch($template['type']){
                case 'image':
                    $mdata['image'] = $template['image'];
                break;
                case 'table':
                    $fmTable = [];
                    $rows = $template['rows'];
                    $cols = $template['cols'];
                    for($iRow = 0; $iRow < $rows; $iRow++){
                        $newCol = [];
                        for($iCol = 0; $iCol < $cols; $iCol++){
                            $newCol[] = [
                                'text' => $mdata[$iCol .'-'.$iRow]
                            ];
                        }
                        $fmTable[] = $newCol;
                    }
                    
                    $mdata = ['table' => $fmTable];


                break;
            }
            //Guarda MDATA en formato json (string) ya traducido.
            $mdata['idstr'] = $template['type'];
            $mod->mdata = json_encode($mdata);

        }
        return json_encode($sinfo);
    }

    public function getInfoFromZones(Request $r, $zonesIds, $langId){
        $zonesIdArr = explode(',', $zonesIds);
        $sinfos = StaticInfo::join('zones', 'zones.id', '=', 'static_info.zone_id')
        ->whereIn('static_info.zone_id', $zonesIdArr)
        ->get(['static_info.zone_id', 'zones._name as zoneName', 'static_info.id as id']);

        foreach ($sinfos as $sinfo) {
            \ModelTranslator::loadPreferredTranslations($sinfo, ['title'], $langId);
        }

        return json_encode($sinfos);
    }

    public function saveGalleryImage(Request $r){
        //Log::info($r->all());

        $user = Auth::user();
        $img = $r->all();
        $base64 = $img['base64'];
        $bytes = base64_decode($base64);
        $base64 = null; //Limpio variable. 

        //Genero nombre de archivo
        $fileName = $user->id . "" .  round(microtime(true) * 1000) . ".png";

        Storage::disk('static_info_gallery')->put($fileName, $bytes);
        $bytes = null;
    }

    public function getStaticInfoList(Request $r){
        $list = StaticInfo::get();
        foreach ($list as $sinfo) {
            $nativeLang = \App\Language::where('is_native', true)->first();
            $prefId = $nativeLang ? $nativeLang->id : -1;
            \ModelTranslator::loadPreferredTranslations($sinfo, ['title'], $prefId);

        }
        return json_encode($list);
    }


    public function removeStaticInfo(Request $r, $id){



        DB::transaction(function() use($id){
            $modules = Module::where('staticinfo_id', $id)->get();
            foreach ($modules as $mod) {
                //Log::info(json_encode($mod));
                \ModelTranslator::removeTranslations($mod);

                $mod->delete();
            }
            $sinfo = StaticInfo::where('id', $id)->first();
            //Log::info(json_encode($sinfo));

            \ModelTranslator::removeTranslations($sinfo);
            $sinfo->delete();

        });
    }

    public function save(Request $r){
        $data = $r->all();
        $staticinfoId = null;
        $modules = $data['modules'];
        unset($data['modules']);
        $sinfo_translation = ['title' => $data['title']];
        //Le quita a data "title" para que al trabajar con eloquent no de error
        unset($data['title']);

        if (isset($data['id'])){
            $staticinfoId = $data['id'];
            StaticInfo::where('id', $data['id'])
            ->update($data);
        } else {
            $staticinfoId =  StaticInfo::create($r->all())->id;
        }

        //Guarda traduccion de titulo
        $sinfo = StaticInfo::find($staticinfoId);
        \ModelTranslator::saveTranslations($sinfo, $sinfo_translation, ['title']);


        $exists = Module::where('staticinfo_id', $staticinfoId)->get();
        foreach ($exists as $m) {
            \ModelTranslator::removeTranslations($m);
            $m->delete();
        }

        Module::where('staticinfo_id', $staticinfoId)->delete();
        foreach ($modules as $mod) {
            $mod['staticinfo_id'] = $staticinfoId;
            $newmod = Module::create($mod);
            \ModelTranslator::saveTranslations($newmod, $mod, ['mdata']);
        }

        return $staticinfoId;
    }

}

<?php
use Illuminate\Support\Facades\Hash;
use \App\User;
use Illuminate\Support\Facades\Log;


/*
|--------------------------------------------------------------------------|
| Application Routes                                                       |
|--------------------------------------------------------------------------|
|                                                                          |
| Here is where you can register all of the routes for an application.     |
| It's a breeze. Simply tell Laravel the URIs it should respond to         |
| and give it the controller to call when that URI is requested.           |
|                                                                          |
*/

// RESULTADO DE LAS TRANSACCIONES
Route::get('/todopago/ok', 'Payments\\PaymentsController@paymentOk');
Route::get('/todopago/error', 'Payments\\PaymentsController@paymentErr');
Route::get('/payments/{id}', 'Payments\\PaymentsController@getPayment');
/*
Route::get('/make_user', function () {
   $user = User::create([
          "name" => "Enzo",
          "email" => "scalone.enzo@gmail.com",
          "password" => Hash::make("asdasdasd")
        ]);

        $user->roles()->attach(App\Role::where('id_str', 'ADMIN')->first()->id);
        $user->roles()->attach(App\Role::where('id_str', 'CLIENT')->first()->id);
        $user->roles()->attach(App\Role::where('id_str', 'EVENT_MANAGER')->first()->id);


});
*/
Route::get('/', function () {
    return view('welcome');
});

//Funciones varias
Route::get('/email_exists/{email}', 'Users\\UserController@checkEmail');


// Route's posts
Route::get('/posts', 'Posts\\PostsController@getPosts')->middleware('customer.auth');
Route::post('/posts', 'Posts\\PostsController@savePost')->middleware('customer.auth');
Route::get('/posts/{id}', 'Posts\\PostsController@getPost')->middleware('customer.auth');

Route::get('/reports', 'Posts\\PostsController@getReports')->middleware('customer.auth');

Route::post('/posts/remove/{id}', 'Posts\\PostsController@deletePost')->middleware('customer.auth');
Route::post('/posts/{id}/activate', 'Posts\\PostsController@activatePost')->middleware('customer.auth');
Route::post('/posts/{id}/cancel_time', 'Posts\\PostsController@cancelPostTime')->middleware('customer.auth');

Route::post('/reports/{id}/read', 'Posts\\PostsController@readReport')->middleware('customer.auth');


Route::get('/payment_history', 'Payments\\PaymentsController@getPaymentHistory')->middleware('customer.auth');


Route::post('/posts/block/{post_id}', 'Posts\\PostsController@blockPostsUser')->middleware('customer.auth');
Route::get('/posts/getPostsUser/{user_id}', 'Posts\\PostsController@getPostsUser')->middleware('customer.auth');


// Route's zones
Route::post('/zones','Zones\\ZonesController@saveZone')->middleware('customer.auth');
Route::get('/zones','Zones\\ZonesController@getZones')->middleware('customer.auth');
Route::post('/zones/remove/{id}','Zones\\ZonesController@deleteZone')->middleware('customer.auth');
Route::post('/static_info/remove/{id}','StaticInfo\\StaticInfoController@removeStaticInfo')->middleware('customer.auth');

// User administration
Route::post('/login', 'Users\\UserController@login');
Route::post('/logout', 'Users\\UserController@logout')->middleware('customer.auth');
Route::get('/session', 'Users\\UserController@getLoggedUser');
Route::post('/change_password', 'Users\\UserController@changePassword')->middleware('customer.auth');
Route::post('/register_admin', 'Users\\UserController@registerAdmin')->middleware('customer.auth');
Route::post('/register_client', 'Users\\UserController@registerClient');

Route::get('/clients', 'Users\\UserController@getClients')->middleware('customer.auth');
Route::post('/users/{id}/activate', 'Users\\UserController@activateAccount')->middleware('customer.auth');
Route::post('/users/{id}/disable', 'Users\\UserController@disableAccount')->middleware('customer.auth');
Route::post('/users', 'Users\\UserController@saveUser')->middleware('customer.auth');

Route::get('/payment_methods', 'Users\\UserController@getPaymentMethods')->middleware('customer.auth');

Route::post('/account_info', 'Users\\UserController@saveAccountData')->middleware('customer.auth');
Route::get('/account_info', 'Users\\UserController@getAccountData')->middleware('customer.auth');

// PAGOS Y PAYMENTS Y PAGOS
Route::get('/promos',  'Payments\\PaymentsController@getPromos');
Route::post('/promos',  'Payments\\PaymentsController@savePromos')->middleware('customer.auth');;
Route::get('/testTp',  'Payments\\PaymentsController@testTp');

Route::post('/confirm_order',  'Payments\\PaymentsController@confirmOrder');


// Route's Roles
Route::get('/getRoles', 'Users\\UserController@getRoles')->middleware('customer.auth');

//Static info 

Route::post('/static_info',  'StaticInfo\\StaticInfoController@save')->middleware('customer.auth');
Route::get('/static_info/gallery',  'StaticInfo\\StaticInfoController@getGallery')->middleware('customer.auth');
Route::get('/static_info/list',  'StaticInfo\\StaticInfoController@getStaticInfoList')->middleware('customer.auth');

Route::post('/static_info/gallery',  'StaticInfo\\StaticInfoController@saveGalleryImage')->middleware('customer.auth');
Route::get('/static_info/{id}',  'StaticInfo\\StaticInfoController@findById')->middleware('customer.auth');


// Categories 
Route::get('/categories',  'Categories\\CategoriesController@getCategoryTree')->middleware('customer.auth');
Route::get('/translated_categories',  'Categories\\CategoriesController@getCategoryTreeWithTranslations')->middleware('customer.auth');

Route::get('/categories/list',  'Categories\\CategoriesController@getCategoryList')->middleware('customer.auth');
Route::get('/categories_by_post_type',  'Categories\\CategoriesController@getCategoryAmountByPostType')->middleware('customer.auth');
Route::post('/categories',  'Categories\\CategoriesController@saveAllCategories')->middleware('customer.auth');
Route::post('/categories_by_post_type',  'Categories\\CategoriesController@saveCategoriesByPostType')->middleware('customer.auth');

// API para android
Route::get('/api/zones', 'Zones\\ZonesController@getZonesApi');
Route::get('/api/posts', 'Posts\\PostsController@getPostsApi');
Route::get('/api/posts/{id}', 'Posts\\PostsController@getPostApi');
Route::get('/api/posts/{id}/photos', 'Posts\\PostsController@getPhotosApi');
Route::get('/api/posts/zone_id/{zone_id}', 'Posts\\PostsController@getPostsByZoneApi');
Route::get('/api/posts/tourist/{email}', 'Posts\\PostsController@getPostsByTouristEmailApi');
Route::post('/api/posts/tourist/{email}', 'Posts\\PostsController@saveFavoritesApi');
Route::post('/api/posts/{id}/rating', 'Posts\\PostsController@ratePostApi');
Route::post('/api/posts/{id}/report', 'Posts\\PostsController@reportPostApi');
//Esta super ruta trae un paquete de update de acuerdo a las ultimas fechas correspondientes
Route::get('/api/posts/zone_ids/{zone_ids}/from_dates/{last_post_date}/{last_category_date}/{langId}', 'Posts\\PostsController@getApiUpdate');
Route::get('/api/languages/{last_lang_update}/{last_translation_update}', 'Languages\\LanguageController@getApiLanguageUpdatePack');

Route::get('/api/categories/{date}', 'Posts\\PostsController@getCategoriesFromDate');
Route::get('/api/zones/{zone_ids}/static_info/no_html/{langId}', 'StaticInfo\\StaticInfoController@getInfoFromZones');
Route::get('/api/static_info/{id}/{langId}', 'StaticInfo\\StaticInfoController@getStaticInfo');


//Checkeador de link
Route::post('/check_link', 'Posts\\PostsController@checkLink');

//Facturacion (admin)
Route::get('/billing', 'Payments\\PaymentsController@getClientPaymentInfo');
Route::post('/billing/{payment_id}/bill', 'Payments\\PaymentsController@saveBillInfo');
Route::get('/notifications/payments_not_billed', 'Payments\\PaymentsController@getPaymentsNotBilled');

//Traducciones
Route::post('/languages/{id}/native', 'Languages\\LanguageController@setNative');
Route::get('/languages/translation_items', 'Languages\\LanguageController@getTranslationItems');
Route::get('/languages/translation/native', 'Languages\\LanguageController@getNativeTranslation');

Route::get('/languages/translation/{language_id}', 'Languages\\LanguageController@getTranslation');

Route::get('/languages', 'Languages\\LanguageController@getLanguages');
Route::post('/languages/translation', 'Languages\\LanguageController@saveTranslation');
Route::post('/languages/remove', 'Languages\\LanguageController@removeLanguageAndReferences');
Route::post('/languages/language_preference', 'Languages\\LanguageController@saveLanguagePreference');

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{

    public $table = "promos";
    public $timestamps = false;

    protected $fillable = [
        'time', 'price', 'post_type', 'name'
    ];

}

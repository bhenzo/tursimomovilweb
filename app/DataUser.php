<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataUser extends Model
{
    public $table = "data_users";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cuit', 'bussiness_name', 'phone','_address','user_id', 'city', 'postal_code', 'province', 'address_number'
    ];
}

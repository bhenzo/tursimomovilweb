<?php

namespace App\Services;
use App\Post;

class TodoPagoService {

  public function __construct(){
    //TODO FIXME Estos son datos de sandbox, hay que sacarlos de otro lado.
     $this->merchant = 2159;
     $this->authorization = 'PRISMA A793D307441615AF6AAAD7497A75DE59';
     $this->security = 'A793D307441615AF6AAAD7497A75DE59';
     $this->endpoint = 'https://developers.todopago.com.ar/services/t/1.1/';
     $this->mode = 'test';
     $this->currencycode = 032;
  }

    public function getPayForm($orderCode, $clientIp, $userId, $amount, $userInformation){
        $http_header = array('Authorization'=> $this->authorization);
        $connector = new \TodoPago\Sdk($http_header, $this->mode);

        $baseUrl = config('app.url');

        $optionsSAR_comercio = array (
          'Security'=> $this->security,
          'EncodingMethod'=>'XML',
          'Merchant'=>$this->merchant,
          'URL_OK'=>  $baseUrl.'/todopago/ok?order=' . $orderCode,
	        'URL_ERROR'=> $baseUrl.'/todopago/error?order=' . $orderCode
        );

        $prov = substr($userInformation['province'], 0, 2);
        $grandtotal = $amount;
        $grandtotal = number_format($grandtotal, 2, '.',  '');

        //Hago un formateo del telefono por si no vino bien
        $phone = preg_replace("/[^0-9]/","", $userInformation['phone']);
        if (substr($phone, 0, 2) != '54'){
            $phone = '54' . $phone;
        }
        while(strlen($phone) < 8){
          $phone = $phone . '0';
        }

        //Veo que la ip del cliente sea algun numero del largo de una ip
        if (strlen($clientIp) < 6){
          $requestIp = "127.0.0.1";
        } else {
          $requestIp = $clientIp;
        }


        //DATOS DEL CLIENTE O DUEnO DE TARJETA
        $uEmail = $userInformation['email'];
         $uCity = $userInformation['city'];
         $uName = $userInformation['name'];
         $uLastName = $userInformation['lastname'];
         $uPostalCode = $userInformation['postal_code'];
         $uAddress =  $userInformation['address'];
         $uAddrNumber =  $userInformation['address_number'];

        //Datos de la compra
        $CSITPRODUCTCODE = '0001';
        $CSITPRODUCTDESCRIPTION = 'SegundosTM';
        $CSITPRODUCTNAME = 'SegundosTM';
        $CSITPRODUCTSKU = '0001';
        $CSITTOTALAMOUNT = "$grandtotal";
        $CSITQUANTITY = '1';
        $CSITUNITPRICE = "$grandtotal";


        //Array para mandar a todo pago
        $optionsSAR_operacion = array (
         //'MERCHANT'=> $this->merchant,
         'Merchant'=> $this->merchant,
         'OPERATIONID'=> $orderCode,
         'CURRENCYCODE'=> 32,
         'AMOUNT'=> $grandtotal,
         'EMAILCLIENTE'=>$uEmail,
         'CSBTCITY'=> $uCity,
         'CSSTCITY'=> $uCity,
         'CSBTCOUNTRY'=> "AR",
         'CSSTCOUNTRY'=> "AR",
         'CSBTEMAIL'=>  $uEmail,
         'CSSTEMAIL'=>  $uEmail,
         'CSBTFIRSTNAME'=>  $uName,
         'CSSTFIRSTNAME'=>  $uName,
         'CSBTLASTNAME'=>  $uLastName,
         'CSSTLASTNAME'=>  $uLastName,
         'CSBTPHONENUMBER'=>  $phone,
         'CSSTPHONENUMBER'=>  $phone,
         'CSBTPOSTALCODE'=>  $uPostalCode,
         'CSSTPOSTALCODE'=>  $uPostalCode,
         'CSBTSTATE'=> $prov,
         'CSSTSTATE'=> $prov,
         'CSBTSTREET1'=>  $uAddress. " " .  $uAddrNumber,
         'CSSTSTREET1'=>  $uAddress . " " .  $uAddrNumber,
         'CSBTCUSTOMERID'=> $userId,
         'CSBTIPADDRESS'=> $requestIp,
         'CSPTCURRENCY'=> "ARS",
         'CSPTGRANDTOTALAMOUNT'=> $grandtotal,
         'CSMDD7'=> "",
         'CSMDD8'=> "Y",
         'CSMDD9'=> "",
         'CSMDD10'=> "",
         'CSMDD11'=> "",
         'CSMDD12'=> "",
         'CSMDD13'=> "",
         'CSMDD14'=> "",
         'CSMDD15'=> "",
         'CSMDD16'=> "",

         'CSITPRODUCTCODE'=> $CSITPRODUCTCODE,
         'CSITPRODUCTDESCRIPTION'=> $CSITPRODUCTDESCRIPTION,
         'CSITPRODUCTNAME'=> $CSITPRODUCTNAME,
         'CSITPRODUCTSKU'=> $CSITPRODUCTSKU,
         'CSITTOTALAMOUNT'=> $CSITTOTALAMOUNT,
         'CSITQUANTITY'=> $CSITQUANTITY,
         'CSITUNITPRICE'=> $CSITUNITPRICE
         );

         $rta = $connector->sendAuthorizeRequest($optionsSAR_comercio, $optionsSAR_operacion);

         if (isset($rta['StatusCode']) && $rta['StatusCode'] == -1){
           //Solicitud registrada
           return [
             'status' => 'ok',
             'form_url' => $rta['URL_Request']
           ];
         } else {
           return [
             'status' => 'err',
             'message' => $rta['StatusMessage']
           ];
         }



    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{

    public $table = "reports";
    public $timestamps = true;

    protected $fillable = [
        'tourist_unique_identificator', 'message', 'post_id'
    ];
   

}

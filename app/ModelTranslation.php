<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelTranslation extends Model
{

    public $table = "model_translations";
    public $timestamps = false;

    protected $fillable = [
       'model', 'model_id', 'attribute', 'content', 'language_id'
    ];
    
}

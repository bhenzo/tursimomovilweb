<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public $table = "languages";
    public $timestamps = true;

    protected $fillable = [
        '_name', 'is_native', 'preference'
    ];


}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Translator;

class TranslatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('modeltranslator',function(){

            return new \App\Helpers\ModelTranslator();
    
    });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    public $table = "payment_methods";
    public $timestamps = false;

    protected $fillable = [
        
    ];
}

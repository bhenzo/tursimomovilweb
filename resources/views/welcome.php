<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="app/app.css" rel="stylesheet">
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyBdUW6PlIo3d-LUA4mmwPAIrBcsrN1Z_m0"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">
   
    <script src="app/app.js" charset="utf-8"></script>
    <title>Hello, world!</title>
  </head>
  <body data-ng-app="TurismoMovil">

    <div class="tapador" data-ng-show="showTapador()">
      <div class="minicartel">
        Cargando... {{(maxAjax - ajaxLoading + 1)}} / {{maxAjax}}
      </div>
    </div>


    <div data-ng-controller="HeaderCtrl" ng-include="'app/html/header/header.html'"></div>


    <div data-ng-view data-ng-class="{'container' : !isPublicPath()}">
    </div>

  </body>


</html>

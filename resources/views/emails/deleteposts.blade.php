<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <hr>
        <h3>Publicaciones Canceladas</h3>
        <p>Has recibido este correo electrónico porque se acaban de eliminar la siguiente publicacion: {{$publicacion}} por no cumplir con nuestras normas.</p>
        <br><br>
        <p>Atte Staff de Turismo Movil</p>
      <hr>
  </body>
</html>
